<html>

<body class=bg>
<link rel='stylesheet' href='p9u2j3mklasd9i123kpocl435n/style.css'>
<link rel="icon" type="image/x-icon" href="icon.ico">


<script src='p9u2j3mklasd9i123kpocl435n/com.js'></script>
<script>
	function start(){
		Com.sendStatus(()=>{
			location.href='p9u2j3mklasd9i123kpocl435n/'		
		})
	}


</script>	

<div id="title">
	<div id="inner">
		JONAS
		<br>
		<a onclick="start()">&#9654;</a>
	</div>
</div>
	
<footer class="footer">
  <p style="text-align:left;">
    © 2018 Benedikt Strehle
  <span style="float:right;">
    <a href="https://gitlab.com/benStre/jonas" ><img style="height:1.7em" src="gitlab-card.png"/></a></div>  
  </span>
  </p>
	
</footer>

<style>

	
	body, html {
	    	height: 100%;
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	body{
	 	display: flex;
  		flex-direction: column;		
	}

	.footer {
	  	flex-shrink: 0;
		background-color:#111;
		color:white;
		padding-left:1em;
		padding-right:1em;
		font-size:0.9em;
		font-family:GameFont;
	}

	.bg { 
	    /* The image used */
	    background-image: url("bg3.png");

	    /* Full height */
	    height: 100%; 

	    /* Center and scale the image nicely */
	    background-position: center;
	    background-repeat: no-repeat;
	    background-size: cover;
	}

	#title{
		width:100%;
		text-align:center;
		font-family:GameFont;
		font-size:5em;
		color:white;
		margin-top:15%;
		
		
		flex: 1 0 auto;
	}

	#inner {
		padding:3%;
		background-color:#00000077;	
	}

	
	
	a:hover {
	   cursor:pointer;
	   color: #eee;          
	}

	a:active { 
	    color:#ddd;
	}

	/* custom font */
	@font-face {
	    font-family: 'GameFont';
	    src: url('p9u2j3mklasd9i123kpocl435n/font2.ttf');
	}
	
</style>
     
</html>
