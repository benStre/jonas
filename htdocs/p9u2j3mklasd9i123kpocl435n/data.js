// sprite dictionary
spritedata = {
    
    "persongrey_front": 2,

    "personbrown_front": 2,
    "personbrown_left": 5,
    "personbrown_right": 5,
    "personbrown_jumpright": 1,
    "personbrown_jumpright": 1,
   
    "personred_left": 5,
    "personred_front": 4,
    "personred_right": 5,
    "personred_jumpleft": 1,
    "personred_jumpright": 1,
    "personred_sit": 2,
    "personred_back": 1,

    "personwhite_left": 5,
    "personwhite_front": 3,
    "personwhite_right": 5,
    "personwhite_jumpleft": 1,
    "personwhite_jumpright": 1,
    "personwhite_sit": 1,
   
    "personblue_left": 5,
    "personblue_front": 2,
    "personblue_right": 5,
    "personblue_jumpleft": 1,
    "personblue_jumpright": 1,
    "personblue_sit": 1,
    

    "personorange_left": 5,
    "personorange_front": 3,
    "personorange_right": 5,
    "personorange_jumpleft": 1,
    "personorange_jumpright": 1,

    "jonas_faceright": 1,
    "jonas_faceleft": 1,
    "jonas_facefront": 6,
    "jonas_faceback": 1,

    "jonas_hairright":3,
    "jonas_hairleft":3,
    "jonas_hairfront":3,
    "jonas_hairback":3,

    "benni_faceright": 1,
    "benni_faceleft": 1,
    "benni_facefront": 4,

    "hannah_facefront": 2,
    "janina_facefront": 2,
    
    "soni_facefront": 2,
    "soni_faceleft": 1,
    "soni_faceright": 1,
    
    "oma_facefront": 2,
    "oma_faceleft": 1,
    "oma_faceright": 1,

    "elvi_facefront": 2,
    "opa_facefront": 2,
    "beate_facefront": 2,


    "decoration": 11,
    "hubatz_front": 3,
    "hubatz_right": 8,
    "hubatz_left": 8,
    "hubatz_look": 2,
    "hubatz_stand":2,

    "particles" : 24,
    "skyline": 3,
    "ground": 28,
    "vegetation": 11,
    "wall" : 1,
    "box" : 4,
    "ball": 1,
    "woodendoor" : 2,
    "stars" : 1,
    "vehicles" : 4,
    "field" : 3,
    "groundx" : 4,
    "windmill" : 6,
    "terrain" : 4,
    "speechbubble" : 12,
    "codeinput":1,
    "fieldground":3,
    "gameover": 6, 
    "uicontrols": 4,
    "jungleground": 19,
    "bridge":2,
    "drone":4,
    "sunrays":1,
    "waterfall":4,
    "rocks":4,
    "junglevegetation": 2,
    "items":4,
    "winterdecoration": 12,
    "sleigh":4,
    "laboratory":20,
    "ladders": 1,
    "lego": 10,
    "giftcard":1,
    "rocket": 5,

}

common_leveldata = {
    sprites: [
        "skyline",
        "ground",
        "vegetation",
        "decoration", 
        "particles",
        "terrain",
        "gameover",
        "box",

        "jonas",
        "personred",
        "uicontrols",
        "sunrays",
        "items",
        "speechbubble",

    ],

    sounds: [ 
        "gameover", 
        "notification", 
        "success",
        "grab",
        "door",
        "blop",
        "item",
        "coin",
        "fall",
    ],

}
