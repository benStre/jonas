
/*** VARIABLES *********************************/

/*
	Falls du mal aus irgendeinem Grund hier landest empfehle ich dir nicht, den code genauer anzuschauen - ich bin nicht mehr dazugekommen noch überall sinnvolle kommentare dazuzuschreiben :d und alles schön zu formatieren...

*/



var url = new URL(window.location.href);
var screen = url.searchParams.get("screen");

if(screen=="home"){
	window.location.href = "http://jonas18.rf.gd/"
}

// FPS MANAGEMENT
var REQUESTED_FPS = 50,
    now,
    last = performance.now(),
    interval = 1000/REQUESTED_FPS,
    delta,
    fps = REQUESTED_FPS,
    update_fps = 20


// physical constants
var DM
var DEFAULT_W, DEFAULT_H, ORIGIN
var ACCELERATION = 0.14

//Canvas and DOM Elements
var can = document.getElementById("can"),
    ctx=can.getContext("2d");



// Coins

var max_total_coins = 100
var min_level_coins = 20



// level
var CURRENT_LEVEL = 0

var DEBUG = false
var RUNNING = false
var NO_PARTICLES = false


var GAME_AUTOSTOP = false

// keys
var pressed_keys = []

var mouseX=0
var mouseY=0

// gameover messages
var gameover_messages = ["Das war wohl nix...", "Aller Guten Dinge sind 3!", "Übung macht den Meister...", "Das wird schon noch..."]

//document.body.style.cursor = 'none';

function resetGlobals(){ // reset variables in every level

    window.timeouts = [] // all timeouts
    window.intervals = [] // all intervals

    window.noise = new Noise()

    window.pause_animation_counter=0
    window.pause_animation_shift_x = 0
    window.pause_animation = false
    window.pause_step = 0


    window.GAME_OVER = false
    window.gameover_button_added = false
    window.CONTROLLABLE=true
    window.gameover_message = ""
    window.gameover_tip = ""


    window.todo_tip = ""


    window.total_coins = 0

    window.items = []

    window.max_img_requested = 1000

    window.portal_exists = false

    window.gameover_animation_count = 0
    window.gameover_animation_finished = null

    window.quest = null
    window.quest_count = 0
    window.quest_existing = false
    window.quest_finished = true
    window.quest_finished_count = 0
    window.quest_text_width = 0


    window.level_ground_particles = 1

    window.code_window = false
    
    window.loading = false 


    window.achievement = false

    window.button_home = false
    window.button_reload = false

    window.dark_sky=false
    window.dark_sky_value = 0

    window.controls_disabled = false
    window.controls_disable_counter = 0
    window.coins_disabled = false

    window.show_code = false
    window.loaded_code_url = false

    window.mouse_is_down = false

    window.todos = []

    window.key_hint = []
    window.any_key_hint = 0
    window.constant_keyhint = false

    window.burn_count = 0
    window.key_hint_width = 0

    // ...sprites, players etc.
    window.jonas = null
    window.persons={}

    window.portals = []

    window.patterns = {}

    window.buttons = []

    // special levels

    window.background = null
    window.sun = null
    window.sprites = {}
    
    window.window_blocks = []

    window.sounds = {}
    window.sound_buffers = {}

    window.jumpables = []

    window.particles = []
    window.background_particles = []
    window.smokes = []
    window.sky_particles = []
    window.objects = []
    window.background_objects = []
    window.foreground_objects = []
    window.multilayer_objects = []
    window.physic_objects = []
    window.vehicles = []
    window.actionables = []
    window.hillscapes = []

    window.actionable_in_focus = {}

    window.fill_underground  = false


    // keys
    window.pressed_keys = []

    window.mouseX=0
    window.mouseY=0

    // audio
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    window.audio_ctx = new AudioContext()

    // constants

    window.CURRENT_LEVEL_DATA = {}

    window.img_loaded_counter = 0
    window.img_requested_counter = 0
    window.sound_load_counter = 0
    window.sound_requested_counter = 0    
}



/**********************************************/

window.onresize = function(){
    console.log("resize window")
    resizing()
    handleTimeout(resizing, 200)
}


function nextLevel(){
	Com.sendLevelStatus(CURRENT_LEVEL+1)
	localStorage.setItem("current_level", CURRENT_LEVEL+1)
        location.reload()
}


function resizing(){
    try{
        parameters()
        background.updateGradient()
        if(sun!=false) sun.updateGradient()

    }catch(e){}
}



window.onblur = function(){
    try{pause()}catch(e){}
}

window.onfocus = function(){
    try{resume()}catch(e){}
}


window.onload = ()=>{
    init()
}



/*****************************************/

/* init */

function init(elem){


    can.style.display = "block"
    //fullscreen() // only working with user gesture
    
    console.log(leveldata)

    parameters()
     
    START_LEVEL(parseInt(localStorage.getItem("current_level"))|| 1)
    
}


function loadingScreen(level, color, percent){

    if(percent>1) percent=1

    let font_size = 30
    ctx.fillStyle = color
    ctx.fillRect(0, 0, can.width, can.height);

  
    ctx.fillStyle = "grey"   
    ctx.fillRect((can.width-font_size*3)/2, can.height/2+font_size, (font_size*3), font_size/10)
    
    ctx.fillStyle = "white"
    ctx.textAlign="center"; 
    ctx.font=`${font_size}px GameFont`;
    ctx.fillText("LEVEL " + level, can.width/2, can.height/2)    

    ctx.fillRect((can.width-font_size*3)/2, can.height/2+font_size, percent*(font_size*3), font_size/10)

}


function START_LEVEL(level, restart=false){

    
   
    
    try{
        clearAllTimeouts()
        clearAllIntervals()
    } catch(e){}
    

    RUNNING = false
    GAME_AUTOSTOP = true
    Sound.stop()

    var level_color = leveldata[level-1].border_color
    var level = level

    setTimeout(()=>{
        resetGlobals()
    
        
        loading=true
        
        var loadingscreen_interval = handleInterval(()=>{
            
            if(img_requested_counter!=0) max_img_requested= img_requested_counter
            
            let percent = img_loaded_counter/max_img_requested

            //console.warn(percent) 

            loadingScreen(level, level_color, percent)
            if(percent==1){
                loading = false
                clearInterval(intervals[loadingscreen_interval])
            }
            
        }, 100)


        // add Buttons
        Button.add(()=>can.width-50, ()=>10, ()=>40, ()=>40, "white", {}, ()=>{
            if(!GAME_AUTOSTOP) managePause()
        })
        

        loadLevel(level, ()=>{
            setTimeout(()=>{

                loadPatterns() // create patterns from loaded sprites
               
                localStorage.setItem("current_level", CURRENT_LEVEL)

                loadCoins()

                RUNNING = true
                GAME_AUTOSTOP = false


                run() // start game loop (requestAnimationFrame)    
            }, 500)
          
        }) // load first level and run
    }, 600)
         
}



function loadCoins(){


    let level_coins = min_level_coins
    let collected_level_coins = DataStorage.getLevelData("collected_coins")

    if(collected_level_coins==undefined){
         DataStorage.setLevelData("collected_coins", [])
         collected_level_coins = []
    }
    
    console.log(collected_level_coins.length + " coins collected")

    for (let c=0;c<level_coins;c++){
        if(!collected_level_coins.includes(c)){
            Item.add(2, CURRENT_LEVEL_DATA.coins[c][0], CURRENT_LEVEL_DATA.coins[c][1], 2.5, 2.5, jonas, ()=>{
                //console.log("COIN!!!")
                DataStorage.addLevelData("collected_coins", c)
                total_coins++
            }, "coin")            
        }

    }


    total_coins = 0

    for(let level=1;level<=5;level++){
        total_coins+=DataStorage.getDataFromLevel(level, "collected_coins", []).length
    }

    console.log("total_coins: " + total_coins)

}

function parameters(){
    // set canvas dimensions
    can.width = window.innerWidth
    can.height = window.innerHeight
    can.width = can.clientWidth
    can.height = can.clientHeight



    // set game world parameters
    DM = 10//Math.round(can.height/80)  // rounding error = very big problem, velocity differences!!! DM=10!

    DEFAULT_W = 7*DM   // default player hitbox width

    ORIGIN = [can.width/2 - DEFAULT_W/2 , can.height - 20*DM]

    try{jonas.updateParameters()}catch(e){}

}

function loadSounds(sounds, callback){
    sound_load_counter = 0
    sound_requested_counter = 0

    Sound.load(sounds)
    waitForSounds(callback)

}


function loadSprites(groups, callback){

    img_loaded_counter = 0
    img_requested_counter = 0

   
    for (var group_name of groups){  // every group

        console.log("\n"+group_name+":")

        // create group array ( e.g. "jonas")
        sprites[group_name] = []

        // get the real sprite group names from the sprite dictionary ( e.g. "jonas_front"-> "front")
        for (var actual_group_name in spritedata){
            if (actual_group_name.startsWith(group_name)){
                let sub_group_name = remove_first_occurrence(actual_group_name, group_name+"_")
                
                if(sub_group_name==actual_group_name){ // no sub group
                     // add the sprites
                    for(var i=0; i<spritedata[actual_group_name]; i++){
                        addSprite(actual_group_name+"_"+i+".png", sprites[group_name])
                    }  
                    continue

                }

                console.log(sub_group_name)
                sprites[group_name][sub_group_name] = [] // create sub group array ( e.g. "jonas.front")
                
                // add the sprites
                for(var i=0; i<spritedata[actual_group_name]; i++){
                    addSprite(actual_group_name+"_"+i+".png", sprites[group_name][sub_group_name])
                }
            }
        }        
        
    }

    waitForSprites(()=>{ // create outline sprites
        console.log("\noutline sprites:")
        for (var data of CURRENT_LEVEL_DATA.outline_sprites){
            console.log(data)
            draw_outline(sprites[data[0]][data[1]], data[0], data[1], CURRENT_LEVEL_DATA.secondary_color, data[2])
        }
        waitForSprites(()=>{
            loadUISprites()
            waitForSprites(()=>callback())
        })
    })

}


function loadUISprites(){
    sprites.ui = []
    

    
    img_requested_counter++

    sprites.ui.key = []
    img1 = new Image()
    img1.onload = ()=>{
        img_loaded_counter++
        sprites.ui.key[0] = img1
    }
    img1.onerror = function(){
        console.error("error loading img")
        this.src = "sprites/default.png" // set default texture
    }
    img1.src = "sprites/key.png"




    img_requested_counter++

    sprites.ui.quest = []
    img2 = new Image()
    img2.onload = ()=>{
        img_loaded_counter++
        sprites.ui.quest[0] = img2
    }
    img2.onerror = function(){
        console.error("error loading img")
        this.src = "sprites/default.png" // set default texture
    }
    img2.src = "sprites/quest.png"


    img_requested_counter++

    sprites.ui.quest = []
    img3 = new Image()
    img3.onload = ()=>{
        img_loaded_counter++
        sprites.ui.quest[1] = img3
    }
    img3.onerror = function(){
        console.error("error loading img")
        this.src = "sprites/default.png" // set default texture
    }
    img3.src = "sprites/quest_finished.png"

}


function draw_outline(img, group, index, color, thickness=5) {
  img_requested_counter++

  var canvas = document.createElement('canvas');
  canvas.width=img.width
  canvas.height=img.height

  var ctx = canvas.getContext('2d')

  var dArr = [-1,-1, 0,-1, 1,-1, -1,0, 1,0, -1,1, 0,1, 1,1], // offset array
      s = thickness,  // thickness scale
      i = 0,  // iterator
      x = 0,  // final position
      y = 0;
  
  // draw images at offsets from the array scaled by s
  for(; i < dArr.length; i += 2)
    ctx.drawImage(img, x + dArr[i]*s, y + dArr[i+1]*s);
  
  // fill with color
  ctx.globalCompositeOperation = "source-in";
  ctx.fillStyle = color;
  ctx.fillRect(0,0,canvas.width, canvas.height);
  
  // draw original image in normal mode
  ctx.globalCompositeOperation = "source-over";
  ctx.drawImage(img, x, y);
    
  var img = new Image();

  img.onload = ()=>{
    img_loaded_counter++
  }

  img.setAttribute('crossOrigin', 'anonymous'); // works for me


  img.src = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"); // convert to image
  sprites[group]["outline"] = []
  sprites[group]["outline"][index] = img

}



function waitForSprites(callback){
    //console.log(img_requested_counter, img_loaded_counter)
    if(img_requested_counter==img_loaded_counter){
        console.log("All sprites loaded")
        handleTimeout(()=>{
            img_requested_counter=0
            img_loaded_counter=0
            callback()}, 50)
    } else {
        handleTimeout(()=>waitForSprites(callback), 100)
    }
}

function waitForSounds(callback){
    //console.log(img_requested_counter, img_loaded_counter)
    if(sound_requested_counter==sound_load_counter){
        console.log("All sounds loaded")
        handleTimeout(()=>{
            sound_requested_counter=0
            sound_load_counter=0
            callback()}, 50)
    } else {
        handleTimeout(()=>waitForSounds(callback), 100)
    }
}


function addSprite(src, dest){
    img_requested_counter++
    img = new Image()
    img.onload = ()=>{
        img_loaded_counter++
    }
    img.onerror = function(){
        console.error("error loading img")
        this.src = "sprites/default.png" // set default texture
    }
    img.src = "sprites/" + src // set src (file name)


    dest.push(img); // add to group array

}


function remove_first_occurrence(str, searchstr){
	var index = str.indexOf(searchstr);
	if (index === -1) {
		return str;
	}
	return str.slice(0, index) + str.slice(index + searchstr.length);
}

function flip(src,target){
    var img = new Image();
    img.onload = function(){
        var c = document.createElement('canvas');
        c.width = this.width;
        c.height = this.height;
        var ctx = c.getContext('2d');
        ctx.scale(-1,1);
        ctx.drawImage(this,-this.width,0);
        this.onload = undefined; 
        target.src = c.toDataURL();
    }
    img.src = src;
}

/*****************************************/


function fullscreen(){
    elem = can
    try {
        if (!document.fullscreenElement && !document.mozFullScreenElement &&
        !document.webkitFullscreenElement && !document.msFullscreenElement) {
            if (elem.requestFullscreen) {
              elem.requestFullscreen();
            } else if (elem.msRequestFullscreen) {
              elem.msRequestFullscreen();
            } else if (elem.mozRequestFullScreen) {
              elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
              elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        }   
    } catch(e){}
  
}



/*** GAME ***/

// convert DM/game dimensions into pixel coordinates:

function positionMarker(X, Y, color="red"){
     ctx.fillStyle = color;
     ctx.fillRect(Xtox(X),Ytoy(Y),5,-5);
}


function Xtox(X){
    return ORIGIN[0] + X*DM - jonas.X*10
}
function Ytoy(Y){
    return ORIGIN[1] - Y*DM + jonas.noJumpY*10
}


function xtoX(x){
    return (x+jonas.X*10 - ORIGIN[0]) / DM
}

function ytoY(y){
    return (y - ORIGIN[1] - jonas.noJumpY*10) / -DM 
}

   


function Stos(S){
    return S*DM
}


function X2tox(X2){
    return ORIGIN[0] + X2*DM - jonas.X*3
}
function Y2toy(Y2){
    return ORIGIN[1] - Y2*DM + jonas.noJumpY*3
}

function xtoX2(x){
    return (x+jonas.X*3 - ORIGIN[0]) / DM
}


function X3tox(X3){
    return ORIGIN[0] + X3*DM - jonas.X*6
}
function Y3toy(Y3){
    return ORIGIN[1] - Y3*DM + jonas.noJumpY*6
}
function xtoX3(x){
    return (x+jonas.X*6 - ORIGIN[0]) / DM
}


function X4tox(X4){
    return ORIGIN[0] + X4*DM - jonas.X*9
}
function Y4toy(Y4){
    return ORIGIN[1] - Y4*DM + jonas.noJumpY*9
}


/* classes */


class DataStorage {

    static clearAll(){
        localStorage.clear()
    }
    static clearLevelData(){
        localStorage["level"+CURRENT_LEVEL] = "{}"    
    }


    static getLevelData(key, is_missing_return=undefined){
        try {
            var data = JSON.parse(localStorage.getItem("level"+CURRENT_LEVEL))[key]
        } catch(e){
            localStorage.setItem("level"+CURRENT_LEVEL, "{}")
            return is_missing_return
        }
        return (data==undefined ? is_missing_return : data)
    }

    static getDataFromLevel(level, key, is_missing_return=undefined){
        try {
            var data = JSON.parse(localStorage.getItem("level"+level))[key]
        } catch(e){
            localStorage.setItem("level"+level, "{}")
            return is_missing_return
        }
        return (data==undefined ? is_missing_return : data)
    }




    static addLevelData(key, value){
        try {
            var data = JSON.parse(localStorage.getItem("level"+CURRENT_LEVEL))

        } catch(e){
            localStorage.setItem("level"+CURRENT_LEVEL, "{}")
        }
        if(!Array.isArray(data[key])) data[key] = []
        data[key].push(value)
        localStorage.setItem("level"+CURRENT_LEVEL, JSON.stringify(data))
    }





    static setLevelData(key, value){
        try {
            var data = JSON.parse(localStorage["level"+CURRENT_LEVEL])
        } catch(e){
            var data = {}
        }
        data[key] =  value
        localStorage.setItem("level"+CURRENT_LEVEL, JSON.stringify(data))
    }

    static setGlobalData(key, value){
        localStorage.setItem(key, JSON.stringify(value))
    }

    static getGlobalData(key){
        return JSON.parse(localStorage.getItem(key))
    }
}


class Sound{

    static load(sounds){

        var requests = {}

        for(let src of sounds){
            sound_requested_counter++

            requests[src] = new XMLHttpRequest();
            if(src.includes(".")){
                requests[src].open('GET', "sounds/"+src, true);
            } else {
                requests[src].open('GET', "sounds/"+src+".mp3", true);            
            }
            requests[src].responseType = 'arraybuffer';
            // Decode asynchronously
            requests[src].onload = ()=> {
                audio_ctx.decodeAudioData(requests[src].response, (buffer)=> {
                  sound_buffers[src] = buffer;
                  console.log("loaded SOUND " + src)
                  sound_load_counter++
                });
            }
            requests[src].send();    
        }
       
    }

    static play(sound, volume=1, tag=false, loop=false, mute_background=false, end_callback=false){
       
        if(!tag) tag=sound 

        var source = audio_ctx.createBufferSource(); // creates a sound source
        var gainNode = audio_ctx.createGain();
        source.buffer = sound_buffers[sound]    // tell the source which sound to play
        source.connect(gainNode);       // connect the source to the context's destination (the speakers)
        


        gainNode.connect(audio_ctx.destination);
        gainNode.gain.setValueAtTime(volume, audio_ctx.currentTime);
        
        source.loop = loop;
        sounds[tag] = {default_volume: volume, gainNode: gainNode, source:source}

        if(mute_background!=false){
            Sound.setVolume(mute_background[0], mute_background[1])
        }

        source.onended = function(e) {
            console.log(tag+": play finished, reset "+ mute_background[0])
            if(mute_background!=false) Sound.resetVolume(mute_background[0])
            if(end_callback!=false) end_callback()
            delete sounds[tag]
        }
        

        source.start(0); 
    } 
    
    static setVolume(tag, volume){
        if(sounds[tag]) sounds[tag].gainNode.gain.setValueAtTime(volume, audio_ctx.currentTime);
    }

    static resetVolume(tag){
        sounds[tag].gainNode.gain.setValueAtTime(sounds[tag].default_volume, audio_ctx.currentTime);
    }

    static setDefaultVolume(tag, volume){
        sounds[tag].default_volume = volume
        Sound.setVolume(tag, volume)
    }
    
    static clearSound(tag){
        sounds[tag].source.stop()
    }

    static stop(){
        try{audio_ctx.close()}catch(e){}
    }

    static pause(){
        audio_ctx.suspend()
    }

    static resume(){
        audio_ctx.resume()
    }
}

class Player{
    constructor(X, Y, hairtype="default"){

        
        this.setHairType(hairtype)
        this.setFaceExpression("neutral")

        // appearance
        this.character_body = "personred"
        this.character_face = "jonas"

        // default variables
        this.w = DEFAULT_W
        this.h = DEFAULT_W
        this.W = DEFAULT_W/DM
        this.H = 2*this.W

        //this.hitbox = ()=>[this.X+1, this.X+this.W-1, this.Y+1, this.Y+this.H-1]
        
        this.falldown = true

        this.ground_level = -0.5 // feet are lower than the actual ground


        this.X = X
        this.Y = Y //this.ground_level

        this.hitbox_margin = 0.7
        

        // animation
        this.animate_counter = 0
        this.animation_step = 0
        this.current_body = sprites.personred.front[0]
        this.current_head = sprites.jonas.facefront[this.facetype]

       
        if(this.character_face=="jonas") this.current_hair = sprites.jonas.hairfront[this.hairtype]
        this.head_pos = 0

        this.last_animation_type = null
        this.short_animation_delay = 0
        this.next_random_stop = 100


        this.down = false
        this.sitting = false

        // inventory
        //this.inventory = new Inventory()

        // velocity
        this.vY = 0
        this.vX = 0

        this.default_autojump_X = 0.3
        this.default_autojump_Y = 0.6

        // running / control
        this.canMoveRight = true
        this.canMoveLeft = true

        this.autojump_counter=0
        this.autojump_Y=0, this.autojump_X=0
        
        this.using_vehicle = false

        this.on_ladder = false

        this.speechbubble = 0
        this.speechbubble_size = 0
        this.speech_side = 0


        this.useRelativePos = true

        this.createdGroundParticles = false

    }

    setHairType(type){              
        if(type=="default") this.hairtype = 0
        else if(type=="cap") this.hairtype = 1
        else if(type=="wintercap") this.hairtype = 2
    }



    setFaceExpression(type="neutral"){
        if(type=="neutral") this.facetype = 0
        else if(type=="sleeping") this.facetype = 1
        else if(type=="sad") this.facetype = 2
        else if(type=="scared") this.facetype = 3
        else if(type=="happy") this.facetype = 4
        else if(type=="confused") this.facetype = 5
    }
       

    updateParameters(){
        this.w = DEFAULT_W
        this.h = DEFAULT_W
        this.W = DEFAULT_W/DM
        this.H = 2*this.W  

       
    }

    draw_speechbubble(){
        if(this.speechbubble){

            if(this.speechbubble_size<4){
                this.speechbubble_size*=1.08   
                ctx.globalAlpha = this.speechbubble_size/4     
            } 

            if(this.speech_side==0){
                ctx.drawImage(sprites.speechbubble[0], Xtox(this.X+0.8*this.W), Ytoy(this.Y+this.H*.5), this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);
                ctx.drawImage(sprites.speechbubble[this.speechbubble], Xtox(this.X+0.8*this.W), Ytoy(this.Y+this.H*.5), this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);
                if(DEBUG) ctx.strokeRect(Xtox(this.X+0.8*this.W), Ytoy(this.Y+this.H*.5), this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM)

            } else {
                ctx.drawImage(sprites.speechbubble[1], Xtox(this.X+.2*this.W), Ytoy(this.Y+this.H*.5), -this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);
                ctx.drawImage(sprites.speechbubble[this.speechbubble], Xtox(this.X+.2*this.W), Ytoy(this.Y+this.H*.5), -this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);                
                if(DEBUG) ctx.strokeRect(Xtox(this.X+.2*this.W), Ytoy(this.Y+this.H*.5), -this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM)

            }

            ctx.globalAlpha = 1

        }
    }

    stop_say(){
        this.speechbubble_size = 0
        this.speechbubble = 0
    }



    say(index, side="right"){
        Sound.play("blop", .4)
        this.speechbubble = index
        this.speechbubble_size = 1
        this.speech_side = side=="right" ? 0 : 1
    }


    rideVehicle(X, Y, position_type, position_index, head, vehicle_index, type){
        this.vehicle = type
        this.vehicle_index = vehicle_index
        this.X = X 
        this.Y = Y
        this.using_vehicle = true
        this.current_body = sprites[this.character_body][position_type][position_index]
        if(head == "front"){
            this.lookFront()      
        } else if(head=="right"){
            this.lookRight()    
            if(position_type=="right") {
                this.head_pos=1  
            }
        } else if(head=="left"){
            this.lookLeft()      
        }
    }

    leaveVehicle(){
        this.using_vehicle = false
        this.vehicle = ""
        this.vX=0
        this.vY=0
    }

    lookLeft(){
        this.current_head = sprites[this.character_face].faceleft[0]
        if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairleft[this.hairtype]
        this.head_pos=5
    }

    lookRight(){
        this.current_head = sprites[this.character_face].faceright[0]
        if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairright[this.hairtype]
        this.head_pos=4
    }

    lookFront(){
       this.current_head = sprites[this.character_face].facefront[this.facetype]
       if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairfront[this.hairtype]
       this.head_pos=6 
    }

    lookBack(){
       try{
           this.current_head = sprites[this.character_face].faceback[0]
           if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairback[this.hairtype]
           this.head_pos=0
           this.current_body = sprites[this.character_body].back[0]      
       } catch(e){}

    }

    smooth_scroll(){ 
        let change = 30  

        // smooth y scroll
        if (this.Y==this.noJumpY || Math.abs(this.Y-this.noJumpY)>0.001 ){
            this.noJumpY += (this.Y-this.noJumpY)/change  // 30 needs to be a fix value
            this.useRelativePos = true
        } else {
            this.noJumpY = this.Y
            this.useRelativePos = false
        }    


    }
    

    update(){


        if (this.X+this.vX <0){ // stop at world border
            this.vX = 0
            this.X = 0
        }

        this.smooth_scroll()

        // side collision


        //reset values
        this.canMoveRight = true
        this.canMoveLeft = true

        var x1 = this.vX + this.X+1, // pre-calculate current hitbox
            x2 = this.vX + this.X+this.W-1,
            y1 = this.vY + this.Y+1,
            y2 = this.vY + this.Y+this.H-1

        for (var i=0; i<CURRENT_LEVEL_DATA.side_borders.length;i++){  // each vertical collision line
            var line = CURRENT_LEVEL_DATA.side_borders[i]  
            if((line[0]<x2+0.3 && line[0]>x1-0.3)  // x position intersection
                    && (y1 <= line[2] && line[1] <= y2)) {   // y intersection

                if(x2-line[0] < line[0]-x1){ // right side 
                    if(this.using_vehicle && vehicles[this.vehicle_index].vX>0){ // jonas falls down
                        //vehicles[this.vehicle_index].removeDriverJonas()
                    } else {
                        this.canMoveRight = false
                        if((this.vY==0||this.autojump_Y!=0) && this.vX>0 && line[2]-line[1]<6){  // player moving right and able to jump..
                            this.autojump_Y=this.default_autojump_Y
                            this.autojump_X=this.default_autojump_X
                            this.autojump_counter=20 // start to animate autojump
                            //console.log("jump")
                        }                        
                    }

                } else {   // left side 
                    if(this.using_vehicle && vehicles[this.vehicle_index].vX<0){
                       //vehicles[this.vehicle_index].removeDriverJonas()
                    } else {
                        this.canMoveLeft = false
                        if((this.vY==0||this.autojump_Y!=0) &&  this.vX<0 && line[2]-line[1]<6){ // player moving left and able to jump..
                            this.autojump_Y=this.default_autojump_Y
                            this.autojump_X=-this.default_autojump_X
                            this.autojump_counter=20 // start to animate autojump
                        }
                    }   
                }
            
            }
        }

        // ladders

        if(!this.using_vehicle){
            let on_ladder = false

            for (var i=0; i<CURRENT_LEVEL_DATA.ladders.length;i++){  
                var line = CURRENT_LEVEL_DATA.ladders[i]  

                let l1x = line[0],
                    l1y = line[1]+line[3],
                    r1x = line[0]+line[2],
                    r1y = line[1]

                if(rectsIntersection(l1x, l1y, r1x, r1y, x1, y1+this.H*0.06, x2, y1)){
                    on_ladder = true
                }


            }

            if(on_ladder){
                this.on_ladder=true
                this.in_air = false
                this.lookBack()
            } else {
                this.on_ladder=false
            }            
        }
        



        // each horizontal collision line


        var collision = false
        var not_up = false
        
        
        for (var i=0; i<CURRENT_LEVEL_DATA.ground_borders.length;i++){  
            var line = CURRENT_LEVEL_DATA.ground_borders[i]  
            if((line[0]<y2 && line[0]>y1-0.3)  // y position intersection
                    && (x1+this.hitbox_margin <= line[2] && line[1] <= x2-this.hitbox_margin)) {   // x intersection


                collision = true
                if(!this.in_air) not_up = true
                this.in_air = false
                if(this.vY>0) this.falldown=true

                this.vY = 0

                if(!this.createdGroundParticles){
                    for(var i=0;i<4;i++){
                        BackgroundParticle.create(sprites.particles[level_ground_particles], this.X+this.W/3, this.Y+0.3, 0, 0.1, 20)
                    }
                    this.createdGroundParticles = true
                }
                
                break;
                           
            }
        }

        // hillscapes 

        for (var i=0; i<hillscapes.length;i++){  
            if(y1<hillscapes[i].f(x1) || y1<hillscapes[i].f(x2)){
                collision = true
                if(!this.in_air) not_up = true
                this.in_air = false
                if(this.vY>0) this.falldown=true

                this.vY = 0
                this.Y+= ( hillscapes[i].f(x2) - hillscapes[i].f(this.X+this.W-1))

                if(!this.createdGroundParticles){
                    for(var i=0;i<4;i++){
                        BackgroundParticle.create(sprites.particles[level_ground_particles], this.X+this.W/3, this.Y+0.3, 0, 0.1, 20)
                    }
                    this.createdGroundParticles = true
                }
                
                break;
            }
        }



        if(this.vY!=0){
            this.createdGroundParticles = false
        }

      
      
         // jumpables
        for (var i=0; i<jumpables.length;i++){  
            var line = jumpables[i]  
            if((line[0]<y2 && line[0]>y1-0.3 && this.Y+1>line[0])  // y position intersection
                    && (x1+0.7 <= line[2] && line[1] <= x2-0.7)) {  // x intersection
                    this.vY=jumpables[i][4]
                    jumpables[i][3].push()
            }
        }

        
        // platforms
        for (var i=0; i<CURRENT_LEVEL_DATA.platforms.length;i++){  
            var line = CURRENT_LEVEL_DATA.platforms[i]  
            if((line[0]<y2 && line[0]>y1-0.3 && this.Y+1>line[0])  // y position intersection
                    && (x1+0.7 <= line[2] && line[1] <= x2-0.7)) {   // x intersection

                collision = true
                if(!this.in_air) not_up = true
                this.in_air = false
                if(this.vY>0){
                    this.falldown=true      
                } 

                if(this.vY<0){
                    if(!this.createdGroundParticles){
                        for(var i=0;i<4;i++){
                            BackgroundParticle.create(sprites.particles[level_ground_particles], this.X+this.W/3, this.Y+0.3, 0, 0.1, 15)
                        }
                        this.createdGroundParticles = true
                    }
                }

                this.vY = 0

                
                break;
                           
            }
        }
    

        if(!collision && !this.on_ladder){ //if(!collision && (!DEBUG || this.noDebugFlying)){
            this.vY-= ACCELERATION
            this.in_air = true
            this.falldown=false   
        }

        if(!this.in_air && (!collision || not_up)) this.falldown=false


        // update X / Y (v+autojump)
        

        if (this.autojump_counter>0){  // autojumping
            this.autojump_counter-- 
            this.X+=this.vX+this.autojump_X  
            this.Y+=this.vY+this.autojump_Y
            
        } else {   // normal
            this.X+=this.vX
            this.Y+=this.vY    

            this.autojump_X=0  // reset autojump
            this.autojump_Y=0

        }
       
          
        
        //let gridPosBorder = CURRENT_LEVEL_DATA.ground_borders[Math.round(this.X/10)]  // get collision x grid pos
        //this.groundCollision(gridPosBorder)


    }


    groundCollision(gridPosBorder){

        if ((this.Y + (this.vY-ACCELERATION) < gridPosBorder+this.ground_level) && (this.vY!=0)){ // prevent falling to deep in the next frame->calculate fitting velocityY
           this.Y = gridPosBorder+this.ground_level
           this.vY = 0
           this.in_air = false
        }
        else if(this.Y>gridPosBorder+this.ground_level){
            this.vY-= ACCELERATION
            jonas.in_air = true   
 
        } else  {  // on the ground
            this.vY = 0
            this.in_air = false
        } 
    }

    manage_animation(){

        if(!this.using_vehicle && !this.on_ladder){

            // choose animation type
            if((this.vX>0 && this.vY==0) || this.autojump_X>0) {                    // running right
                this.animate("right")
                this.current_head = sprites[this.character_face].faceright[0]
                if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairright[this.hairtype]
                this.head_pos = 1
                this.short_animation_delay=0  // needed to "reset" jump animation
                this.sitting = false

            } else if ((this.vX<0 && this.vY==0) || this.autojump_X<0){            // running left
                this.animate("left")
                this.current_head = sprites[this.character_face].faceleft[0]
                if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairleft[this.hairtype]
                this.head_pos=2
                this.short_animation_delay=0  // needed to "reset" jump animation
                this.sitting = false
            }

            else if(this.vX>0 && this.vY!=0){               // jumping right
                if (this.short_animation_delay>10){
                   this.current_body = sprites[this.character_body].jumpright[0]
                   this.current_head = sprites[this.character_face].faceright[0]
                   if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairright[this.hairtype]
                   this.head_pos=1
                   this.last_animation_type = "jumpright"
                } else {
                    this.current_body = sprites[this.character_body].right[0]
                    this.short_animation_delay++
                    this.last_animation_type = "right"
                }   
                this.sitting = false
            } 

            else if(this.vX<0 && this.vY!=0){               // jumping left
                if (this.short_animation_delay>10){
                   this.current_body = sprites[this.character_body].jumpleft[0]
                   this.current_head = sprites[this.character_face].faceleft[0]               
                   if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairleft[this.hairtype]
                   this.head_pos=2
                   this.last_animation_type = "jumpleft"
                } else {
                    this.current_body = sprites[this.character_body].left[0]
                    this.short_animation_delay++
                    this.last_animation_type = "left"
                }          
                this.sitting = false
            } 

            else if(this.vY!=0){                            // jumping front
                if (this.short_animation_delay>10){
                   this.current_body = sprites[this.character_body].front[1]
                   this.current_head = sprites[this.character_face].facefront[this.facetype]        
                   if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairfront[this.hairtype]
                   this.head_pos=0
                   this.last_animation_type = "jumpfront"
                } else {
                    this.current_body = sprites[this.character_body].front[this.facetype]
                    this.head_pos=0
                    this.short_animation_delay++
                    this.last_animation_type = "front"
                }               
                this.sitting = false

            } 

            else if (this.down){  // down
                this.current_body = sprites[this.character_body].front[2]
                this.current_head = sprites[this.character_face].facefront[this.facetype]
                if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairfront[this.hairtype]
                this.falldown=false  
                this.head_pos=3
                this.sitting = false
            } 

            else {                                         // standing and blinking
                this.current_body = this.sitting ? sprites[this.character_body].front[2] : sprites[this.character_body].front[0]
                if(this.character_face=="jonas") this.current_hair = sprites[this.character_face].hairfront[this.hairtype]
                this.head_pos = this.sitting ? 3 : 0
                if(this.facetype==0){
                    this.animate_head_random("facefront", 0, 1)
                } else {
                    this.current_head = sprites[this.character_face].facefront[this.facetype]
                }

                this.short_animation_delay=0  // needed to "reset" jump animation
            }
        }


    }


   
    draw() {


            this.manage_animation() // body/head animation

            if (jonas.Y<-200){ // check player height, only for jonas!
                
                restartLevel("Bleibe auf den Wegen")
            }

            var yPos = (this.in_air || this.useRelativePos) ? Ytoy(this.Y) : ORIGIN[1]
            if(this.sitting) yPos-=1.8*DM
            // head

            if (this.head_pos==1){
                this.draw_head(ORIGIN[0]+1.1*DM, yPos-1.88*this.h, this.w, this.h);
            } else if (this.head_pos==2){
               this.draw_head(ORIGIN[0]-1.2*DM, yPos-1.88*this.h, this.w, this.h);
            } else if (this.head_pos==3){
                this.draw_head(ORIGIN[0], yPos-1.65*this.h, this.w, this.h);
            } else if (this.head_pos==4){
                this.draw_head(ORIGIN[0]-0.3*DM, yPos-1.89*this.h, this.w, this.h);
            } else if (this.head_pos==5){
                this.draw_head(ORIGIN[0]-1.4*DM, yPos-1.89*this.h, this.w, this.h);
            } else if (this.head_pos==6){
                this.draw_head(ORIGIN[0]-0.6*DM, yPos-1.89*this.h, this.w, this.h);
            } else {// default
                this.draw_head(ORIGIN[0], yPos-1.88*this.h, this.w, this.h);
            }

            // body
            ctx.drawImage(this.current_body, ORIGIN[0], yPos-this.h, this.w, this.h);


            if (DEBUG) {  // hitboxes

                if(!this.canMoveLeft || !this.canMoveRight){
                    ctx.strokeStyle = "red"  // real hitbox
                    ctx.lineWidth = 2   
                } else if(this.in_air) {
                    ctx.strokeStyle = "blue"  
                } else if(this.on_ladder){
                    ctx.lineWidth = 2   
                    ctx.strokeStyle = "cyan"
                } else {
                    ctx.strokeStyle = "magenta"  
                }

                ctx.beginPath()          
                ctx.rect(Xtox(this.X+1), yPos-1*DM, (this.W-2)*DM, (-this.H+2)*DM);
                ctx.stroke();
                ctx.closePath()

            }

           



    }


    draw_head(x,y,w,h){
        ctx.drawImage(this.current_head, x, y, w, h);
        if(this.character_face=="jonas") ctx.drawImage(this.current_hair, x, y, w, h);
    }



    animate(type){

        if(this.last_animation_type != type) { // reset if new animation type
            this.animate_counter=9
            this.last_animation_type = type
        }

        if (this.animate_counter>6){ // animate every n frames
            this.animate_counter=0 // reset counter
            if(this.animation_step+1<sprites[this.character_body][type].length) {  // sprite frame available / start from beginning
                this.animation_step++
            } else { this.animation_step=0 } 
            this.current_body = sprites[this.character_body][type][this.animation_step]
        }
        this.animate_counter++

    }

    animate_head_random(type, normal, blink){

        if(this.last_animation_type != type) {  // reset if new animation type
            this.animate_counter=10
            this.last_animation_type = type
        }


        if(this.animate_counter==10){  // open closed eyes again
            this.current_head = sprites[this.character_face][type][normal] 
            this.animate_counter++
        }

        else if (this.animate_counter>this.next_random_stop && Math.random()>0.9){ // animate every n frames

            this.current_head = sprites[this.character_face][type][blink]  // close eyes
            this.animate_counter=0  // reset counter
            this.next_random_stop = 100 + Math.random()*200
        } 

        else {
            this.animate_counter++
        }
    }

}

class Inventory {

    constructor(){
        this.items = []
    }

    setItems(items){
        this.items = items
    }

    add(item){
        var already_existing = false
        for(var i of this.items){
            if(i[0].name==item.name){
                i[1]++
                already_existing = true
                break
            }
        }

        if(!already_existing){
            this.items.push([item, 1])
        }
    }
}


class Drone {
    static create(X, Y){
        vehicles.push(new Drone(X, Y))
    }

    constructor(X, Y){
        this.X = X
        this.Y = Y

        this.vX = 0
        this.vY = 0

        this.H = 10
        this.W = 30
        
        this.jonasSeat = {X:11.5, Y:5}


        this.flying=false
  

        this.power_status = 1
        
        this.crashed=false
        this.crash_timeout = false
        this.crash_reason = "Pass auf, dass die Drohne nirgendwo dagegen fliegt"

        this.angle = 0
        this.rotate = 0

        this.burned = false
        this.burning = false
        
        this.labor_quest_shown = false

        this.make_actionable()
    }


    make_actionable(){ 

        this.inv_actionable = new Actionable(this.X, this.Y, this.W, this.H, null, null, 10, 10, 20, 5, "Drohne benutzen", false, (object)=>{
            this.inv_actionable = null
            this.addDriverJonas("right", 0, "right")

            if(!this.labor_quest_shown){
                showQuest("Erreiche das geheime Labor")
                this.labor_quest_shown = true
            } 
            
        })    

 
    }


    addDriverJonas(position_type, position_index, head){
        this.sprite = sprites.drone[0]

        //this.vX=.8
        this.flying = true    
        Sound.play("drone", .08, "drone", true)    
        if(this.burning) Sound.setVolume("drone", 0)
           
        var index = vehicles.indexOf(this)
        jonas.rideVehicle(this.X+this.jonasSeat.X, this.Y+this.jonasSeat.Y, position_type, position_index, head, index, "drone")
        this.jonasDriving = true
        this.inv_actionable = null
        setKeyHint(["E", "Absteigen"])
        constant_keyhint = true
        actionable_in_focus.action_callback = ()=>{
            this.removeDriverJonas()
        }   
    }

    removeDriverJonas(){
        constant_keyhint = false
        jonas.leaveVehicle()
        this.flying=false
        Sound.clearSound("drone")

        this.jonasDriving = false
        this.make_actionable()
    }



    update(){
    
         if(!this.flying){
            this.vY=-.5
         }
        
        //burning 
        if(!this.burned && this.X>518&&this.Y<522){
            console.log("burning")
            this.burned = true
            this.burning = true
            Sound.play("alarm", .2, "alarm", true)
            let down_interval = handleInterval(()=>{
                if(this.vY==0){
                    showQuest("Mache die Drohne wieder flugfähig")
                    clearInterval(intervals[down_interval])
                }
            }, 1000)

        }
        
        if(this.burning){
            this.vY=-.5
            this.vX=0
            this.angle*=.6
            Particle.create(sprites.particles[8], this.X+3, this.Y+7, -0.1, 0.3, 30, 2)
        }
    




        // power_status
        if(!this.burning && this.flying && this.jonasDriving) this.power_status-=.001

        if(this.power_status<0) this.power_status=0

        if(this.power_status==0 ){
            this.flying=false
            this.vY=-5
            this.vX=.01
            this.crashed=true
            this.rotate = 0.2
            this.crash_reason = "Sammle Akkus, damit die Drohne weiterfliegen kann"
        }
    
        if(!this.crashed && this.flying && Math.abs(this.vX)>0.15 || Math.abs(this.vY)>0.2){
            Sound.setVolume("drone", .2)   
        } else {
            Sound.setVolume("drone", .08)   
        }


        
   
        //if(this.vX!=0)this.vX+=.0001

        let x1 = this.X+this.vX,
            x2 = this.X+this.W+this.vX,
            y1 = this.Y+this.vY,
            y2 = this.Y+this.H+this.vY
        

        let ground_collision = false

        for (var i=0; i<CURRENT_LEVEL_DATA.ground_borders.length;i++){  
            var line = CURRENT_LEVEL_DATA.ground_borders[i]  
            if((line[0]<y2 && line[0]>y1-0.3)  // y position intersection
                    && (x1 <= line[2] && line[1] <= x2)) {   // x intersection
                
                
                if(this.vY>0){
                    this.crashed=true
                    this.flying=false
                    this.rotate = -0.1
                } else if(this.vY<=0) {
                    ground_collision=true
                }

                this.vY = 0

                break;
                           
            }
        }


        for (var i=0; i<CURRENT_LEVEL_DATA.side_borders.length;i++){  
            var line = CURRENT_LEVEL_DATA.side_borders[i]
            if( (y1 <= line[2] && line[1] <= y2)  &&
                ((this.X+this.vX<line[0] && this.X+this.vX+this.W>line[0]) ||
                (this.X+this.W+this.vX>line[0] && this.X+this.vX<line[0]) )) {
                    
                    this.vX=-0.5*this.vX

                    if(!ground_collision){
                        this.vY=-5
                        this.rotate = 0.15

                        this.flying=false
                        this.crashed=true
                    }
            } 
        }




        this.vX*=0.96
        this.vY*=0.96

        if(this.vY>1.5){
            this.vY=1.5    
        } 
        else if(this.vY<-.6) this.vY=-.6

        if(this.vX>2) this.vX=2
        else if(this.vX<-2) this.vX=-2

        if(!ground_collision && this.Y+this.vY<=100 && this.Y+this.vY>=-100){
            this.Y+=this.vY
        } else {
            this.vY=0
        }        

       if(Math.abs(this.vX)<0.0001){
           this.vX=0
       } 

       if(this.vX>0.3){
           this.rotate=0.005
       } else if (this.vX<-0.3){
           this.rotate=-0.005
       } else {
           this.rotate=0
           this.angle*=0.94//rotate back
       }


        this.X+=this.vX


        if(this.jonasDriving){
            jonas.X = this.X+this.jonasSeat.X
            jonas.Y = this.Y+this.jonasSeat.Y
        }


        if(this.crashed){
            this.Y-=1
            if(!this.crash_timeout){
                this.crash_timeout=true
                console.log("DRONE CRASHED", this.crash_reason)
                Sound.play("crash")
                setTimeout(()=>restartLevel(this.crash_reason), 300)
            } 
        }
               


        
    }

    draw(){


        

        if(!this.crashed){
            if(this.angle+this.rotate<0.3 && this.angle+this.rotate>-0.3){
                this.angle+=this.rotate
            }
        } else {
            this.angle+=this.rotate
        }

        
        if(!this.jonasDriving && this.inv_actionable!=null){
            this.inv_actionable.X = this.X
            this.inv_actionable.Y = this.Y
            if(this.inv_actionable.check(jonas.X, jonas.Y)){
                this.sprite = sprites.drone.outline[0]
            } else {
                this.sprite = sprites.drone[0]
            }
        }
        
        if(this.angle!=0){
            this.rotationCenterX = this.X+this.W/2
            this.rotationCenterY = this.Y+this.H/2
            ctx.save();
            ctx.translate(Xtox(this.rotationCenterX), Ytoy(this.rotationCenterY) );
            ctx.rotate( this.angle);
            ctx.translate( -Xtox(this.rotationCenterX), -Ytoy(this.rotationCenterY) );   
        }


        if(this.jonasDriving){
            jonas.draw()
        }


        if (this.inv_actionable!=null) this.inv_actionable.draw()

        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
        
        // rotors back   
        let i1 = (!this.flying||this.crashed||this.burning) ? 1 : 2 
        let i2 = (!this.flying||this.crashed||this.burning) ? 3 : 2
        ctx.drawImage(sprites.drone[i2], Xtox(this.X+.06*this.W), Ytoy(this.Y+1.15*this.H), 0.33*this.W*DM, .12*this.W*DM)
        ctx.drawImage(sprites.drone[i1], Xtox(this.X+0.605*this.W), Ytoy(this.Y+1.15*this.H), 0.33*this.W*DM, .12*this.W*DM)
        
        // rotors front
        ctx.drawImage(sprites.drone[i1], Xtox(this.X-0.16*this.W), Ytoy(this.Y+0.97*this.H), 0.495*this.W*DM, .18*this.W*DM)
        ctx.drawImage(sprites.drone[i2], Xtox(this.X+0.65*this.W), Ytoy(this.Y+0.97*this.H), 0.495*this.W*DM, .18*this.W*DM)

        
        if(DEBUG){
            ctx.strokeStyle = "red"
            ctx.strokeRect(Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
        }

        if(this.angle!=0) ctx.restore()

    }
}



class Person extends Player{

    static add(X, Y, name, face, body){
        persons[name] = new Person(X, Y, name, face, body)
    }
    
    static add_hubatz(X, Y){
        persons["hubatz"] = new Hubatz(X, Y)
    }

    constructor(X, Y, name, face="jonas", body="personred"){
        super(X, Y)

        this.name = name

         // animation
        this.current_body = sprites[body].front[0]
        this.current_head = sprites[face].facefront[0]



        this.character_body = body
        this.character_face = face

        this.wait_follow = 0
        this.player_direction = 0
        this.finding_jonas = false

        this.following_jonas = false
        this.follow_jonas_callback = null

        this.speechbubble = 0
        this.speechbubble_size = 0
        this.speech_side = 0

        this.jumping_to_jonas = false
    }
    
    say(index, side="right"){
        Sound.play("blop", .4)
        this.speechbubble = index
        this.speechbubble_size = 1
        this.speech_side = side=="right" ? 0 : 1
    }

    followJonas(callback){
        this.follow_jonas_callback = callback
        this.following_jonas = true
    }

    follow_player(){
        if (jonas.X-14>this.X){
            this.wait_follow++
            this.player_direction=1 
            if(this.canMoveRight && (this.vX!=0 || this.wait_follow>30)){    
                this.wait_follow = 0
                this.vX=0.4  
            } else {
                this.vX=0
            }
            this.finding_jonas = 1
            
        } else if (jonas.X+14<this.X){
            this.wait_follow++
            this.player_direction=-1 
            if(this.canMoveLeft && (this.vX!=0 || this.wait_follow>30)){
                this.wait_follow = 0
                this.vX=-0.4
            } else {
                this.vX=0
            }
            this.finding_jonas = -1
        } else {
            if(jonas.X>this.X){
                this.player_direction=1 
            } else if(jonas.X<this.X){
                this.player_direction=-1 
            }
            this.vX=0
            
            if(Math.abs(this.lastPlayerPosReached-jonas.X)>40){ // was player far enough away ? 
                if (this.finding_jonas==-1){ 
                    this.finding_jonas = false
                } else if (this.finding_jonas==1){
                    this.finding_jonas = false
                }   
                
            }
            try{this.follow_jonas_callback()}catch(e){}

            this.lastPlayerPosReached = jonas.X

        }  

        if(!this.jumping_to_jonas && jonas.Y>this.Y+15){
            this.jumping_to_jonas = true
            this.vY=1.3
        }

        if(this.vY==0) this.jumping_to_jonas = false
    }

    update(){// #changed
        super.update()
        /*if(!this.canMoveRight){
            this.vX=0
        }
        if(!this.canMoveLeft){
            this.vY=0
        }*/
    }

    
    
    draw() { // #changed
                
        if(this.following_jonas) this.follow_player()
        this.manage_animation() // body/head animation
    
        // head
        
        if (this.head_pos==1){
            if(this.character_body=="personbrown"){
                ctx.drawImage(this.current_head, Xtox(this.X)+1.1*DM, Ytoy(this.Y)-1.78*this.h, this.w, this.h);
            } else {
                ctx.drawImage(this.current_head, Xtox(this.X)+1.1*DM, Ytoy(this.Y)-1.88*this.h, this.w, this.h);                
            }
        } else if (this.head_pos==2){
            if(this.character_body=="personbrown"){
                ctx.drawImage(this.current_head, Xtox(this.X)-.8*DM, Ytoy(this.Y)-1.78*this.h, this.w, this.h);
            } else {
                ctx.drawImage(this.current_head, Xtox(this.X)-1.3*DM, Ytoy(this.Y)-1.88*this.h, this.w, this.h);                
            }
        } else if (this.head_pos==3){
            ctx.drawImage(this.current_head, Xtox(this.X), Ytoy(this.Y)-1.65*this.h, this.w, this.h);
        } else {// default
            if(this.character_body=="personbrown"){
                ctx.drawImage(this.current_head, Xtox(this.X), Ytoy(this.Y)-1.78*this.h, this.w, this.h);
            } else {
                ctx.drawImage(this.current_head, Xtox(this.X), Ytoy(this.Y)-1.88*this.h, this.w, this.h);                
            }
        }

        // body
        ctx.drawImage(this.current_body, Xtox(this.X), Ytoy(this.Y)-this.h, this.w, this.h);
       

        if (DEBUG) {  // hitboxes
          
            if(!this.canMoveLeft || !this.canMoveRight){
                ctx.strokeStyle = "red"  // real hitbox
                ctx.lineWidth = 2   
            } else if(this.in_air) {
                ctx.strokeStyle = "blue"  
            } else {
                ctx.strokeStyle = "magenta"  
            }
            
            ctx.beginPath()          
            ctx.rect(Xtox(this.X+1), Ytoy(this.Y)-1*DM, (this.W-2)*DM, (-this.H+2)*DM);
            ctx.stroke();
            ctx.closePath()
        
        }


    }

    draw_speechbubble(){
        if(this.speechbubble){

            if(this.speechbubble_size<4){
                this.speechbubble_size*=1.08   
                ctx.globalAlpha = this.speechbubble_size/4     
            } 

            if(this.speech_side==0){
                ctx.drawImage(sprites.speechbubble[0], Xtox(this.X+0.8*this.W), Ytoy(this.Y+this.H*.5), this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);
                ctx.drawImage(sprites.speechbubble[this.speechbubble], Xtox(this.X+0.8*this.W), Ytoy(this.Y+this.H*.5), this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);
                if(DEBUG) ctx.strokeRect(Xtox(this.X+0.8*this.W), Ytoy(this.Y+this.H*.5), this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM)

            } else {
                ctx.drawImage(sprites.speechbubble[1], Xtox(this.X+.2*this.W), Ytoy(this.Y+this.H*.5), -this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);
                ctx.drawImage(sprites.speechbubble[this.speechbubble], Xtox(this.X+.2*this.W), Ytoy(this.Y+this.H*.5), -this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM);                
                if(DEBUG) ctx.strokeRect(Xtox(this.X+.2*this.W), Ytoy(this.Y+this.H*.5), -this.speechbubble_size*2*DM, -this.speechbubble_size*3*DM)

            }

            ctx.globalAlpha = 1

        }
    }

    stop_say(){
        this.speechbubble_size = 0
        this.speechbubble = 0
    }
}



class Hubatz extends Player{

    constructor(X, Y){
        super(X, Y)
        this.W = 7.7
        this.H = 7

        this.X = X
        this.Y = Y

        this.hitbox_margin = 1.8

        this.wait_follow = 0
        this.wait_sit = 0
        this.player_direction=0


        // velocity
        this.vY = 0
        this.vX = 0

        this.default_autojump_Y = 1.2
        this.default_autojump_X = 0.15

        
        this.called_back = false

        this.current_body = sprites.hubatz.right[0]

        this.follow_jonas_callback = ()=>{}

        this.character_body = "hubatz"

        // prevent floating in debug mode
        this.noDebugFlying = true
        
        this.finding_jonas = false
        this.lastPlayerPosReached = -100

    }

     rideVehicle(X, Y, position_type, position_index, head, vehicle_index, type){
        this.vehicle = type
        this.vehicle_index = vehicle_index
        this.X = X 
        this.Y = Y
        this.using_vehicle = true
        this.current_body = sprites[this.character_body].look[1]
    }

    followJonas(callback){
        this.follow_jonas_callback = callback
        this.called_back = false
    }

    follow_player(){
        if (jonas.X-8>this.X){
            this.wait_follow++
            this.player_direction=1 
            if(this.canMoveRight && (this.vX!=0 || this.wait_follow>30)){    
                this.wait_follow = 0
                this.vX=0.2
            } else {
                this.vX=0
            }
            this.finding_jonas = 1
            
        } else if (jonas.X+8<this.X){
            this.wait_follow++
            this.player_direction=-1 
            if(this.canMoveLeft && (this.vX!=0 || this.wait_follow>30)){
                this.wait_follow = 0
                this.vX=-0.2
            } else {
                this.vX=0
            }
            this.finding_jonas = -1
        } else {
            if(jonas.X>this.X){
                this.player_direction=1 
            } else if(jonas.X<this.X){
                this.player_direction=-1 
            }
            this.vX=0
            
            if(Math.abs(this.lastPlayerPosReached-jonas.X)>40){ // was player far enough away ? 
                if (this.finding_jonas==-1){  // heart particles
                    this.finding_jonas = false
                    Particle.create_delay(300, sprites.particles[0], this.X, this.Y+this.H)          
                } else if (this.finding_jonas==1){
                    Particle.create_delay(300, sprites.particles[0], this.X+this.W/2, this.Y+this.H)          
                    this.finding_jonas = false
                }   
                Sound.play("miau", .1)
            }
            
            if(!this.called_back){
                this.called_back=true
                try{this.follow_jonas_callback()}catch(e){}
            }

            this.lastPlayerPosReached = jonas.X

        }  
    }

    manage_animation(){ // #changed
        
        this.follow_player()

        // choose animation type
        if((this.vX>0 && this.vY==0) || this.autojump_X>0) {                    // running right
            this.animate("right")
            this.wait_sit = 0                                      
            this.short_animation_delay=0  // needed to "reset" jump animation

        } else if ((this.vX<0 && this.vY==0) || this.autojump_X<0){            // running left
            this.animate("left")
            this.wait_sit = 0                                      
            this.short_animation_delay=0  // needed to "reset" jump animation

        }

        else if(this.vX>0 && this.vY!=0){               // jumping right
            if (this.short_animation_delay>10){
               this.current_body = sprites[this.character_body].right[0]
               this.wait_sit = 0                                                 
               this.last_animation_type = "jump"
            } else {
                this.current_body = sprites[this.character_body].right[0]
                this.short_animation_delay++
            }   
        } 

        else if(this.vX<0 && this.vY!=0){               // jumping left
            if (this.short_animation_delay>10){
               this.current_body = sprites[this.character_body].left[0]
               this.wait_sit = 0                                      
               this.last_animation_type = "jump"
            } else {
                this.current_body = sprites[this.character_body].left[0]
                this.short_animation_delay++
            }          
        } 

        else if(this.vY!=0){                            // jumping front
            if (this.short_animation_delay>10){
               //this.current_body = sprites[this.character_body].front[0]
               this.wait_sit = 0                                      
               this.last_animation_type = "jump"
            } else {
                //this.current_body = sprites[this.character_body].left[0]
                this.short_animation_delay++
            }               
          
        } 


        else {  // standing and blinking
            this.wait_sit++
            if(this.wait_sit>200){  
                this.animate_random("front", 0, 2, 1)
                this.short_animation_delay=0  // needed to "reset" jump animation
            } else {
                if (this.player_direction==-1){
                    this.current_body = sprites[this.character_body].look[0]
                } else if (this.player_direction==1){
                    this.current_body = sprites[this.character_body].look[1]
                }
                
            }
        }
        


    }

    draw() { // #changed
               
        if(!this.using_vehicle) this.manage_animation() // body/head animation
        // body

        ctx.beginPath()
        ctx.drawImage(this.current_body, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM);

        if (DEBUG){
            ctx.strokeStyle = "purple"
            ctx.rect(Xtox(this.X), Ytoy(this.Y+this.H), (this.W)*DM, (this.H)*DM);
            ctx.stroke()
        }
        ctx.closePath()

    }

    animate_random(type, normal, normal2, blink){  // #changed

 
        
        if(this.animate_counter==8){  // open closed eyes again
            this.current_body = Math.random()>0.5 ? sprites[this.character_body][type][normal] : sprites[this.character_body][type][normal2]   
            this.animate_counter++
        } 


        else if (this.animate_counter>this.next_random_stop&& Math.random()>0.9){ // animate every n frames
            this.current_body = sprites[this.character_body][type][blink]  // close eyes

            this.animate_counter=0  // reset counter
            this.next_random_stop = 50 + Math.random()*150
        } 

        else {
            this.animate_counter++
        }
    }
}



class CodeWindow{

    static show(code, right, wrong){
        code_window = new CodeWindow(code, right, wrong)
    }

    static reset(){
        for(var i=code_window.button_indices.length-1;i>=0;i--){
            buttons.splice(code_window.button_indices[i], 1)
        }
     
        code_window = false
    }


    constructor(code, right){

        this.keys = ["1","2","3","4","5","6","7","8","9","Escape","0","Backspace"]
        this.code = code
        this.right = right
        this.sprite = sprites.codeinput[0]

        this.input_code = ""

        this.color = "red"

        this.h = ()=>0.8*can.height
        this.w = ()=>0.8*can.height
        this.shift_x = ()=>(can.width-this.w())/2
        this.shift_y = ()=>(can.height-this.h())/2

        this.button_indices = []

        this.addButtons()

        this.opening = false
    }
    
    addButtons(){
        
        let size = 0.129

        let numbers = this.keys
        let c = 0

        for(let y=0; y<4;y++){
            for(let x=0; x<3;x++){
                console.log("add " +numbers[c])

                this.button_indices.push(Button.add( ()=>code_window.shift_x()+code_window.w()*0.305+x*code_window.w()*size, ()=>code_window.shift_y()+code_window.w()*0.352+y*code_window.w()*size, ()=>code_window.w()*size, ()=>code_window.w()*size, "white", {key:numbers[c]}, (data)=>{
                    console.log(data.key)
                    code_window.input(data.key)
                })) 
                c++

            }
        }

      
    }

    input(number){
        if(number=="Escape"){
            CodeWindow.reset()
        } else if (number=="Backspace"){
            this.input_code = this.input_code.slice(0, -1)
        } else if(this.input_code.length<6){
            Sound.play("beep", .6)
            this.input_code+=number       
        } else {
            Sound.play("error", .6)
        }

        if(this.input_code==this.code){
            Sound.play("success", .8)
            Sound.play("electric_door")
   
            this.color = "green"
            if(!this.opening){
                this.opening=true
                handleTimeout(()=>{
                    this.right()
                    CodeWindow.reset()
                },1000)
            }
        } else if(!this.opening){
            this.color = "red"
        }
    }



    draw(){
        
        let w = this.w(),
            h = this.h(),
            shift_x = this.shift_x(),
            shift_y = this.shift_y()

        ctx.globalAlpha=0.5
        ctx.fillStyle = "black"
        ctx.fillRect(0, 0, can.width, can.height)
        ctx.globalAlpha=1

        ctx.drawImage(this.sprite, shift_x, shift_y, w, h)
        

        
        ctx.fillStyle = this.color
        ctx.fillRect(shift_x+w*0.31, shift_y+h*0.19, w*0.37, h*0.09)

        ctx.font=`${w/20}px Monospace`;
        ctx.textAlign="right"
        ctx.fillStyle="white"

        ctx.fillText(this.input_code, shift_x+w*0.66, shift_y+h*0.25)
    }
}


function hideCodeWindow(){
    code_window = false
}

function setKeyHint(hint){
    if(hint[1]!=key_hint[1]){
        console.log(hint)
        calculateKeyHintTextWidth(hint[1])
        key_hint = hint        
    }
}

function resetKeyHint(){
    actionable_in_focus = {}
    key_hint = []
}

function calculateKeyHintTextWidth(text){
    // calculate with of text
    key_hint_width = 0
    ctx.font="15px GameFont";
    for (var i=0; i<text.length; ++i) {
      key_hint_width += Math.round(ctx.measureText(text[i]).width);
    }    
    key_hint_width+=15
}


function showGiftCardLink(){
    show_code = true
    loaded_code_url = false
}


function showQuest(text){

    Sound.play("notification")

    quest_finished_count=0
    quest_finished=false
            
    quest_existing = true
    quest = {text:text}
    quest_count = 0
    quest_text_width=0
    

    // calculate with of text etc.
    ctx.font="16px GameFont";
    for (var i=0; i<text.length; ++i) {
      quest_text_width += Math.round(ctx.measureText(text[i]).width);
    }
    
    quest_text_width+=80
}

function questFinished(){
    if(!quest_finished){
        Sound.play("success", .7, "success", false, ["background_music", .1],false)
        quest_finished = true
    }  
}

// collisions


function rectsIntersection(l1x, l1y, r1x, r1y, l2x, l2y, r2x, r2y){
    // If one rectangle is on left side of other
    if (l1x > r2x || l2x > r1x)
        return false;
 
    // If one rectangle is above other
    if (l1y < r2y || l2y < r1y)
        return false;
 
    return true;
}

function rectangleLineCollision(x1, y1, x2, y2, rx1, ry1, rx2, ry2){

    var left =   lineLineCollision(x1,y1,x2,y2, rx1,ry1,rx1,ry2);
    var right =  lineLineCollision(x1,y1,x2,y2, rx2,ry1,rx2,ry2);
    var top =    lineLineCollision(x1,y1,x2,y2, rx1,ry1,rx2,ry1);
    var bottom = lineLineCollision(x1,y1,x2,y2, rx1,ry2,rx2,ry2);

    
    return [left, right, top, bottom]
}

function lineLineCollision(x1, y1, x2, y2, x3, y3, x4, y4) {

  // calculate the direction of the lines
  var uA = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
  var uB = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));

  if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
    return true;
  }
  return false;
}



function restartLevel(tip){

    if(!GAME_OVER){

        if(tip=="Bleibe auf den Wegen"){
            Sound.play("fall", .6)
        }

        code_window=false

        CONTROLLABLE=false

        GAME_OVER = true

        gameover_message = gameover_messages[Math.floor(Math.random()*gameover_messages.length)];
        gameover_tip = tip

        // adapt background music volume
        /*Sound.play("gameover", .2, "gameover", false, ["background_music", .2], ()=>{
            console.log("played gameover")
            Sound.setDefaultVolume("background_music", 0.3)    
        })*/
        

        // reset level and restart
        gameover_animation_finished = ()=>{
            RUNNING = false
            
            START_LEVEL(CURRENT_LEVEL, true)
        }

        //gameover_animation_finished()
    }
    

    
   
}

class HillScape{

    static create(f, color1, color2, top_sprites, callback, noise_scale=1){
        hillscapes.push(new HillScape(f, color1, color2, top_sprites, noise_scale))
        callback()
    }

    constructor(f, color1, color2, top_sprites, noise_scale=1){
        this.f = f       
        this.color2 = color2
        this.top_sprites = top_sprites
        
        this.noise_scale = noise_scale
        this.color1 =  color1

    }

    draw(){
        ctx.fillStyle = this.color1
        ctx.strokeStyle = this.color2
        ctx.beginPath()
        ctx.moveTo(-20, Ytoy(this.f(xtoX(-20))-1));

        for(var X=xtoX(0); X<xtoX(can.width);X+=0.05){
            var Y = this.f(X)+noise.getVal(X/this.noise_scale)*0.5
            ctx.lineTo(Xtox(X),Ytoy(Y-1)) // ()
        }

        ctx.lineTo(can.width+20, Ytoy(this.f(xtoX(can.width))-1))
        ctx.lineTo(can.width+20, Ytoy(0))
        ctx.lineTo(can.width+20, can.height+10)
        ctx.lineTo(-20, can.height+10)
        ctx.fill()
        
        ctx.lineWidth=1.4*DM
        ctx.stroke()
        ctx.lineWidth=1

        //console.log(cs)

        if(DEBUG) {
            ctx.strokeStyle = "red"
            ctx.stroke()
        }

        if(this.top_sprites!=false){
            for(var X=Math.round(xtoX(0)); X<xtoX(can.width);X+=1){
                var n = noise.getVal(X/this.noise_scale)
                if(n<1/3) var i=0
                else if(n<2/3) var i=1
                else var i=2
                ctx.drawImage(this.top_sprites[i], Xtox(X),Ytoy(this.f(X)+2+n*0.2), 1.5*DM, 3*DM)
            }            
        }



    }
}

var Noise = function() {
    var MAX_VERTICES = 256;
    var MAX_VERTICES_MASK = MAX_VERTICES -1;
    var amplitude = 1;
    var scale = 1;

    var r = [];

    for ( var i = 0; i < MAX_VERTICES; ++i ) {
        r.push(Math.random());
    }

    var getVal = function( x ){
        var scaledX = x * scale;
        var xFloor = Math.floor(scaledX);
        var t = scaledX - xFloor;
        var tRemapSmoothstep = t * t * ( 3 - 2 * t );

        /// Modulo using &
        var xMin = xFloor & MAX_VERTICES_MASK;
        var xMax = ( xMin + 1 ) & MAX_VERTICES_MASK;

        var y = lerp( r[ xMin ], r[ xMax ], tRemapSmoothstep );

        return y * amplitude;
    };

    /**
    * Linear interpolation function.
    * @param a The lower integer value
    * @param b The upper integer value
    * @param t The value between the two
    * @returns {number}
    */
    var lerp = function(a, b, t ) {
        return a * ( 1 - t ) + b * t;
    };

    // return the API
    return {
        getVal: getVal,
        setAmplitude: function(newAmplitude) {
            amplitude = newAmplitude;
        },
        setScale: function(newScale) {
            scale = newScale;
        }
    };
};



class Object2d{

    static create(X, Y, W, H, sprite_group, sprite_index){
        objects.push(new Object2d(X, Y, W, H, sprite_group, sprite_index))
    }

    constructor(X, Y, W, H, sprite_group, sprite_index){
        this.X = X
        this.Y = Y
        this.W = W
        this.H = H

        this.sprite_group = sprite_group
        this.sprite_index = sprite_index

        this.disappearing = false
        this.disappear_v = 0.6
        
        if(this.sprite_group!=null && this.sprite_index!=null)
            this.sprite = sprites[sprite_group][sprite_index]
       
    }
   
    remove(){
        try{
            var i = objects.indexOf(this)
            objects.splice(i, 1)
        } catch(e){}
    }


    disappear(callback=null){
        console.log("disappearing")
        this.disappearing = true
        this.disappear_callback = callback
    }



    draw(){
        
        if(this.disappearing){
            if(this.H-this.disappear_v>=0){
                this.H-=this.disappear_v
                this.W-=this.disappear_v
                this.X+=this.disappear_v/2
                this.Y+=this.disappear_v/2    
            } else {
                this.remove()
                this.disappear_callback()
                
            }
            
        }

        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
       
    }

}


class MovingObject extends Object2d{
    static create(group, index, X, Y, W, H, borders, vX, vY, check, check_return=false, returnDir=0){
        physic_objects.push(new MovingObject(group, index, X, Y, W, H, borders, vX, vY, check, check_return, returnDir))
    }

    constructor(group, index, X, Y, W, H, borders, vX, vY, check, check_return, returnDir){
        super(X, Y, W, H, group, index)
        this.vX = vX
        this.vY= vY
        this.borders = borders
        
        this.check = check
        this.check_return = check_return
        this.returnDir = returnDir // 1 x 2 y 3 both
    }

    update(){


        if(jonas.Y>this.Y  && jonas.Y<this.Y+5 && jonas.vY==0 && 
        ((jonas.X>this.X && jonas.X<this.X+this.W) || (jonas.X+jonas.W>this.X && jonas.X<this.X+this.W))
        ){
            //console.log("jonas on lego brick")
            jonas.X+=this.vX
            jonas.Y+=this.vY
        }



        if(this.check(this.X, this.Y)){
            this.X+=this.vX
            this.Y+=this.vY

            for(let border of this.borders){
                border[0]+=this.vY
                border[1]+=this.vX
                border[2]+=this.vX
            }            
        }

        if(this.check_return!=false){
            if(this.check_return(this.X, this.Y)){
                if(this.returnDir==1){
                    this.vX=-this.vX
                } else if(this.returnDir==2){
                    this.vY=-this.vY
                } else if(this.returnDir==3){
                    this.vY=-this.vY
                    this.vX=-this.vX
                } 
            }
        }


    }


}


class Object2dRotate extends Object2d{
    static create(X, Y, W, H, sprite_group, sprite_index, vA){
        return objects.push(new Object2dRotate(X, Y, W, H, sprite_group, sprite_index, vA))-1
    }

    constructor(X, Y, W, H, sprite_group, sprite_index, vA){
        super(X, Y, W, H, sprite_group, sprite_index)
        this.vA = vA
        this.angle = 0

        this.rotationCenterX = this.X+this.W/2
        this.rotationCenterY = this.Y+this.H/2
    }

    draw(){

        this.angle+=this.vA

        ctx.save()
        ctx.translate(Xtox(this.rotationCenterX), Ytoy(this.rotationCenterY) );
        ctx.rotate( this.angle);
        ctx.translate( -Xtox(this.rotationCenterX), -Ytoy(this.rotationCenterY) );
        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)

        ctx.restore()
    }
}


class Item extends Object2d{

    static add(index, X, Y, W, H, collector, callback, sound="item"){
        items.push(new Item(index, X,Y, W, H, collector, callback, sound))

    }     

    constructor(index, X,Y,W,H, collector, callback, sound){
        super(X,Y, W, H, "items", index)
        this.c = this.X*.2
        this.collector = collector
        this.callback = callback
        this.sound = sound

    }   

    update(){

        if(!this.disappearing){

            let l1x = this.X,
                l1y = this.Y+this.H,
                r1x = this.X+this.W,
                r1y = this.Y,
                             
                l2x = this.collector.X,
                l2y = this.collector.Y+this.collector.H,
                r2x = this.collector.X+this.collector.W,
                r2y = this.collector.Y               
            


            if(rectsIntersection(l1x, l1y, r1x, r1y, l2x, l2y, r2x, r2y)){
                this.callback()   
                Sound.play(this.sound, .2)
                this.disappear()         
            }            

            if(this.collector.using_vehicle && !this.collector.using_rocket){
                l2x = vehicles[this.collector.vehicle_index].X,
                l2y = vehicles[this.collector.vehicle_index].Y+vehicles[this.collector.vehicle_index].H,
                r2x = vehicles[this.collector.vehicle_index].X+vehicles[this.collector.vehicle_index].W,
                r2y = vehicles[this.collector.vehicle_index].Y  

                if(rectsIntersection(l1x, l1y, r1x, r1y, l2x, l2y, r2x, r2y)){
                    this.callback()   
                    Sound.play(this.sound, .2)
                    this.disappear()         
                } 
            } 
               
        }

    }

    remove(){
        try{
            var i = items.indexOf(this)
            items.splice(i, 1)
        } catch(e){}
    }

    disappear(callback){
        console.log("disappearing")
        this.disappearing = true
        this.disappear_callback = callback
    }



     draw(){
                 
        if(this.disappearing){
            if(this.H-this.disappear_v>=0){
                this.H-=this.disappear_v
                this.W-=this.disappear_v
                this.X+=this.disappear_v/2
                this.Y+=this.disappear_v/2    
            } else {
                this.remove()
                if(this.disappear_callback!=null) this.disappear_callback()

            }

        }

        this.c+=0.06
        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H+Math.sin(this.c)), this.W*DM, this.H*DM)
        if (DEBUG){
            ctx.strokeStyle = "yellow"
            ctx.strokeRect( Xtox(this.X), Ytoy(this.Y+this.H+Math.sin(this.c)*.7), this.W*DM, this.H*DM)
        } 
     }

}

class Waterfall extends Object2d{
     static create(X, Y, W, H){
         background_objects.push(new Waterfall(X,Y, W, H))
     } 

     constructor(X,Y,W,H){
         super(X,Y, W, H, "waterfall", 0)
         this.i=0

     }   


     draw(){    
        this.i+=0.5
        if(this.i>3){
            this.i=0
            ParticleX4.create(sprites.particles[16], this.X+this.W*.2+Math.random()*this.W*.5, this.Y+0.7*this.H, -0.05, 0.2, 60, 10)
            ParticleX4.create(sprites.particles[18], this.X+this.W*.2+Math.random()*this.W*.5, this.Y, 0, 0.05, 40, 3)
            ParticleX4.create(sprites.particles[18], this.X+this.W*.2+Math.random()*this.W*.5, this.Y, 0, 0.05, 40, 3)
            ParticleX4.create(sprites.particles[18], this.X+this.W*.2+Math.random()*this.W*.5, this.Y, 0, 0.05, 40, 3)

        } 
        ctx.drawImage(sprites.waterfall[Math.floor(this.i)], X4tox(this.X), Y4toy(this.Y+this.H), this.W*DM, this.H*DM)
        ctx.drawImage(sprites.rocks[0], X4tox(this.X), Y4toy(this.Y+this.H), this.W*DM, this.H*DM)

     }
}

class BackgroundHouse extends Object2d{
     static create(group, index, X, Y, W, H){
         background_objects.push(new BackgroundHouse(group, index, X,Y, W, H))
     } 

     constructor(group, index, X,Y, W, H){
         super(X,Y, W, H, group, index)
     }

     draw(){    
        ctx.drawImage(sprites[this.sprite_group][this.sprite_index], X4tox(this.X), Y4toy(this.Y+this.H), this.W*DM, this.H*DM)
     }

}


class Windmill extends Object2d{
     static create(X, Y, S){
         background_objects.push(new Windmill(X,Y, S, 0))
     }

     constructor(X,Y, S, sprite_index){
         super(X,Y, S, S*3, "windmill", sprite_index)
         this.angle=0
         this.rotationCenterX = this.X+this.W/2.15
         this.rotationCenterY = this.Y+this.H*0.94

     }

     draw(){    
        this.angle+=0.01


        ctx.drawImage(this.sprite, X3tox(this.X), Y3toy(this.Y+this.H), this.W*DM, this.H*DM)
            
        ctx.save();
        ctx.translate(X3tox(this.rotationCenterX), Y3toy(this.rotationCenterY) );
        ctx.rotate( this.angle);
        ctx.translate( -X3tox(this.rotationCenterX), -Y3toy(this.rotationCenterY) );
        ctx.drawImage(sprites.windmill[this.sprite_index+1], X3tox(this.rotationCenterX-this.W), Y3toy(this.rotationCenterY+this.W), this.W*2*DM, this.W*2*DM)      
        if(DEBUG) ctx.strokeRect(X3tox(this.rotationCenterX-this.W), Y3toy(this.rotationCenterY+this.W), this.W*2*DM, this.W*2*DM)

        ctx.restore()

        if(DEBUG){
            ctx.fillStyle = "red"
            ctx.fillRect(X3tox(this.rotationCenterX), Y3toy(this.rotationCenterY), 2, 2)
        }
     }
   
}

class BigWindmill extends Windmill{
     static create(X, Y, S){
         multilayer_objects.push(new BigWindmill(X, Y, S, 2))
     }    

     draw(){    
        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)    
       
     }

     draw_foreground(){
        this.angle+=0.01
  
        ctx.save();
        ctx.translate(Xtox(this.rotationCenterX), Ytoy(this.rotationCenterY) );
        ctx.rotate( this.angle);
        ctx.translate( -Xtox(this.rotationCenterX), -Ytoy(this.rotationCenterY) );
        ctx.drawImage(sprites.windmill[this.sprite_index+1], Xtox(this.rotationCenterX-this.W), Ytoy(this.rotationCenterY+this.W), this.W*2*DM, this.W*2*DM)      
        if(DEBUG) ctx.strokeRect(Xtox(this.rotationCenterX-this.W), Ytoy(this.rotationCenterY+this.W), this.W*2*DM, this.W*2*DM)

        ctx.restore()

        if(DEBUG){
            ctx.fillStyle = "red"
            ctx.fillRect(Xtox(this.rotationCenterX), Ytoy(this.rotationCenterY), 2, 2)
        }
     }
   
}



class Vehicle extends Object2d{

    static create(vehicle_type, X, Y, W, H, sitPositions, sprite_group, sprite_index, wheel1, wheel2){
        vehicles.push(new Vehicle(vehicle_type, X, Y, W, H, sitPositions, sprite_group, sprite_index, wheel1, wheel2))
    }

    constructor(vehicle_type, X, Y, W, H, sitPositions, sprite_group, sprite_index, wheel1, wheel2){
        
        super(X, Y, 10, 10, sprite_group, sprite_index)

        this.wheel1_data = wheel1
        this.wheel2_data = wheel2

        this.vehicle_type = vehicle_type

        this.maxVX = 2
        this.groundY = 0

        this.custom_properties() // load Properties !important    
    
        this.sprite = sprites[this.sprite_group][this.sprite_index]

        this.wheel1 = new Wheel(X+this.wheel1_data[0], Y+this.wheel1_data[1], this.wheel1_data[2], this.wheel1_data[2], this.wheel1_data[7], this.wheel1_data[3], this.wheel1_data[4], this.wheel1_data[5], this.wheel1_data[6])
        this.wheel2 = new Wheel(X+this.wheel2_data[0], Y+this.wheel2_data[1], this.wheel2_data[2], this.wheel2_data[2], this.wheel1_data[7], this.wheel2_data[3], this.wheel2_data[4], this.wheel2_data[5], this.wheel1_data[6])

        this.wheel2_distX = this.wheel2.X-this.X
        this.wheel2_distY = this.wheel2.Y-this.Y

 
        this.availableSeat = 0

        this.collided = false
        
        this.vX = 0
        this.vY = 0
        this.wheel2.vX=0

        this.sliding = false

        this.on_ground = false
        this.yAcc = 0


        this.wheel_distance = Math.abs(this.wheel1_data[0]-this.wheel2_data[0])

        this.angle = 0
        this.jonasDriving = false
        this.personsDriving = []

        this.accelerate = false
        this.trailer_connected = false
        
        this.inv_actionable = null

        this.first_acceleration = 0
        this.engine_running = false

        this.crashed = false
        
    }

    custom_properties(){

        // wooden trailer
        if(this.vehicle_type=="wooden_trailer"){
            this.W = 25
            this.H = 12
            this.sitPositions = [{X:2.8, Y:5.2}, {X:8.8, Y:5.2}, {X:14.8, Y:5.2}]

            this.sprite_index = 2

            this.zPos = -1
            
            this.wheel1_data = [0,-3,8,3, false, true, true, "vehicles"]
            this.wheel2_data = [14,-3,8,3, false, true, true, "vehicles"]
          
        }

        // tractor
        if(this.vehicle_type=="tractor"){
            this.W = 20
            this.H = 12.5
            this.sitPositions = [{X:3.6, Y:6.2}]
            this.jonasSeat = {X:2.8, Y:6.2}
            
            this.sprite_index = 0
            this.zPos = 1

            this.wheel1_data = [0,0,6,1, true, false, true, "vehicles"],
            this.wheel2_data = [14,0,5,1, false, false, true, "vehicles"]
        }

        // sleigh
        if(this.vehicle_type=="sleigh"){
            this.W = 20
            this.H = 6.7
            this.sitPositions = [{X:6.2, Y:4.3}]
            this.jonasSeat = {X:2.6, Y:2}
            
            this.sprite_index = 0
            this.zPos = 1

            this.wheel1_data = [0,0,6,1, true, false, false, "sleigh"], // sleigh=dummy group - wheel isnt shown
            this.wheel2_data = [14,0,5,1, false, false, false, "sleigh"]
       
            this.maxVX = 1.7

            this.groundY = -.9
        }

    }

    make_actionable(){ 

        if(this.vehicle_type=="tractor"){
            this.inv_actionable = new Actionable(this.X, this.Y, this.W, this.H, null, null, 10, 10, 20, 1, "Rasenmäher-Traktor benutzen", false, (object)=>{
                this.inv_actionable = null
                this.addDriverJonas("sit", 0, "right")
                Sound.play("engine_start", .2, "engine_start", false, false, ()=>{
                    Sound.play("engine_running", .2, "engine_running", true)
                    this.engine_running = true

                })
            })    
        }     

        else if(this.vehicle_type=="sleigh"){
            this.inv_actionable = new Actionable(this.X, this.Y, this.W, this.H, null, null, 10, 10, 20, 1, "Schlitten benutzen", false, (object)=>{
                this.inv_actionable = null
                this.addDriverJonas("sit", 1, "right")
            })    
        } 
    }

    connectTrailer(trailer){
        this.trailer_connected = true
        this.trailer = trailer
    }

    addDriver(name, position_type, position_index, head, moveHead){
        if(this.availableSeat<=this.sitPositions.length-1){
            var index = vehicles.indexOf(this)
            persons[name].rideVehicle(this.X+this.sitPositions[this.availableSeat].X, this.Y+this.sitPositions[this.availableSeat].Y, position_type, position_index, head, index, "tractor")
            this.personsDriving.push({name:name, sit:this.sitPositions[this.availableSeat], moveHead:moveHead})
            this.availableSeat++    
            console.log(name + " found a seat")
            return true
        } else {
            return false
        }

    }


    removeDriver(name){
        persons[name].leaveVehicle()
        if(this.availableSeat>0) this.availableSeat--

        for (var p=0;p<this.personsDriving.length;p++){
            if(this.personsDriving[p].name==name){
                 this.personsDriving.splice(p, 1);       
            }
        }
    }

    addDriverJonas(position_type, position_index, head){
        this.sprite = sprites[this.sprite_group][this.sprite_index]
                
        var index = vehicles.indexOf(this)
        jonas.rideVehicle(this.X+this.jonasSeat.X, this.Y+this.jonasSeat.Y, position_type, position_index, head, index, this.vehicle_type)
        this.jonasDriving = true
        this.inv_actionable = null
        setKeyHint(["E", "Absteigen"])
        constant_keyhint = true
        actionable_in_focus.action_callback = ()=>{
            this.removeDriverJonas()            
        }   
    }

    removeDriverJonas(){
        constant_keyhint = false
        jonas.leaveVehicle()
        this.jonasDriving = false
        this.make_actionable()
        if(this.engine_running) Sound.clearSound("engine_running")
        this.engine_running=false
    }

    sleigh_crash(){
        this.crashed = true
        
        GravityParticle.create(sprites.sleigh[2], this.X, this.Y, 20, 6.7);
        GravityParticle.create(sprites.sleigh[3], this.X, this.Y, 20, 6.7)
        GravityParticle.create(sprites.sleigh[1], this.X, this.Y,20, 6.7)
        GravityParticle.create(sprites.sleigh[3], this.X, this.Y,  20, 6.7)
        GravityParticle.create(sprites.sleigh[1], this.X, this.Y, 20, 6.7);
        GravityParticle.create(sprites.sleigh[2], this.X, this.Y, 20, 6.7);
        GravityParticle.create(sprites.sleigh[3], this.X, this.Y, 20, 6.7)
        GravityParticle.create(sprites.sleigh[1], this.X, this.Y,20, 6.7)
        GravityParticle.create(sprites.sleigh[3], this.X, this.Y,  20, 6.7)
        GravityParticle.create(sprites.sleigh[1], this.X, this.Y, 20, 6.7);
        
        Particle.create(sprites.particles[level_ground_particles], this.X, this.Y+2, 0, 0.4, 10, 4)
        Particle.create(sprites.particles[level_ground_particles], this.X, this.Y+2, 0, 0.4, 10, 4)
        Particle.create(sprites.particles[level_ground_particles], this.X, this.Y+2, 0, 0.4, 10, 4)
            
        Sound.play("crash")
        console.log("collided!")
        this.removeDriverJonas()
        try{this.removeDriver("hubatz")}catch(e){}

        setTimeout(()=>{
            restartLevel("Passe auf die Felsen auf")
        }, 3000)
    }

    update(){
        
      if(!this.crashed){
      
         if(this.vehicle_type=="sleigh" && this.jonasDriving && this.sliding) this.vX+=.02
        
         if(this.vehicle_type=="sleigh" && this.X>1050) this.vX=0
         
         if(this.vehicle_type=="tractor" && this.X>990) this.vX=0


           var y1 = this.vY + this.Y+1,
               y2 = this.vY + this.Y+this.H-2,
               x1 = this.X+this.vX,
               x2 = this.X+this.W+this.vX

           let ground_collided = false

           for (var i=0; i<CURRENT_LEVEL_DATA.ground_borders.length;i++){  
                var line = CURRENT_LEVEL_DATA.ground_borders[i]  
                if((line[0]<y2 && line[0]>y1)  // y position intersection
                        && (x1 <= line[2] && line[1] <= x2)) {   // x intersection
                    console.log("ground_border!")
                    this.vY=1
                    ground_collided = true      
                    break;   
                }
            }


            if(!ground_collided){
                for (var i=0; i<CURRENT_LEVEL_DATA.side_borders.length;i++){  
                    var line = CURRENT_LEVEL_DATA.side_borders[i]
                    if( (y1 <= line[2] && line[1] <= y2)  &&
                        ((this.X+this.vX<line[0] && this.X+this.vX+this.W>line[0]) ||
                        (this.X+this.W+this.vX>line[0] && this.X+this.vX<line[0]) )) {
                            this.vX=-0.1*this.vX
                            this.collided=true
                            if(this.vehicle_type=="sleigh"){
                                this.sleigh_crash()
                            }
                    } else {
                        this.collided = false
                    }
                }            
            }




            if(!this.jonasDriving && this.inv_actionable!=null){
                this.inv_actionable.X = this.X
                this.inv_actionable.Y = this.Y
                if(this.inv_actionable.check(jonas.X, jonas.Y)){
                    this.sprite = sprites[this.sprite_group].outline[this.sprite_index]
                } else {
                    this.sprite = sprites[this.sprite_group][this.sprite_index]
                }
            }

            if(this.jonasDriving && this.Y<-30){
                restartLevel("Passe auf Abgründe auf")
            }


            this.angle = this.wheel_angle()


            // velocity behaviour
            this.vX*=0.99
            if(this.vX>this.maxVX) this.vX = this.maxVX
            if(this.vX<-this.maxVX+.5) this.vX = this.maxVX+-5
            if(Math.abs(this.vX)<0.001) this.vX = 0
            if(this.angle>0.05) this.vX+=0.005
            if(this.angle<-0.05) this.vX-=0.005

            if (this.X+this.vX <0){ // stop at world border
                this.vX = 0
                this.X = 0
                this.collided = true
            }

            if(this.trailer_connected && this.trailer.collided){
                this.collided = true
                this.vX*=-0.5
            } 


            if(this.vehicle_type=="sleigh"){
                if(this.vY<0){
                    this.vY=0    
                } 

                if(this.wheel1.on_ground || this.wheel2.on_ground){
                    this.on_ground = true
                } else {
                    this.on_ground=false
                }

                this.vY-=this.yAcc
                this.yAcc+=0.03

                if(this.on_ground) this.yAcc=0

                this.wheel1.vY = this.vY
                this.wheel2.vY = this.vY*1.2

            }



            this.wheel2.vX = this.vX
            this.wheel1.vX = this.wheel2.vX

            if(this.trailer_connected) this.trailer.vX = this.vX


            this.wheel1.update()
            this.wheel2.update() 


            this.X = this.wheel2.X-this.wheel2_distX
            this.Y = this.wheel2.Y-this.wheel2_distY+this.groundY

            this.rotationCenterX = (this.wheel2.X+this.wheel2.W/2)
            this.rotationCenterY = (this.wheel2.Y+this.wheel2.H/2)

            if(this.jonasDriving){
                jonas.X = this.X+this.jonasSeat.X
                jonas.Y = this.Y+this.jonasSeat.Y

                if(this.accelerate==-1){
                    jonas.lookLeft()
                } else if(this.accelerate==1){
                    jonas.lookRight()
                }
                if(this.vX==0 && this.accelerate==0){
                    jonas.lookFront()
                }

                if(this.vehicle_type=="tractor"){                
                    if(this.accelerate!=0 && this.first_acceleration<6){
                        this.first_acceleration++
                        console.log("sound", this.accelerate, this.first_acceleration)
                        Sound.play("engine_accelerate", .3)
                    } else if(this.accelerate==0) {
                        if(this.engine_running) Sound.setVolume("engine_running", .4)
                        this.first_acceleration=0
                    }
                } 
            } 

            if(this.personsDriving.length){
                for(var p of this.personsDriving){
                    persons[p.name].X = this.X+p.sit.X
                    persons[p.name].Y = this.Y+p.sit.Y

                    if(p.moveHead){
                        if(this.accelerate==-1){
                            persons[p.name].lookLeft()
                        } else if(this.accelerate==1){
                            persons[p.name].lookRight()
                        }
                        if(this.vX==0 && this.accelerate==0){
                            persons[p.name].lookFront()
                        }                   
                    }

                }
            }



        }
        
       

    }

    wheel_angle(){
        var dir = this.wheel1.Y>=this.wheel2.Y ? 1 : -1
        return dir*(Math.PI/2-Math.atan( Math.abs(this.wheel2.X-this.wheel1.X) / Math.abs(this.wheel2.Y-this.wheel1.Y)))
    }

    rotatedRelativePoint(rX, rY){ // relative X, Y
        var newX = this.rotationCenterX - (this.wheel2_data[1]+1.5-rY)*Math.sin(this.angle) - (this.wheel2_data[0]+2.5-rX) *Math.cos(this.angle)
        var newY = this.rotationCenterY - (this.wheel2_data[1]+1.5-rY)*Math.cos(this.angle) + (this.wheel2_data[0]+2.5-rX) *Math.sin(this.angle)
        return {X:newX, Y:newY}
    }
   

    draw(){
        
        if(!this.crashed){
            this.exhaust = this.rotatedRelativePoint(1,4)

            // very complicated calculations!!

            if (this.inv_actionable!=null) this.inv_actionable.draw()

            if(this.accelerate){
                this.accelerate = false

                if(this.vX>0){

                    BackgroundParticle.create(sprites.particles[8], this.exhaust.X, this.exhaust.Y, 0.2, 0.3, 3)
                } else {
                    BackgroundParticle.create(sprites.particles[8], this.exhaust.X, this.exhaust.Y, -0.2, 0.3, 3)
                }
            }

            ctx.save();
            ctx.translate(Xtox(this.rotationCenterX), Ytoy(this.rotationCenterY) );
            ctx.rotate( this.angle);
            ctx.translate( -Xtox(this.rotationCenterX), -Ytoy(this.rotationCenterY) );

            // draw jonas/persons at right position BACK
            if(this.zPos==-1){
                if(this.personsDriving.length!=0){
                    for(var p of this.personsDriving){
                        persons[p.name].draw()
                    }
                }    

                if(this.jonasDriving){
                    jonas.draw()
                }        
            }



            ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)

            // draw jonas/persons at right position FRONT
            if(this.zPos==1){
                if(this.personsDriving.length!=0){
                    for(var p of this.personsDriving){
                        persons[p.name].draw()
                    }
                }

                if(this.jonasDriving){
                    jonas.draw()
                }

            }


            if(DEBUG){
                ctx.strokeStyle = "white"
                ctx.strokeRect(Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
            }

            ctx.restore();


            this.wheel1.draw()
            this.wheel2.draw()

            /*ctx.fillStyle = "white"
            ctx.fillRect(Xtox(this.exhaust.X), Ytoy(this.exhaust.Y), 1*DM, 1*DM)
            */    


        }



    }
}


class Wheel extends Object2d{
    
    /*static create(X,Y, W, H, has_particles=false){
        physic_objects.push(new Wheel(X, Y, W, H, W, H, has_particles))
    }*/

    constructor(X,Y, W, H, sprite_group, sprite_index, has_particles=false, rotating=false, visibility=true){
        super(X, Y, W, H, sprite_group, sprite_index)
        this.vY = 0
        this.vX = 0
        this.has_particles=has_particles
        this.angle=0
        this.rotating=rotating

        this.on_ground = false

        this.visibility = visibility
    }

    draw(){ // # changed

        if(this.visibility){
            if(this.rotating){
                if(this.vX!=0) this.angle-=this.vX/10
                ctx.save();
                ctx.translate(Xtox(this.X+this.W/2), Ytoy(this.Y+this.H/2) );
                ctx.rotate(this.angle);
                ctx.translate( -Xtox(this.X+this.W/2), -Ytoy(this.Y+this.H/2) );
                ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
                ctx.restore()            
            } else {
                ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
            }


            if(this.has_particles){
              if(this.vX>0.5){
                BackgroundParticle.create(sprites.particles[level_ground_particles], this.X, this.Y+2, 0, 0.4, 5)
              } else if(this.vX<-0.1)  {
                BackgroundParticle.create(sprites.particles[level_ground_particles], this.X, this.Y+2, -0.4, 0.2, 5)
              }
            } 

        }
            if(DEBUG){
                ctx.strokeStyle="black"
                ctx.strokeRect(Xtox(this.X), Ytoy(this.Y+this.W), this.W*DM, this.H*DM)
            }


       

    }

    update(){
        var y1 = this.Y + this.vY+0.6,
            x1 = this.X + this.W/2 + this.vX
        
        if(Math.abs(this.vX)>1){
            this.vY-=ACCELERATION * (0.5/Math.abs(this.vX))
        } else {
            this.vY-=ACCELERATION
        }


        for (var i=0; i<hillscapes.length;i++){  
            var fx = hillscapes[i].f(x1)
            if(y1<fx){
                this.vY = ( hillscapes[i].f(x1) - hillscapes[i].f(this.X + this.W/2))
                this.on_ground = true
                //this.Y-= ( hillscapes[i].f(x1) - hillscapes[i].f(this.X + this.W/2))
            } else {
                this.on_ground = false
            }
        }




        this.X += this.vX
        this.Y += this.vY
    }
    
}



// handle Timeouts and Intervals

function handleTimeout(call, delay){
    return timeouts.push(setTimeout(call, delay))-1
}


function clearAllTimeouts(){
    for(let t of timeouts){
        clearTimeout(t)
    }
    timeouts = []
}

function handleInterval(call, delay){
    return intervals.push(setInterval(call, delay))-1
}


function clearAllIntervals(){
    for(let i of intervals){
        clearInterval(i)
    }
    intervals = []
}



class Door extends Object2d{
    static create(X, Y, W, H, sprite_group, sprite_index, _X, _Y, _W, _H, _sprite_index, delay, callback){
        objects.push(new Door(X, Y, W, H, sprite_group, sprite_index, _X, _Y, _W, _H, _sprite_index, delay, callback))
    }

    constructor(X, Y, W, H, sprite_group, sprite_index, _X, _Y, _W, _H, _sprite_index, delay=false, callback){
        super(X, Y, W, H, sprite_group, sprite_index)
        this._X = _X
        this._Y = _Y
        this._W = _W
        this._H = _H
        this._sprite_index = _sprite_index

        if(delay){
            handleTimeout(()=>{
                this.open()
                callback()
            }, delay)
        }
    }

    open(){
        Sound.play("door", .6)

        this.X = this._X
        this.Y = this._Y
        this.W = this._W
        this.H = this._H
        this.sprite = sprites[this.sprite_group][this._sprite_index]

    }

}

class Mushroom extends Object2d{
    static create(X, Y){
        objects.push(new Mushroom(X, Y))
    }    
    constructor(X, Y){
        super(X, Y, 5, 5, "vegetation", 6)
        this.draw_sprite = this.sprite
        this.extending = false
        jumpables.push([Y+3, X, X+5, this, 3.6])
    }

    push(){
        Sound.play("jump")
        this.draw_sprite = sprites["vegetation"][7]
        this.wait_extend = 0
        this.extending = true
    }
    
    draw(){
        if(this.extending){
            this.wait_extend++

            if(this.wait_extend==10){
              this.draw_sprite = sprites["vegetation"][8]  
            }
            if(this.wait_extend==30){
              this.draw_sprite = sprites["vegetation"][6]  
              this.extending = false
            }
    
        }
        
        ctx.drawImage(this.draw_sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
       
    }
}



class Actionable extends Object2d{

    static create(X, Y, W, H, sprite_group, sprite_index, action_left = 0, action_right = 0, action_top = 0, action_bottom = 0, key_info="", outline, action_callback = null){
        actionables.push(new Actionable(X, Y, W, H, sprite_group, sprite_index, action_left, action_right, action_top, action_bottom, key_info, outline, action_callback))
    }


    constructor(X, Y, W, H, sprite_group, sprite_index, action_left, action_right, action_top, action_bottom, key_info, outline, action_callback){
        super(X, Y, W, H, sprite_group, sprite_index)
        

        if(outline && sprite_group!=null) this.outline_sprite = sprites[sprite_group].outline[sprite_index]
        
        this.outline = outline

        this.action_left = action_left
        this.action_right = action_right
        this.action_top = action_top
        this.action_bottom = action_bottom

        this.action_callback  = action_callback

        this.key_info = key_info

        this.focus = false
        this.deactivated = false
        this.deactivated_forever = false

    }

    remove(){ // #changed
        try{
            var i = actionables.indexOf(this)
            actionables.splice(i, 1)
        } catch(e){console.error(e)}
    }

    deactivate(){
        this.deactivated = true
        
    }

    deactivate_forever(){
        this.deactivated_forever = true
    }
    
    draw(){// #changed
        

        if(this.disappearing){
            if(this.H-this.disappear_v>=0){
                this.H-=this.disappear_v
                this.W-=this.disappear_v
                this.X+=this.disappear_v/2
                this.Y+=this.disappear_v/2    
            } else {
                this.remove()
                this.disappear_callback()

            }

        }


        if (this.focus && !this.deactivated_forever){            
            if((this.outline || this.sprite_group==null) && !this.deactivated){
                setKeyHint(["E", this.key_info])
                //console.log(this.key_info)
                if(this.sprite_group!=null) ctx.drawImage(this.outline_sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
            } else {
                if(this.sprite_group!=null) ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
            }
            if(this.deactivated) key_hint = []
        } else {
            this.deactivated = false
            if(!any_key_hint && !constant_keyhint) key_hint = []
            if(this.sprite_group!=null) ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
        }    
        



    }


    check (X, Y){  // #new
        if (this.X+this.W/2-this.action_left<X && this.X+this.W/2+this.action_right>X
            && this.Y-this.action_bottom<Y && this.Y+this.action_top>Y){ // get object in focus
            actionable_in_focus = this
            this.focus = true
            any_key_hint++
            return true
        } else {

           this.focus = false
           if(!any_key_hint && !constant_keyhint) actionable_in_focus = {}
           return false
 
        }
    }



}

function disableControls(){
    controls_disabled = true
    dark_sky=true
    dark_sky_value = 0
}



class Rocket extends Actionable{
   static create(l){
        actionables.push(new Rocket())
   }


    constructor(){
        super(510, 66, 30, 50, "rocket", 0, 8, 8, 20, 20, "In die Rakete steigen", true, (object)=>{
            jonas.using_vehicle = true
            jonas.using_rocket = true
            object.deactivate_forever()

            setTimeout(()=>{
                 this.vA = 0.001
                 this.vY = 0.6

                 disableControls()
                 Sound.play("rocket", .3)
                 Sound.clearSound("background_music")
                 Sound.play("end", .6)
            }, 2000)
           
        })

        this.vX = 0
        this.vY = 0
        this.vA = 0

        this.angle = 0

        this.animation_index = 0

    }

    draw(){// #changed

        this.W*=.999999
        this.H*=.999999
        
        
        ctx.save();
        ctx.translate(Xtox(this.rotationCenterX), Ytoy(this.rotationCenterY) );
        ctx.rotate( this.angle);
        ctx.translate( -Xtox(this.rotationCenterX), -Ytoy(this.rotationCenterY) ); 

        if(jonas.using_vehicle ){


            this.X += this.vX
            this.Y += this.vY

            this.angle+=this.vA

            if(this.angle>0.2){
                this.angle=0.2
                this.vA=-0.95*this.vA
            } else if(this.angle<-0.2){
                this.angle=-0.2
                this.vA=-0.95*this.vA
            }

            this.rotationCenterX = this.X+this.W/2
            this.rotationCenterY = this.Y+this.H/2

            jonas.X=this.X+this.W/2.55
            jonas.Y=this.Y+this.H/2.9
            jonas.draw()

            if(this.vY>0){
                this.animation_index+=0.2
                if(this.animation_index>3) this.animation_index=0

                ctx.drawImage(sprites.rocket[Math.floor(this.animation_index+1)], Xtox(this.X+this.W/2.85), Ytoy(this.Y+this.H*.13), 10*DM, 10*DM)
                          
            }
        }


        super.draw()

        ctx.restore()

    }


}



class Rotatable extends Actionable{
    static create(X, Y, W, H , sprite_group, sprite_index, action_left, action_right, action_top, action_bottom, rotating_sprite_group , rotating_sprite_index){
        actionables.push(new Rotatable(X, Y, W, H, sprite_group, sprite_index, action_left, action_right, action_top, action_bottom, "", false, ()=>{}, rotating_sprite_group , rotating_sprite_index))
    }

    constructor(X, Y, W, H, sprite_group, sprite_index, action_left, action_right, action_top, action_bottom, key_info, outline, action_callback, rotating_sprite_group , rotating_sprite_index){
        super(X, Y, W, H, sprite_group, sprite_index, action_left, action_right, action_top, action_bottom, key_info, outline, action_callback)
        this.rotating_sprite = sprites[rotating_sprite_group][rotating_sprite_index]

        this.angle = 0
        this.touched = false
    }   

    check (X, Y){  // #new
        if (this.X+this.W/2-this.action_left<X && this.X+this.W/2+this.action_right>X
            && this.Y-this.action_bottom<Y && this.Y+this.action_top>Y){ // get object in focus
            if(!this.touched) {
                this.touched=true
            }
            return true
        } else {

        
           return false
 
        }
    }

    draw(){
        super.draw()

        if(this.touched && this.angle<Math.PI) this.angle+=0.3

        ctx.save();
        ctx.translate(Xtox(this.X+this.W/2), Ytoy(this.Y+this.H/2) );
        ctx.rotate(this.angle);
        ctx.translate( -Xtox(this.X+this.W/2), -Ytoy(this.Y+this.H/2) );
        ctx.drawImage(this.rotating_sprite, Xtox(this.X), Ytoy(this.Y+this.H), this.W*DM, this.H*DM)
        ctx.restore()    
    }
}


function cloneCanvas(oldCanvas) {

    //create a new canvas
    var newCanvas = document.createElement('canvas');
    var context = newCanvas.getContext('2d');
    newCanvas.width = oldCanvas.width;
    newCanvas.height = oldCanvas.height;

    context.drawImage(oldCanvas, 0, 0);

    return newCanvas;
}

class Sun{

    constructor(rX, rY, c1, c2, brightness=0.5, size=1, rays=140){
        this.c1 = c1
        this.c2 = c2

        this.rX = rX
        this.rY = rY

        this.rays = rays

        this.brightness = brightness
        this.size = size

        this.updateGradient()
    
    }

    updateGradient(){
        this.w = can.width
        this.h = can.height
        

        // foreground grd
        this.grd=ctx.createRadialGradient(this.w*this.rX,this.h*this.rY,3*DM*this.size,this.w/3,this.h/2,70*DM*this.size);
        this.grd.addColorStop(.01,this.c2);

        this.grd.addColorStop(.7,"transparent");
        
        // background grd
        this.bg_grd=ctx.createRadialGradient(this.w*this.rX,this.h*this.rY,3*DM*this.size,this.w/3,this.h/2,70*DM*this.size);
        this.bg_grd.addColorStop(.03,this.c1);
        this.bg_grd.addColorStop(.03,this.c2+"aa");
        this.bg_grd.addColorStop(.05,this.c2+"44");
        this.bg_grd.addColorStop(.1,this.c2+"11");
        this.bg_grd.addColorStop(.3,"transparent");

 
        this.canvas = cloneCanvas(can)
        this.ctx = this.canvas.getContext("2d")
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        
 
        this.ctx.globalAlpha=this.brightness
        this.ctx.fillStyle=this.grd;
        this.ctx.fillRect(0,0,this.w,this.h);

        this.ctx.globalAlpha=1

    }

    draw(){
        ctx.drawImage(this.canvas, 0, 0)
    }

}



class Background{

    constructor(c,clouds, terrain){
        this.sky_colors = c
        this.clouds = clouds
        this.terrain = terrain

        this.updateGradient()


    }

    updateGradient(){
        this.w = can.width
        this.h = can.height

        this.grd = ctx.createLinearGradient(0,0,0,can.height);

        for(let color in this.sky_colors){
            this.grd.addColorStop(color, this.sky_colors[color]);
            this.grd.addColorStop(color, this.sky_colors[color]);
            this.grd.addColorStop(color, this.sky_colors[color]);            
        }

        this.canvas = cloneCanvas(can)
        this.ctx = this.canvas.getContext("2d")
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)

        this.ctx.fillStyle=this.grd;
        this.ctx.fillRect(0,0,this.w,this.h);
        
        if(sun!=false && sun.rays){
            ctx.globalAlpha=0.4
            let size = sun.size*sun.rays
            this.ctx.drawImage(sprites.sunrays[0], this.w*sun.rX-(size/2),this.h*sun.rY-(size/2), size, size)

            ctx.globalAlpha=1            
        }

   
        if(sun!=false){
            this.ctx.fillStyle=sun.bg_grd;
            this.ctx.fillRect(0,0,this.w,this.h);
        }
    }


    draw(){
        //ctx.clearRect(0, 0, can.width, can.height)
        ctx.drawImage(this.canvas, 0, 0)
        
        if(this.clouds!=null){
            var X2max = Math.ceil(xtoX2(can.width)/100),
                X2min = Math.floor(xtoX2(0)/100)

            for(var X=X2min;X<=X2max;X++){
                ctx.drawImage(this.clouds, X2tox(X*100), Y2toy(20), 100*DM, 20*DM)
            }
        }
        
        if(this.terrain!=null){
            var X3max = Math.ceil(xtoX3(can.width)/100),
                X3min = Math.floor(xtoX3(0)/100)

            for(var X=X3min;X<=X3max;X++){
                ctx.drawImage(this.terrain, X3tox(X*100), Y3toy(12), 100*DM, 30*DM)
                //ctx.drawImage(this.terrain, X3tox(X*100), Y3toy(-4), 100*DM, 30*DM)

            }
        }

        if(dark_sky){
            dark_sky_value+=.001
            if(dark_sky_value>1) dark_sky_value=1
            ctx.globalAlpha = dark_sky_value
            ctx.fillStyle="black"
            ctx.fillRect(0, 0, can.width, can.height)
            ctx.fillStyle="white"
	    ctx.font=`bold 30px GameFont`;
            ctx.textAlign="center"
	    ctx.fillText("Alles Gute zum 18. Geburtstag!", can.width*.5, can.height*.15)
            ctx.globalAlpha = 1
        }
    }
}


class Particle{

    static create(sprite, X, Y, vX=0, vY=0.1, lifetime=30, size=1){
        particles.push(new Particle(sprite, X, Y, vX, vY, lifetime,size))   
    }



    static create_delay(delay, sprite, X, Y, vX=0, vY=0.1, lifetime=30, size=1.1){
        handleTimeout(()=>particles.push(new Particle(sprite, X, Y, vX, vY, lifetime,size)), delay)  
    }

    constructor(sprite, X, Y, vX, vY, lifetime, size){
        this.sprite = sprite
        this.my_lifetime = lifetime
        this.lifetime = lifetime
        this.vX = vX+(Math.random()-1)*0.05
        this.vY = vY+Math.random()*0.05
        this.X = X+Math.random()
        this.Y = Y+Math.random()

        this.size = (.5 + Math.random()*2)*size
    }

    update(){        
        this.X += this.vX -0.01
        this.Y += this.vY
        this.size*=1.01
        
        this.lifetime--
        if(this.lifetime<=-this.my_lifetime){
            var i = particles.indexOf(this);
            particles.splice(i, 1);
        }
    }

    draw(){
        ctx.globalAlpha = 1-(Math.abs(this.lifetime)/this.my_lifetime)

        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y), this.size*DM, this.size*DM)
        ctx.globalAlpha = 1

        if(DEBUG){
            ctx.strokeStyle = "white"
            ctx.strokeRect(Xtox(this.X), Ytoy(this.Y), this.size*DM, this.size*DM)
        }

    }

}




class SnowFlake extends Particle{
    static create(X){
        particles.push(new SnowFlake(X))   
    }   

    constructor(X){
        super(sprites.particles[21], X, ytoY(Math.random(can.height)), Math.random()*0.01, -0.1+Math.random()*0.04, 200, 1)   

        if(Math.random()>.99){
            this.scale = true 
        }  else {
            this.scale = false
        }
    }

    update(){        
        this.X += this.vX -0.01
        this.Y += this.vY
        if(this.scale) this.size*=1.01
        
        this.lifetime--
        if(this.lifetime<=-this.my_lifetime){
            var i = particles.indexOf(this);
            particles.splice(i, 1);
        }
    }

    draw(){
        ctx.globalAlpha = 1-(Math.abs(this.lifetime)/this.my_lifetime)

        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y), this.size*DM, this.size*DM)
        ctx.globalAlpha = 1

        if(DEBUG){
            ctx.strokeStyle = "white"
            ctx.strokeRect(Xtox(this.X), Ytoy(this.Y), this.size*DM, this.size*DM)
        }

    }
}

class GravityParticle extends Particle{
    static create(sprite, X, Y, W, H){
        particles.push(new GravityParticle(sprite, X, Y, W, H))   
    }   

    constructor(sprite, X, Y, W, H){
        super(sprite, X, Y, 0, .6+.3*Math.random(), 200, 1)   
        this.W = W
        this.H = H

        this.vA = Math.random()*.2
        this.angle = 0

    }

    update(){        
        this.X += this.vX -0.01
        this.Y += this.vY

        this.vY-=0.03

        this.angle+=this.vA
        
        this.lifetime--
        if(this.lifetime<=-this.my_lifetime){
            var i = particles.indexOf(this);
            particles.splice(i, 1);
        }
    }

    draw(){
        this.rotationCenterX = this.X + this.W/2
        this.rotationCenterY = this.Y + this.H/2

        ctx.save();
        ctx.translate(Xtox(this.rotationCenterX), Ytoy(this.rotationCenterY) );
        ctx.rotate( this.angle);
        ctx.translate( -Xtox(this.rotationCenterX), -Ytoy(this.rotationCenterY) ); 

        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y), this.W*DM, this.H*DM)
        ctx.globalAlpha = 1

        if(DEBUG){
            ctx.strokeStyle = "white"
            ctx.strokeRect(Xtox(this.X), Ytoy(this.Y), this.W*DM, this.H*DM)
        }

        ctx.restore()

    }
}


class GravityCoin extends Particle{

    static create(sprite, X, Y, W, H){
        particles.push(new GravityCoin(sprite, X, Y, W, H))   
    }   

    constructor(sprite, X, Y, W, H){
        super(sprite, X, Y, .02+.04*Math.random(), .3+.2*Math.random(), 200, 1)   
        this.W = W
        this.H = H
    }

    update(){        
        this.X += this.vX -0.01
        this.Y += this.vY

        this.vY-=0.03

        this.angle+=this.vA
        
        this.lifetime--
        if(this.lifetime<=-this.my_lifetime || this.Y<35){
            var i = particles.indexOf(this);
            particles.splice(i, 1);
        }
    }

    draw(){
        ctx.drawImage(this.sprite, Xtox(this.X), Ytoy(this.Y), this.W*DM, this.H*DM)
    }

}


function snowfall(){
    for(let X=xtoX(-10);X<=xtoX(can.width+10);X+=20){
        SnowFlake.create(X)

    }

    handleTimeout(snowfall, 300)
}

class BackgroundParticle extends Particle{
    static create(sprite, X, Y, vX=0, vY=0.1, lifetime=60, size=1){
        background_particles.push(new BackgroundParticle(sprite, X, Y, vX, vY, lifetime, size))   
    }

    update(){        
        this.X += this.vX
        this.Y += this.vY
        this.size*=1.01
        
        this.lifetime--
        if(this.lifetime<=-this.my_lifetime){
            var i = background_particles.indexOf(this);
            background_particles.splice(i, 1);
        }
    }

    

}

class DustParticle extends Particle{
    static create(sprite, X, Y, vX=0, vY=0.1, lifetime=30, size=1){
        sky_particles.push(new DustParticle(sprite, X, Y, vX, vY, lifetime, size))   
    }

    update(){        
        this.X += this.vX
        this.Y += this.vY
                
        this.lifetime--
        if(this.lifetime<=-this.my_lifetime){
            var i = sky_particles.indexOf(this);
            sky_particles.splice(i, 1);
        }
    }


    draw(){
        ctx.globalAlpha = 1-(Math.abs(this.lifetime)/this.my_lifetime)
    
        ctx.drawImage(this.sprite, this.X, this.Y, this.size*DM, this.size*DM)
        ctx.globalAlpha = 1

        if(DEBUG){
            ctx.strokeStyle = "white"
            ctx.strokeRect(this.X, this.Y, this.size*DM, this.size*DM)
        }

    }
    

}

class ParticleX4 extends Particle{
    static create(sprite, X, Y, vX=0, vY=0.1, lifetime=30, size=1){
        particles.push(new ParticleX4(sprite, X, Y, vX, vY, lifetime,size))   
    }

    draw(){
        ctx.globalAlpha = 1-(Math.abs(this.lifetime)/this.my_lifetime)

        ctx.drawImage(this.sprite, X4tox(this.X), Y4toy(this.Y), this.size*DM, this.size*DM)
        ctx.globalAlpha = 1

        if(DEBUG){
            ctx.strokeStyle = "white"
            ctx.strokeRect(X4tox(this.X), Y4toy(this.Y), this.size*DM, this.size*DM)
        }

    }
}



class Smoke{
    static create(sprite, X, Y, vX, vY, lifetime){
        smokes.push(new Smoke(sprite, X, Y, vX, vY, lifetime))
    }

    constructor(sprite, X, Y, vX, vY, lifetime){
        this.sprite = sprite
        this.X = X
        this.Y = Y
        this.vX = vX
        this.vY = vY
        this.lifetime = lifetime
    }
    
    update(){
         BackgroundParticle.create(this.sprite, this.X, this.Y, this.vX, this.vY, this.lifetime)  
    }
}




class BirdParticle extends Particle{
    static create(X, Y, vX=0, vY=0.1, type, size){
        sky_particles.push(new BirdParticle(X, Y, vX, vY, 500+Math.round(Math.random()*500), type, size))  
    }    

    constructor(X, Y, vX, vY, lifetime, type, size){
        
        if(type=="default"){
            var birds = [2, 4, 6]           
        } else if(type=="parrots"){
            var birds = [10, 12, 14]  
            vY*=.4
        }

        var index = birds[Math.floor(Math.random()*birds.length)]
        super(sprites.particles[index], X, Y, vX, vY, lifetime, size)

        this.type = type
        if(type=="default"){
            this.directionX = .04 
        } else if(type=="parrots"){
            this.directionX = -.08
        }

        this.sprite2 = sprites.particles[index+1] // second animation sprite
        this.draw_sprite = this.sprite
        this.draw_i = 1
        this.interval = 10+Math.round(Math.random()*15)
        this.fly_count = this.interval
    }

    update(){        
        this.X += this.vX + this.directionX
        this.Y += this.vY

        this.lifetime--
        if(this.lifetime<=-this.my_lifetime){
            var i = sky_particles.indexOf(this);
            sky_particles.splice(i, 1);
        }

        this.fly_count--
        if(this.fly_count==0){
            if(this.draw_i==1){
                this.draw_sprite = this.sprite2
                this.draw_i = 2
                this.fly_count=this.interval
            } else {
                this.draw_sprite = this.sprite
                this.draw_i = 1
                this.fly_count=this.interval
            }
        }
    }

    draw(){
        ctx.globalAlpha = 1-(Math.abs(this.lifetime)/this.my_lifetime)

        ctx.drawImage(this.draw_sprite, X2tox(this.X), Y2toy(this.Y), this.size*DM, this.size*DM)
        ctx.globalAlpha = 1

        if(DEBUG){
            ctx.strokeStyle = "white"
            ctx.strokeRect(X2tox(this.X), Y2toy(this.Y), this.size*DM, this.size*DM)
        }

    }
}

function loadPatterns(){
    patterns = {}
    for(var pattern of CURRENT_LEVEL_DATA.patterns){
        patterns[pattern[0]] = ctx.createPattern(sprites[pattern[0]][pattern[1]], "repeat")
    }
}

function addPortal(X, Y, W, H){
    portal_exists = true
    portals.push([X, Y, W, H])
}

function birdswarm(X, type){
    if(type=="default"){
        var count = 20 + Math.random()*20
    } else if(type=="parrots"){
        var count = 8 + Math.random()*5
    }

    for(var i=0;i<count;i++){
        BirdParticle.create(X+ Math.random(), -30, Math.random()*0.01, 0.12, type, 1.5)    
    }
    Sound.play("birds", .3)
}


function birdswarm_interval(type="default"){
    if(RUNNING){
        //console.log("BIRDS!")
        birdswarm(jonas.X-Math.random()*80, type)
        birdswarm(jonas.X+Math.random()*80, type)
    }
    handleTimeout(()=>birdswarm_interval(type), 5000 + Math.random()*8000)
} 


function dust(){
    if(RUNNING){
        let shift = -30*sun.size+Math.random()*60*sun.size

        for(let i=1+Math.random()*3; i>=0;i--){
            DustParticle.create(sprites.particles[17], sun.rX*can.width+shift, sun.rY*can.height+shift, -.2 + .4*Math.random(), -.2 + .4*Math.random(), 140, 2)    
        }
    }
    handleTimeout(()=>dust(), 1000 + Math.random()*2000)
}



class Button{

    static add(x, y, w, h, color, data, callback){
        return buttons.push(new Button(x,y,w,h, color, data, callback)) - 1
    }

    constructor(x, y, w, h, color, data, callback){
        this.x = x
        this.y = y
        this.w = w
        this.h = h
        this.color = color
        this.callback =  callback

        this.data = data

        this.hovering = false
        this.clicked = false
    }

    static remove(i){
        if (i > -1) {
          buttons.splice(i, 1);
        }
    }
    
    check_hover(mousex, mousey){
        let x = this.x(),
            y = this.y(),
            w = this.w(),
            h = this.h()
        
        if(mousex>x && mousex<x+w && mousey>y && mousey<y+h){
            this.hovering = true
        } else {
            this.hovering = false
            this.clicked = false
        }
    }

    check_click(mousex, mousey){
        let x = this.x(),
            y = this.y(),
            w = this.w(),
            h = this.h()

        if(mousex>x && mousex<x+w && mousey>y && mousey<y+h){
            this.clicked = true
            this.callback(this.data)
        } else {
            this.clicked = false
        }
    }

    draw(){
        try{
            let x = this.x(),
                y = this.y(),
                w = this.w(),
                h = this.h()

            ctx.lineWidth = this.hovering ? 2 : 1
            ctx.strokeStyle = this.color
            if(this.clicked){
                ctx.strokeRect(x+1, y+1, w-2, h-2)         
            } else {
                ctx.strokeRect(x, y, w, h)
            }            
        } catch(e){}

    }
}

function openInNewTab(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}


function loadGiftCodeUrl(){
    console.log("load url")
    openInNewTab("http://jonas18.rf.gd/verify.php/")
    show_code = false
}




function showTodo(todo){
    todo_tip = todo
    handleTimeout(()=>todo_tip="", 3000)
}

function showAchievement(a, b){
    achievement = [a,b]
    handleTimeout(()=>achievement=false, 4000)
}

/* //////////////////////////////////////////////////////////*/


//game loop

function run(){
    requestAnimationFrame(run);

    
    now = performance.now();
    delta = now - last;


    // get right framrate
    if(delta>interval){  
        last = now - (delta % interval)  //http://codetheory.in/controlling-the-frame-rate-with-requestanimationframe/

        if(RUNNING) {
            run_update()
            run_draw()
        }
        updateAlways()


        //calculate actual fps
        update_fps++
        if(update_fps>=20){
            fps = parseFloat((1000/delta).toFixed(1))
            update_fps = 0              
        }
    }

      

}


function run_update(){
    update()
    updateLevel()    
}


function run_draw(){

        

    // DEBUG ///////////////////////////////////////

    ctx.lineWidth = 1
    ctx.strokeStyle = "lightblue";
    ctx.strokeStyle = "blue";

    if (DEBUG){
        for (var y=can.height; y>=0; y-=10){
            ctx.beginPath();
            ctx.moveTo(0,Ytoy(y));
            ctx.lineTo(can.width,Ytoy(y));
            ctx.stroke(); 
            ctx.closePath()
        }

        for (var x=0; x<=can.width; x+=10){
            ctx.beginPath();
            ctx.moveTo(Xtox(x),0);
            ctx.lineTo(Xtox(x), can.height);
            ctx.stroke(); 
            ctx.closePath()
        }   
    }


    // Level content

    try{
    

    drawLevel()





    if(jonas.vehicle=="drone"){

        // calculate display height
        let height = vehicles[jonas.vehicle_index].Y/100
        if(height>1) height=1
        else if(height<-1)height=-1

        let percent = vehicles[jonas.vehicle_index].power_status

        percent = Math.round(percent*10)/10

        if(percent<0) percent=0


        ctx.fillStyle = "darkgrey"
        ctx.strokeStyle = "black"
        ctx.lineWidth = 1
        ctx.fillRect(12, 0.15*can.height, 5, 0.7*can.height)
        ctx.strokeRect(12, 0.15*can.height, 5, 0.7*can.height)

        ctx.fillStyle = "white"
        ctx.fillRect(10, 0.485*can.height-height*0.35*can.height, 9, 0.03*can.height)
        ctx.strokeRect(10, 0.485*can.height-height*0.35*can.height, 9, 0.03*can.height)
        

        ctx.textAlign="left"; 
        ctx.font=`15px GameFont`;
        ctx.fillStyle = "white"
        
        ctx.fillText("0 m", 23, 0.505*can.height)
        ctx.fillText("10 m", 23, 0.16*can.height)
        ctx.fillText("-10 m", 23, 0.855*can.height)


        if(percent<=0.3){
            ctx.fillStyle = "red"
        }

        ctx.fillRect(12, 0.9*can.height+30, 20, -30*percent)

        ctx.strokeRect(12, 0.9*can.height, 20, 30)
        ctx.strokeRect(17, 0.9*can.height, 10, -6)
       
        ctx.font=`13px GameFont`;
        ctx.fillText(`${percent*100} %`, 43,  0.9*can.height+28)



    }

    if(jonas.vehicle=="drone" && vehicles[jonas.vehicle_index].burning){
        burn_count++

        if(burn_count>20&&burn_count<40){
            ctx.textAlign="center"; 
            ctx.font=`14px GameFont`;
            ctx.fillStyle = "#e41010"

            ctx.fillText("Propeller beschädigt", can.width/2, can.height/2)    
            ctx.drawImage(sprites.particles[19], can.width/2-10, can.height/2-40, 20, 20)           
        } 

        if(burn_count>=40){
            burn_count=0
        }
      
    }
   

    }catch(e){}




    if (DEBUG) {
        ctx.lineWidth = 1
        ctx.strokeStyle = "red";

        for (var i=0;i<CURRENT_LEVEL_DATA.ground_borders.length;i++){
            var b = CURRENT_LEVEL_DATA.ground_borders[i]
            ctx.beginPath();
            ctx.moveTo(Xtox(b[1]),Ytoy(b[0]));
            ctx.lineTo(Xtox(b[2]), Ytoy(b[0]));  
            ctx.stroke(); 
            ctx.closePath()    

        }

        for (var i=0;i<CURRENT_LEVEL_DATA.side_borders.length;i++){
            var b = CURRENT_LEVEL_DATA.side_borders[i]
            ctx.beginPath();
            ctx.moveTo(Xtox(b[0]),Ytoy(b[1]));
            ctx.lineTo(Xtox(b[0]), Ytoy(b[2]));  
            ctx.stroke(); 
            ctx.closePath()    
        }

        ctx.strokeStyle = "green";

        for (var i=0;i<CURRENT_LEVEL_DATA.platforms.length;i++){
            var b = CURRENT_LEVEL_DATA.platforms[i]
            ctx.beginPath();
            ctx.moveTo(Xtox(b[1]),Ytoy(b[0]));
            ctx.lineTo(Xtox(b[2]), Ytoy(b[0]));  
            ctx.stroke(); 
            ctx.closePath()      
        }

        ctx.strokeStyle = "orange";

        for (var i=0;i<jumpables.length;i++){
            var b = jumpables[i]
            ctx.beginPath();
            ctx.moveTo(Xtox(b[1]),Ytoy(b[0]));
            ctx.lineTo(Xtox(b[2]), Ytoy(b[0]));  
            ctx.stroke(); 
            ctx.closePath()      
        }

        ctx.strokeStyle = "cyan";

        for (var i=0;i<CURRENT_LEVEL_DATA.ladders.length;i++){
            var b = CURRENT_LEVEL_DATA.ladders[i]
            ctx.strokeRect(Xtox(b[0]), Ytoy(b[1]), b[2]*DM, -b[3]*DM)
        }


        positionMarker(0,0, "white")

    }





    /** cool effect **/
    /* for (var x=0; x<=can.width; x+=10){
        ctx.beginPath();
        ctx.moveTo(Xtox(x),0);
        ctx.lineTo(x, can.height);
        ctx.stroke(); 
        ctx.closePath()
    } 

    */

    if(todo_tip && achievement==false){
        ctx.textAlign="center"; 
        ctx.font=`14px GameFont`;


        ctx.fillStyle = "black"

        ctx.fillText(todo_tip, can.width/2, can.height/2)    
    }

    


    if(achievement!=false){
        ctx.textAlign="center"; 
        ctx.font=`14px GameFont`;

        ctx.fillStyle = "black"

        ctx.fillText(achievement[0], can.width/2, can.height/2) 
        ctx.font=`bold 14px GameFont`;
        ctx.fillText(achievement[1], can.width/2, can.height/2+25) 
        
    }

    
    // Coins

    
    if(!coins_disabled){
        ctx.fillStyle = "#666"
        ctx.fillRect(can.width-78, can.height-40, 68, 30)

        ctx.textAlign="right"; 
        ctx.font=`14px GameFont`;
        ctx.fillStyle = "white"
        ctx.fillText(total_coins, can.width-39, can.height-20)


        ctx.drawImage(sprites.items[2], can.width-34, can.height-35, 20, 20)

    }
 

    if(GAME_OVER){



        let h = 0.5*can.height,
            w = 0.7*can.height,
            x = (can.width - w)/2
            y = 0.25*can.height



        quest_existing=false
        if(gameover_animation_count==100){

            if(!gameover_button_added){
                gameover_button_added=true
                Button.add(()=>(can.width - 0.7*can.height)/2+(7*(0.7*can.height)/16), ()=>0.25*can.height+(0.5*can.height)/1.35, ()=>(0.7*can.height)/8, ()=>(0.7*can.height)/8, "white", {}, ()=>{
                    console.log("Restart Level")
                    gameover_animation_finished()
                })
            }

        } else {
            gameover_animation_count++
        }

        if(gameover_animation_count<=50){
            var offsetY = can.height - (gameover_animation_count/50) * can.height       
        } else {
            var offsetY = 0
        }

        ctx.globalAlpha =  ((gameover_animation_count)/120)/2
        ctx.fillStyle = "black"
        ctx.fillRect(0,0,can.width, can.height)
        
        /*ctx.globalAlpha = 0.85

        ctx.strokeStyle = CURRENT_LEVEL_DATA.secondary_color
        ctx.fillStyle = CURRENT_LEVEL_DATA.border_color
        ctx.lineWidth = w/60
        ctx.fillRect(x, y - offsetY, w, h)
        */
        ctx.globalAlpha = 1

        //ctx.strokeRect(x, y - offsetY, w, h)




        ctx.textAlign="center"; 
        ctx.font=`bold ${w/20}px GameFont`;


        ctx.fillStyle = "white"

        ctx.fillText(gameover_message, x+w/2, y - offsetY+h/5)

        ctx.font=`bold ${w/35}px GameFont`;

        ctx.fillText(gameover_tip, x+w/2, y - offsetY+h/3.5)

        ctx.drawImage(sprites.gameover[CURRENT_LEVEL_DATA.gameover_icon], x+(3*w/8), y- offsetY+h/3, w/4, w/4)
        ctx.drawImage(sprites.uicontrols[0], x+(7*w/16), y- offsetY+h/1.35, w/8, w/8)


    }



    // draw UI / HUD
    if(!DEBUG){
        if(key_hint.length!=0){
            ctx.beginPath()

            ctx.drawImage(sprites.ui.key[0],10, 10, 40, 40)
            ctx.textAlign="start"; 
            ctx.font="30px Monospace";
            
            ctx.globalAlpha=.8
            ctx.fillStyle="white"
            ctx.fillRect(48, 16, key_hint_width, 28)
            ctx.globalAlpha=1

            ctx.fillStyle = CURRENT_LEVEL_DATA.border_color;
            ctx.fillText(key_hint[0], 20, 41)
            ctx.font="15px GameFont";

            ctx.fillStyle = CURRENT_LEVEL_DATA.border_color;
            ctx.fillText(key_hint[1], 55, 35)


            ctx.closePath()
        }
    }


    if(quest_existing){
        if(quest_count>100 && quest_finished){
            quest_finished_count++
        } else {
            quest_count++
        }

        if(quest_count<=50){
            var offsetY = 60 - (quest_count/50) * 60     
        } else {
            var offsetY = 0
        }




        ctx.globalAlpha =  (quest_count)/130         

        if(quest_finished_count>=250){
            ctx.globalAlpha =  1 - ((quest_finished_count-250)/100)   
            var offsetY = ((quest_finished_count-250)/100) * 60   
        } 


        ctx.font="16px GameFont";
        ctx.textAlign="start"; 


        ctx.fillStyle = "white"
        ctx.fillRect(can.width/2-quest_text_width/2, can.height + offsetY-60, quest_text_width, 50)

        if(quest_finished){
            ctx.strokeStyle = "#129d12"
            ctx.lineWidth=2
            ctx.strokeRect(can.width/2-quest_text_width/2, can.height + offsetY-60, quest_text_width, 50)
        }

        ctx.globalAlpha =  1


        ctx.fillStyle = "black"
        ctx.fillText(quest.text, can.width/2-quest_text_width/2+55, can.height + offsetY-30)


        if(!quest_finished){
            ctx.drawImage(sprites.ui.quest[0], can.width/2-quest_text_width/2+5, can.height + offsetY-55, 40, 40)
        }
        else {
            ctx.drawImage(sprites.ui.quest[1], can.width/2-quest_text_width/2+5, can.height + offsetY-55, 40, 40)
        }
        if(quest_finished_count>350){

            // reset quest
            quest_finished_count=0
            quest_count=0
            quest_finished=false
            quest = null
            quest_existing = false
        }

    }

    if(code_window!=false){
        code_window.draw()
    }




    if (show_code){
        let w
        if(mouse_is_down){
            w = can.width*.48
        } else {
            w = can.width*.5           
        }
        let h = w*(3/4)

        ctx.globalAlpha = .6
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, can.width, can.height)
        ctx.globalAlpha = 1

        ctx.fillStyle = "white";

        ctx.textAlign = "center"
        ctx.fillText("Deine gesammelten Münzen wurden in einen Geschenkgutschein umgewandelt!", can.width/2, can.height/2-w/3)
        
        ctx.drawImage(sprites.giftcard[0], can.width/2-w/2, can.height/2-h/2, w, h)
    }


    if (DEBUG){
        //show info
        //ctx.beginPath()
        ctx.textAlign="start"; 
        ctx.font="15px GameFont";
        ctx.fillStyle = "white";
        ctx.fillText(`fps: ${fps}`, 10, 20)
        ctx.fillText(`X: ${jonas.X.toFixed(1)}`, 10, 40)
        ctx.fillText(`Y: ${jonas.Y.toFixed(1)}`, 10, 60)
        ctx.fillText(`mouse: ${mouseX} | ${mouseY}`, 10, 80)


        ctx.font="bold 10px GameFont";
        ctx.fillStyle = "red";

        ctx.fillText(`Zum Beenden des DEBUG-MODUS 'I' drücken`, 10, 100)

        //ctx.fillText("x: "+Xtox(jonas.X).toFixed(1), 10, 80)
        //ctx.fillText("y: "+Ytoy(jonas.Y).toFixed(1), 10, 100)

        //ctx.closePath()
    }


 
    
    if(!loading && !controls_disabled){ // loading screen -> not other ui elements
        if(pause_animation){
            pause_animation_counter+=pause_step
            
            //console.log(pause_step)

            if(pause_step<0 && button_home!=false){
                Button.remove(button_home)
                button_home=false
            }
            if(pause_step<0 && button_reload!=false){
                Button.remove(button_reload)
                button_reload=false
            } 

            //console.log(pause_animation_shift_x)

            if( (pause_step>0 && pause_animation_counter<110) || (pause_step<0 && pause_animation_counter>0) ){
                pause_animation_shift_x = pause_animation_counter
            }
            else if( (pause_step>0 &&pause_animation_counter>110) || (pause_step<0 &&pause_animation_counter<0)) {
                pause_animation=false
                if(pause_step>0)  RUNNING=false
            } 



            ctx.fillStyle = CURRENT_LEVEL_DATA.border_color;
            ctx.fillRect(can.width-50-pause_animation_shift_x, 10, 40, 40)
            ctx.drawImage(sprites.uicontrols[3], can.width-50-pause_animation_shift_x, 10, 40, 40)

            if(pause_animation_shift_x>50){
                ctx.fillStyle = CURRENT_LEVEL_DATA.border_color;
                ctx.fillRect(can.width-0-pause_animation_shift_x, 10, 40, 40)
                ctx.drawImage(sprites.uicontrols[0], can.width-0-pause_animation_shift_x, 10, 40, 40)
                
                if(button_reload==false){
                    button_reload = Button.add(()=>can.width-pause_animation_shift_x, ()=>10, ()=>40, ()=>40, "white", {}, ()=>{
                         console.log("button_reload")
                         location.reload()
                    })                    
                }
 
                if(button_home==false){
                    button_home = Button.add(()=>can.width-50-pause_animation_shift_x, ()=>10, ()=>40, ()=>40, "white", {}, ()=>{
                         console.log("button_home")
                         loadScreen("home")
                    })                    
                }

            }

            if(pause_animation_shift_x>100){
                ctx.fillStyle = CURRENT_LEVEL_DATA.border_color;
                ctx.fillRect(can.width+50-pause_animation_shift_x, 10, 40, 40)
                ctx.drawImage(sprites.uicontrols[0], can.width+50-pause_animation_shift_x, 10, 40, 40)


            }
        }

        ctx.beginPath()

        ctx.fillStyle = CURRENT_LEVEL_DATA.border_color;
        ctx.fillRect(can.width-50, 10, 40, 40)


        ctx.fillStyle = CURRENT_LEVEL_DATA.secondary_color;
        ctx.fillRect(can.width-150-pause_animation_shift_x, 15, 100, 30)

        ctx.closePath()

        ctx.textAlign="end"; 
        ctx.font="bold 15px GameFont";
        ctx.fillStyle = "white";
        ctx.fillText("Level " + CURRENT_LEVEL, can.width-60-pause_animation_shift_x, 35)
        ctx.fill()   





        for(var button of buttons){
            button.draw()
        }




        if(RUNNING || GAME_AUTOSTOP || pause_animation){
            ctx.drawImage(sprites.uicontrols[2], can.width-50, 10, 40, 40)
        } else {
            ctx.drawImage(sprites.uicontrols[1], can.width-50, 10, 40, 40)

        }




    }
 

    ////////////////////////////////////////////////

    
    
}


function update(){  // todo replace names with numbers
    
    if (pressed_keys.includes("ArrowRight") || pressed_keys.includes("KeyD")){
        if(!jonas.using_vehicle){
            if(jonas.canMoveRight){
                jonas.vX=0.5
            } else {
                jonas.vX=0
            }            
        } else if(jonas.vehicle=="tractor"){
            vehicles[jonas.vehicle_index].vX+=0.05
            vehicles[jonas.vehicle_index].accelerate = 1
        } else if(jonas.vehicle=="drone" && !vehicles[jonas.vehicle_index].crashed && !vehicles[jonas.vehicle_index].burning){
            vehicles[jonas.vehicle_index].vX+=0.05
        } else if(jonas.vehicle=="sleigh" && !vehicles[jonas.vehicle_index].sliding){
            vehicles[jonas.vehicle_index].sliding=true          
        }

        
    }
    else if (pressed_keys.includes("ArrowLeft") || pressed_keys.includes("KeyA")){
        if(!jonas.using_vehicle){
            if(jonas.canMoveLeft){
                jonas.vX=-0.5
            } else {
                jonas.vX=0
            }
        } else if(jonas.vehicle=="tractor"){
            vehicles[jonas.vehicle_index].vX-=0.05
            vehicles[jonas.vehicle_index].accelerate = -1
        } else if(jonas.vehicle=="drone" && !vehicles[jonas.vehicle_index].crashed && !vehicles[jonas.vehicle_index].burning){
            vehicles[jonas.vehicle_index].vX-=0.05
        } else if(jonas.vehicle=="sleigh" && !vehicles[jonas.vehicle_index].sliding){
            vehicles[jonas.vehicle_index].sliding=true          
        }
    }
    else {
        try{jonas.vX=0}catch(e){}
    }
    
    if (pressed_keys.includes("ArrowUp") || pressed_keys.includes("KeyW")){
        if(!jonas.falldown && !jonas.using_vehicle){
            if (jonas.on_ladder){        //     if (DEBUG){
                jonas.vY=0
                jonas.Y+=0.6
            } else if (!jonas.in_air && !jonas.autojump_Y>0){
                jonas.vY+=2.1  // dont jump when already in air or autojumping
            }

            jonas.down = false
            jonas.H = 2*jonas.W //change height
        }  else if(jonas.using_vehicle && jonas.vehicle=="drone" && !vehicles[jonas.vehicle_index].crashed && !vehicles[jonas.vehicle_index].burning){
                vehicles[jonas.vehicle_index].vY+=.15
        } else if(jonas.vehicle=="sleigh" && vehicles[jonas.vehicle_index].on_ground){
            vehicles[jonas.vehicle_index].vY=2
            vehicles[jonas.vehicle_index].on_ground = false                 
        }  

    } 

    if (pressed_keys.includes("ArrowDown") || pressed_keys.includes("KeyS")){
        if (!jonas.using_vehicle){
            if (jonas.on_ladder){ 
                jonas.vY=0
                jonas.Y-=0.6
            } else {
                jonas.down = true
                jonas.H = 1.7*jonas.W //change height                
            }

        }  else if(jonas.vehicle=="drone" && !vehicles[jonas.vehicle_index].crashed && !vehicles[jonas.vehicle_index].burning){
                vehicles[jonas.vehicle_index].vY-=.15
        } 
    } else {
        try{if (!jonas.using_vehicle){
            jonas.down = false
            jonas.H = 2*jonas.W //change height
        } }catch(e){}
    }
    
}


function managePause(){
    if(RUNNING){
        pause()
    } else{
        resume()
    }
} 

function pause(){
    pause_animation=true
    pause_step=10
    Sound.pause()
}

function resume(){
    Sound.resume()
    pause_step=-10
    RUNNING=true
    pause_animation=true
    run()   // restart draw/update loop
}


function update_levels(){
  var head= document.getElementsByTagName('head')[0];
  var script= document.createElement('script');
  script.src= 'levels.js';
  head.appendChild(script);
  CURRENT_LEVEL_DATA = leveldata[CURRENT_LEVEL-1]

  items = []
  loadCoins()
}


// keys

document.onkeydown = (key)=>{

    if (key.code=="KeyI"){
        DEBUG=!DEBUG
        if(DEBUG){
            document.body.style.cursor = 'default';
        } else {
            //document.body.style.cursor = 'none';
        }
    } 

    else if (!controls_disabled && CONTROLLABLE && key.code=="Space"){
        if(!GAME_AUTOSTOP) managePause()
    }
    
    else if (CONTROLLABLE && key.code=="KeyE"){
        if(actionable_in_focus!=null && Object.keys(actionable_in_focus).length!=0){
            actionable_in_focus.action_callback(actionable_in_focus)
        }
    }

    else if(key.code=="Enter"){
        if(gameover_button_added){ // restart Level
            console.log("Restart")
            //gameover_animation_finished()
        }
        else if(actionable_in_focus!=null && Object.keys(actionable_in_focus).length!=0){
            actionable_in_focus.action_callback(actionable_in_focus)
        } 
    }

     
    else if (key.code=="KeyR"){
        location.reload()
    }

    else if (DEBUG && key.code=="KeyU"){
        console.log("update")
        update_levels()
        setTimeout(update_levels, 200)
    }

    else if(key.code=="KeyC"){
        restartLevel("Diese Level wurde manuell angehalten")
    }

    else if(DEBUG && key.code=="KeyN"){
        try{vehicles[jonas.vehicle_index].power_status=1}catch(e){}
    }

    else if(DEBUG && key.code=="KeyP"){
        console.log("switch particle visibility")
        NO_PARTICLES  = !NO_PARTICLES
    }

    else if(key.code=="KeyX"){
        localStorage.clear()
        location.reload()
    }

    else if(CONTROLLABLE && code_window!=false && code_window.keys.includes(key.key)){
        code_window.input(key.key)
    }

    else if (RUNNING && CONTROLLABLE && !pressed_keys.includes(key.code)){
        if(code_window!=false) CodeWindow.reset()
        pressed_keys.push(key.code)   
    }


}


function setCookie(name, value){
    document.cookie = name + "=" + value + "; expires=Tue, 24 Dec 2019 12:00:00 UTC"; + ";path=/";
}

document.onkeyup = (key)=>{
    remove(pressed_keys, key.code)
    //console.log("up, " + pressed_keys)
}


can.onmousemove = (e)=>{
    try{  // error if player/buttons not yet initialized

        if (DEBUG){
                mouseX = xtoX(e.offsetX).toFixed(1)
                mouseY = ytoY(e.offsetY).toFixed(1)           
        }  

        for(var button of buttons){
            button.check_hover(e.offsetX, e.offsetY)
        }
    }catch(e){}
}


can.onmousedown = (e)=>{
    try{  // error if buttons not yet initialized
        for(var button of buttons){
            button.check_click(e.offsetX, e.offsetY)
        } 
    }catch(e){}

    mouse_is_down = true


}

can.onmouseup = (e)=>{
    try{  // error if buttons not yet initialized
        for(var button of buttons){
            button.clicked = false
        } 
    }catch(e){}

    if(show_code && !loaded_code_url){
        loadGiftCodeUrl()
        loaded_code_url = true
    }

    mouse_is_down = false
}





// some functions / prototypes ...

function remove (array, element) {
    
    const index = array.indexOf(element);

    if (index !== -1) {
        array.splice(index, 1);
    }  
}

Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

Array.prototype.contains = function ( needle ) {
   for (i in this) {
       if (this[i].toString() == needle.toString()) return true;
   }
   return false;
}


// LEVEL


function loadLevel(l, callback){
    
    CURRENT_LEVEL = l
    CURRENT_LEVEL_DATA = leveldata[CURRENT_LEVEL-1]

    CURRENT_LEVEL_DATA.sprites = common_leveldata.sprites.concat(CURRENT_LEVEL_DATA.sprites).unique()
    CURRENT_LEVEL_DATA.sounds = common_leveldata.sounds.concat(CURRENT_LEVEL_DATA.sounds).unique()


    can.style.borderColor = CURRENT_LEVEL_DATA.border_color

    loadSprites( CURRENT_LEVEL_DATA.sprites,()=>{  // load sprites   [ "name" : number of sprites ]

        // callback

        loadSounds(CURRENT_LEVEL_DATA.sounds, ()=>{

            eval("LEVEL_"+CURRENT_LEVEL+"(callback)") 
        })
          
        
    })  // wait until all images are loaded 
}



function drawLevel(){



        background.draw()



        // sky particles
        if(!NO_PARTICLES){
            for (var particle of sky_particles){
                particle.draw()    
            }           
        }


        for(var object of background_objects){
            object.draw()
        }
        
        // hills
        for (var hillscape of hillscapes){
            hillscape.draw()
        }

        // in level 5 -> ground
        if(fill_underground!=false){
            for (let X=Math.floor(xtoX(0)/10)*10; X<xtoX(can.width);X+=10){
                for (let Y=Math.floor(ytoY(can.height)/10)*10; Y<=ytoY(-100);Y+=10){
                    if(!window_blocks.contains([X,Y]) && !(Y>75)) 
                    if(Y==70){
                        ctx.drawImage(sprites.ground[27], Xtox(X), Ytoy(Y), 10*DM, 10*DM)    
                    } else {                        
                        ctx.drawImage(fill_underground, Xtox(X), Ytoy(Y), 10*DM, 10*DM)    
                    }
                }
            }
        }

    
        // environment (blocks)
        map = CURRENT_LEVEL_DATA.map
        for (var s=0; s<map.length; s++){
           try{
               ctx.drawImage(sprites[map[s][0]][map[s][1]], Xtox(map[s][2]), Ytoy(map[s][3]), map[s][4]*DM+1, map[s][5]*DM+1)         
           }  
           catch(e){
               console.error(`ERROR drawing sprite "${map[s][0]}", ${map[s][1]}`)
               pause()
           }
        }

        
        //background particles
        if(!NO_PARTICLES){
            for (var particle of background_particles){
                particle.draw()    
            }
        }

        //objects
        for (var object of objects){
            object.draw()
        }

        for (var object of multilayer_objects){
            object.draw()
        }

        //physic_objects
        for (var object of physic_objects){
            object.draw()
        }

        

        for (var i=actionables.length-1;i>=0;i--){
            actionables[i].draw()
        }

        
        if(portal_exists){
            ctx.fillStyle = patterns.stars
            for (var i=0;i<portals.length;i++){
                ctx.fillRect(Xtox(portals[i][0]),Ytoy(portals[i][1]),portals[i][2]*DM,portals[i][3]*DM)
            }
        }


        for(var i=items.length-1;i>=0;i--){
            items[i].draw()
        }


        // vehicles
        for (var vehicle of vehicles){
            vehicle.draw()
        }


        // persons
        for (var person in persons){
            if(!persons[person].using_vehicle) persons[person].draw()
        }

        
        
       

        if(!jonas.using_vehicle) jonas.draw()

        if(!NO_PARTICLES){
            for (var particle of particles){
                particle.draw()    
            }
        }

        //foreground environment
        map = CURRENT_LEVEL_DATA.foreground_map
        for (var s=0; s<map.length; s++){
            ctx.drawImage(sprites[map[s][0]][map[s][1]], Xtox(map[s][2]), Ytoy(map[s][3]), map[s][4]*DM, map[s][5]*DM)
        }


        for (var object of foreground_objects){
            object.draw()
        }

        for (var object of multilayer_objects){
            object.draw_foreground()
        }

        // person speechbubbles
        for (var person in persons){
            persons[person].draw_speechbubble()
        }

        jonas.draw_speechbubble()

        if(sun!=false) sun.draw()



        if(pause_animation&&pause_animation_counter>70) {
            //console.log("filter")
            //stackBlurCanvasRGB("can", 0, 0, can.width, can.height, (pause_animation_counter-70)*2);
        } else if(GAME_OVER&&gameover_animation_count>50){
            //stackBlurCanvasRGB("can", 0, 0, can.width, can.height, (gameover_animation_count-50)*2);
        }

}


function updateAlways(){
    // update particles
     for (let smoke of smokes){
        smoke.update()
    }

    for (let particle of particles){
        particle.update()
    }
    for (let particle of background_particles){
        particle.update()
    }
    for (let particle of sky_particles){
        particle.update()
    }
}


function updateLevel(){
    
try{
    // update jonas / camera
    jonas.update()

    for (let person in persons){
        persons[person].update()
    }

    any_key_hint = 0 // important!
    for (let actionable of actionables){
        actionable.check(jonas.X+jonas.W/2, jonas.Y)
    }

    for (let object of physic_objects){
        object.update()
    }
    for (let vehicle of vehicles){
        vehicle.update()
    }

   

    for(var item of items){
        item.update()
    }
    
}catch(e){}

}
