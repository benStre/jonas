leveldata = [

// LEVEL 1
        {   

           coins: [
               [30, 6],
               [35, 6],
               [40, 12],
               [44, 12],
               [48, 12],
               [52, 12],
               [56, 12],
               [78, 23],
               [78, 23],
               [37, 33],

               [107, 49], 
               [119, 56], 
               [112, 54], 
               [127, 56], 
               [142, 68], 
               [183, 124], 
               [188, 124], 
               [193, 124], 
               [215, 145], 
               [220, 145], 
            ],          


            sprites: [  // sprite groups            
                "woodendoor",
                "hubatz",
                "benni",                
                "personwhite",

            ],

            outline_sprites: [ // sprites with coloured outline
                ["box", 0, 5],
                ["decoration", 1, 4],
                ["woodendoor", 0, 3]
            ],
            
            patterns : [],

            gameover_icon : 2,

            sounds: ["jump", "level1", "birds"],

            sky_color: {0:"#3E8EAD", .8:"#FFFFFF", .9:"#FFB08E"},
            border_color: "#07507B",
            secondary_color: "#2793e5",
            
            map: [
                ["ground", 6, -10, 0, 10, 10],  // sprite_group, sprite_index, x, y, w, h
                ["ground", 5, 0, 0, 10, 10],
                ["ground", 17, 10, 0, 10, 10],
                ["ground", 16, 20, 0, 10, 10],
                ["ground", 2, 30, 0, 10, 10],
                ["ground", 13, 40, 0, 10, 10],
                ["ground", 2,50, 0, 10, 10],
                ["ground", 15, 60, 0, 10, 10],              
                ["ground", 19, 30, -10, 10, 10],
                ["ground", 15, 40, -10, 10, 10],
                ["ground", 20, 50, -10, 10, 10],
                ["ground", 11, 30, 10, 10, 10],
                ["ground", 9, 40, 10, 10, 10],
                ["ground", 8, 50, 10, 10, 10],
                ["ground", 10, 60, 10, 10, 10],
              

                ["ground", 6, 31, 30, 10, 10],
                ["ground", 5, 41, 30, 10, 10],
                ["ground", 7, 51, 30, 10, 10],
                
                ["vegetation", 9, -5, 39.5, 20, 40],
                ["vegetation", 1, 15, 2.5, 6, 3],
                ["ground", 6, 65, 20, 10, 10],
                ["ground", 7, 75, 20, 10, 10],

                ["ground", 6, 70, 40, 10, 10],
                ["ground", 17, 80, 40, 10, 10],
                ["ground", 0, 90, 40, 10, 10],

                ["ground", 23, 90, 30, 10, 10],
                ["ground", 22, 100, 30, 10, 10],
                ["ground", 3, 100, 40, 10, 10],
                ["ground", 21, 110, 30, 10, 10],
                ["ground", 4, 110, 40, 10, 10],
                ["vegetation", 4, 104, 49, 10, 10],
                ["vegetation", 3, 98, 48, 10, 10],
                ["vegetation", 5, 110, 48.5, 10, 10],

                ["ground", 18, 120, 40, 10, 10],
                ["ground", 7, 130, 40, 10, 10],

                ["ground", 6, 140, 48, 10, 10],
                ["ground", 7, 150, 48, 10, 10],
                ["ground", 24, 160, 58, 10, 10],
                ["ground", 24, 165, 70, 10, 10],
                ["ground", 24, 170, 82, 10, 10],

                ["ground", 6, 136, 64, 10, 10],
                ["ground", 7, 146, 64, 10, 10],

                ["ground", 6, 178, 120, 10, 10],
                ["ground", 5, 188, 120, 10, 10],
                ["ground", 7, 198, 120, 10, 10],
                ["decoration", 2, 16, 9, 10, 10],


                ["vegetation", 10, 190, 121.5, 2, 2],
                ["vegetation", 10, 241, 135.5, 2, 2],
                ["vegetation", 10, 33, 6.5, 2, 2],
                ["vegetation", 10, 35, 7, 3, 3],
                ["vegetation", 10, 76, 21.5, 2, 2],

                ["ground", 6, 208, 124, 10, 10],
                ["ground", 17, 218, 124, 10, 10],
                ["ground", 4, 228, 124, 10, 10],
                ["ground", 23, 228, 114, 10, 10],
                ["ground", 21, 238, 114, 10, 10],
                ["vegetation", 1, 228, 126.5, 6, 3],
                ["ground", 2, 238, 124, 10, 10],
                ["ground", 9, 238, 134, 10, 10],
                ["ground", 11, 248, 143, 10, 10],
                ["ground", 0, 258, 138, 10, 10],
                ["ground", 1, 268, 138, 10, 10],
                ["ground", 15, 268, 128, 10, 10],
                ["ground", 8, 278, 138, 10, 10],
                ["ground", 10, 288, 138, 10, 10],
                ["ground", 15, 278, 128, 10, 10],
                ["ground", 15, 288, 128, 10, 10],

                ["decoration", 0, 258, 169, 16, 32],
                ["vegetation", 5, 151, 53.5, 6, 6],


                ["ground", 4, 248, 134, 10, 10],
                ["ground", 8, 258, 134, 10, 10],
                ["ground", 15, 248, 124, 10, 10],
                ["ground", 20, 258, 124, 10, 10],


                ["vegetation", 9, 85, 74.5, 17.5, 35],
                ["vegetation", 9, 80, 73.5, 17, 34],

                ["ground", 24, 308, 142, 10, 10],

                ["ground", 6, 318, 145, 10, 10],
                ["ground", 5, 328, 145, 10, 10],
                ["ground", 7, 338, 145, 10, 10],

            ],

            foreground_map:[
            ],

            ground_borders: // ground lines [y, xstart, xend] 
                [
                    [0, 0, 30],
                    [5, 30, 40],
                    [10, 40, 60],
                    [5, 60, 70],
                    [40, 100, 103],
                    [45, 103, 112],         
                    [40, 112, 136],   
                    [124, 212, 238],
                    [134, 238, 268],
                    [138, 280, 288],
                    [133, 288, 298]    
                ],

            side_borders: // side lines [x, ystart, yend] 
                [
                    [30, 0, 5],
                    [40, 5, 10],
                    [60, 5, 10],
                    [103, 40, 45],
                    [112, 40, 45],
                    [238, 124, 134],
                    [268, 124, 134],
                    [288, 133, 138],
                    [298, 123, 133],
                    [136, 35, 40],
                    [70, 0, 5],
                    [212, 119, 124],

                ],

            platforms: 
                [
                    [20, 69, 81],      
                    [30, 36, 57],
                    [40, 75, 100], 
                    [48, 145, 155],
                    [58, 162, 168],   
                    [70, 167, 173],   
                    [82, 172, 178],
                    [120, 183, 203],
                    [64, 140, 152],
                    [138, 250, 280],
                    [142, 309, 317],
                    [145, 322, 344]

                ],

           ladders : 
                [
               
                ]
                
          
        },


// LEVEL 2
        {
            coins : [
               [123, 17],
               [129, 16.5],        
               [135, 15.7],        
               [141, 14.8],        
               [147, 14],  
                     
               [213, 10],        
               [219, 11],        
               [225, 10.5],        
               [231, 10],        
               [237, 9.5],        

               [422, 32],        
               [428, 32.5],        
               [434, 32],        
               [440, 31],        
               [446, 29.5],        

               [680, 8.5],        
               [686, 9],        
               [692, 9.4],        
               [812, 32],        
               [818, 32.5],        

            ],


            sprites: [  // sprite groups
               "hubatz",
               "field",
               "winterdecoration",
               "sleigh",
               "woodendoor",

               "hannah",
               "janina",
               "oma",
               "opa",
               "elvi",
               "beate",
               "soni",
               "persongrey",
               "personbrown",
               "personwhite",
            ],


            gameover_icon : 4,

            outline_sprites: [ // sprites with coloured outline
                ["sleigh", 0, 3],
                ["woodendoor", 0],
            ],
            patterns : [
            ],
            sounds : ["level2", "miau", "crash"],

            sky_color: {0:"#000000", 0.5:"#131942", 0.8:"#572266", 0.87:"#6c6f87"},
            border_color: "#02003d",
            secondary_color: "#5e1d91",

            map:
                [
                   ["winterdecoration", 1, 10, 56, 30, 40],
                   ["winterdecoration", 1, 30, 54, 30, 40],
                   ["winterdecoration", 2, -13, 41.5, 15, 20],
                   ["winterdecoration", 2, -20, 36.5, 15, 15],
                   ["winterdecoration", 1, -62, 61, 30, 40],
                  
                   ["winterdecoration", 5, 70, 53.5, 26.7, 40],
                   ["winterdecoration", 5, 140, 49, 26.7, 40],
                   ["winterdecoration", 5, 210, 30, 26.7, 40],
                  
                   ["winterdecoration", 1, 305, 28, 30, 40],
                   ["winterdecoration", 0, 290, 25, 30, 40],
                   ["winterdecoration", 1, 280, 23.5, 30, 40],

                   ["winterdecoration", 2, 420, 20.5, 30, 30],
                   ["winterdecoration", 3, 560, 15, 25, 20],
                 
                   ["winterdecoration", 1, 600, 37.5, 30, 40],
                   ["winterdecoration", 8, 660, 12.5, 10, 10],
                   ["winterdecoration", 8, 740, 19, 15, 10],

                   ["winterdecoration", 4, 830, 56, 30, 40],
                   ["winterdecoration", 4, 890, 56.5, 30, 40],
                   
                   ["winterdecoration", 11, 1184, 38.5, 20, 7.9],
                   ["winterdecoration", 11, 1200, 37.5, 20, 6.7],

                   ["winterdecoration", 1, 1172, 69, 30, 40],

                   ["winterdecoration", 10, 1160, 68, 25, 12.5],
                   
                   ["winterdecoration", 4, 1021, 47, 30, 40],
                  
                   ["winterdecoration", 4, 1140, 78.5, 37.5, 50],
                   ["winterdecoration", 9, 1170, 43, 25, 12.5],
                   ["winterdecoration", 9, 1220, 43, 25, 12.5],
     
                ],

            foreground_map:[
            
            ],

            ground_borders: // ground lines [y, xstart, xend] 
                [
                  [19, 427, 440],
                  [7.5, 560, 584],
                  [10, 661, 668],
                  [17, 741, 752],
                ],

            side_borders: // side lines [x, ystart, yend] 
                [
                  [427, 13.8, 19],
                  [440, 13.8, 19],

                  [560, -.5, 7.5],
                  [584, -.5, 7.5],

                  [661, 1.8, 10],
                  [668, 1.8, 10],

                  [741, 9.3, 17],
                  [752, 9.3, 17],

                ],

            platforms: 
                [
                
                ],
            
            ladders : 
                [
               
                ]
        },


// LEVEL 3
        {
            coins: [
               [53, 1],
               [57, 1],
               [61, 1],
               [65, 1],
               [69, 1],

               [207, 50],
               [213, 52.5],
               [220, 54.5],
               [227, 55],
               [234, 54.5],

               [483, 23],
               [493, 27],
               [503, 33],
               [227, 55],
               [234, 54.5],

               [764, 10],
               [780, 9],
               [1133, 21],
               [1090, 185],
               [1095, 185],

            ],

            sprites: [  // sprite groups
             
                "vehicles",
                "field",
                "groundx",
                "windmill",
                "codeinput",
                "fieldground",
   
                "benni",
                "hannah",
                "janina",
                
                "personwhite",
                "personblue",
            ],

            gameover_icon : 1,

            outline_sprites: [ // sprites with coloured outline
                ["vehicles", 0, 5],
                ["windmill", 4, 2]
            ],

            patterns : [
                ["field", 0]
            ],

            sounds : [
                "level3", 
                "engine_start", 
                "engine_running", 
                "engine_accelerate",
                "beep",
                "electric_door",
                "error",
            ],

            sky_color: {0:"#0F5A78", .4:"#54A4C5", .7:"#87CEEB"},
            border_color: "#f76f3d",
            secondary_color: "#f98b2a",

            map:
                [
                    ["groundx", 0, 20, -10, 10, 10],
                    ["groundx", 0, 80, -6, 6, 6],
                    ["groundx", 1, 100, -8, 10, 10],
                    ["groundx", 1, 160, 5, 5, 5],
                    ["groundx", 2, 40, -2, 10, 4],
                    ["groundx", 2, 90, -3, 15, 6],
                    ["groundx", 0, 250, -13, 15, 15],
                    ["groundx", 0, 350, -6, 5, 5],
                    ["groundx", 1, 400, -9, 5, 5],

                    ["vegetation", 0, 29, 39, 20, 40],
                    ["vegetation", 2, 284, 9.5, 5, 10],

                    ["vegetation", 0, 660, 39, 20, 40],
                    ["vegetation", 0, 679, 42, 20, 40],

                    ["vegetation", 0, 340, 39.5, 20, 40],

                    ["decoration", 3, -6, 28, 50, 30],
                    
                    ["vegetation", 5, 1016, 3.3, 8, 8],
                    ["groundx", 0, 1015, -4, 5, 5],
                    ["groundx", 0, 990, -10, 8, 8],
                    ["groundx", 3, 1000, -8, 8, 8],
                    ["groundx", 0, 1028, -15, 10, 10],
                    ["vegetation", 4, 1010, 3.3, 8, 8],
                    
                    ["vegetation", 4, 1198, 4.8, 10, 10],
                    ["vegetation", 4, 1199, 13.8, 12, 12],
                    ["vegetation", 3, 1198, 10, 10, 10],
                    ["vegetation", 3, 1191, -1, 10, 10],
                    ["groundx", 0, 1171, -14, 10, 10],
                    ["vegetation", 0, 1150, 32, 25, 40],
                    
                    ["decoration", 7, 708, 17.5, 7, 17.5],
                    ["vegetation", 1, 705, 5, 8, 5],

                    ["fieldground", 1, 1090, 11, 10, 10],
                    ["fieldground", 0, 1100, 11, 10, 10],
                    ["fieldground", 2, 1110, 11, 10, 10],

                    ["fieldground", 1, 1125, 17, 10, 10],
                    ["fieldground", 2, 1130, 17, 10, 10],
                    
                    ["fieldground", 1, 1110, 28, 10, 10],
                    ["fieldground", 2, 1120, 28, 10, 10],

                    ["fieldground", 1, 1090, 36, 10, 10],
                    ["fieldground", 2, 1100, 36, 10, 10],

                    ["ground", 25, 1062, 180, 10, 10],


                    ["fieldground", 1, 1075, 185, 10, 10],
                    ["fieldground", 0, 1085, 185, 10, 10],
                    ["fieldground", 2, 1095, 185, 10, 10],
                    

                    ["ground", 25, 1070, 38, 10, 10],
                    ["ground", 25, 1055, 40, 10, 10],

                ],

            foreground_map:[
                    ["vegetation", 1, 280, 3.5, 8, 5],
                    ["vegetation", 2, 288, 4, 8, 6],
                    ["vegetation", 0, 654, 37, 20, 40],
                    ["decoration", 4, 350, 17.8, 10, 20],
                    ["vegetation", 2, 1026, 3.75, 8, 10],

                    ["ground", 25, 1040, 42, 10, 10],
                    

 

            ],

            ground_borders: // ground lines [y, xstart, xend] 
                [
                    [1, 1011, 1017],
                ],

            side_borders: // side lines [x, ystart, yend] 
                [
                  [500.5, 0, 20],
                  [1011, -3.5, 1],
                  [1017, -3.5, 1],
                  [1199, -2.6, 50],
                  [755.5, -200, 2.8],
                  [789.4, -200, -11.5],

                ],

            platforms: 
                [
                    [8, 1093, 1117],
                    [14, 1128, 1136],
                    [25, 1113, 1126],
                    [33, 1093, 1106],

                    [35, 1071, 1078],
                    [37, 1056, 1063],
                    [39, 1041, 1048],

                    [177, 1036, 1048],

                    [182, 1078, 1102],
                    [176, 1063, 1070],

                ],
            ladders : 
                [
                    [1041, 43, 1, 135]
                ]
              
        },




// LEVEL 4
        {

           coins: [
               [110, -3.6],
               [117, -2.6],
               [123, -3.4],
               [187, -8],
               [187, -22],

               [187, -38],
               [208, -68],
               [248, -60],
               [271, -28],
               [309, 13],

               [436, 16],
               [456, 17],
               [500, 16],
               [585, 40],
               [623, 51],

               [657, 59],
               [657, 59],
               [705, 50],
               [740, 72],
               [720, 95],

           ],


            sprites: [  // sprite groups
                "jungleground",
                "bridge",
                "drone",
                "waterfall",
                "rocks",
                "junglevegetation",
                "personorange",
                "soni",
            ],


            gameover_icon : 3,

            patterns : [],

            outline_sprites: [ // sprites with coloured outline
                ["drone", 0, 8],
                ["junglevegetation", 1, 2],

            ],
      
            sounds : ["level4", 
                      "birds", 
                      "background_birds", 
                      "waterfall", 
                      "drone", 
                      "crash", 
                      "alarm",
                      "magic",
                      ],

            sky_color: {0:"#4677a3", 0.5:"#9abfb0", 0.7:"#adedc8", 0.87:"#516841"},
            border_color: "#00a512",
            secondary_color: "#3ddb4f",

            map:
                [


                    ["jungleground", 1, -10, -8, 10, 10],
                    ["jungleground", 1, 0, -8, 10, 10],
                    ["jungleground", 1, 10, -8, 10, 10],
                    ["jungleground", 1, 20, -8, 10, 10],

  
                    ["jungleground", 4, -20, 0, 10, 10],

                    ["jungleground", 8, -10, -16, 10, 10],
                    ["jungleground", 6, 0, -16, 10, 10],
                    ["jungleground", 6, 10, -16, 10, 10],
                    ["jungleground", 6, 20, -16, 10, 10],
                    
                    ["jungleground", 0, -10, 0, 10, 10],
                    ["jungleground", 7, 0, 0, 10, 10],
                    ["jungleground", 7, 10, 0, 10, 10],
                    ["jungleground", 0, 20, 0, 10, 10],



                    ["jungleground", 2, 30, 0, 10, 20],
                    ["jungleground", 3, 67, 0, 10, 20],
                  

                    ["jungleground", 1, 77, -8, 10, 10],
                    ["jungleground", 0, 77, 0, 10, 10],
                    ["jungleground", 5, 87, 0, 10, 10],

                    ["jungleground", 3, 73, -5, 10, 20],
                    ["jungleground", 0, 83, -5, 10, 10],
                    ["jungleground", 7, 93, -5, 10, 10],
                    ["jungleground", 5, 103, -5, 10, 10],
                    ["jungleground", 4, 120, -3, 10, 10],
                    ["jungleground", 0, 130, -3, 10, 10],
                    ["jungleground", 0, 140, -3, 10, 10],
                    ["jungleground", 0, 150, -3, 10, 10],
                    ["jungleground", 5, 160, -3, 10, 10],

                    ["jungleground", 10, 138, -1, 20, 5],

                    ["jungleground", 2, 83, -5, 10, 20],

                    ["bridge", 1, 28.5, 6, 49, 15],

                    ["junglevegetation", 0, 70, 36.5, 30, 40],


                    ["jungleground", 11, -5, 12, 13, 15],
                    ["jungleground", 11, 0, 6.5, 10, 10],
                    

                    ["jungleground", 1, 340, -4, 10, 10],
                    ["jungleground", 1, 350, -4, 10, 10],
                    ["jungleground", 1, 360, -4, 10, 10],
                    ["jungleground", 1, 370, -4, 10, 10],
                    ["jungleground", 1, 380, -4, 10, 10],

                    ["jungleground", 8, 330, -4, 10, 10],
                    ["jungleground", 9, 380, -14, 10, 10],
                    ["jungleground", 6, 370, -14, 10, 10],
                    ["jungleground", 6, 360, -14, 10, 10],
                    ["jungleground", 6, 350, -14, 10, 10],
                    ["jungleground", 8, 340, -14, 10, 10],

                    ["jungleground", 4, 320, 5, 10, 10],
                    ["jungleground", 0, 330, 5, 10, 10],
                    ["jungleground", 0, 340, 4, 10, 10],
                    ["jungleground", 7, 350, 4, 10, 10],
                    ["jungleground", 0, 360, 4, 10, 10],
                    ["jungleground", 0, 370, 4, 10, 10],
                    ["jungleground", 7, 380, 4, 10, 10],
                    ["jungleground", 2, 390, 4, 10, 20],

                    ["junglevegetation", 0, 372, 40.5, 30, 40],
     
                    ["jungleground", 14, 550, -10, 20, 15],
                    ["jungleground", 14, 564, -8, 16, 14],


                    ["jungleground", 4, 320, -80, 10, 10],
                    ["jungleground", 7, 330, -80, 10, 10],
                    ["jungleground", 5, 340, -80, 10, 10],

                    ["jungleground", 4, 340, -60, 10, 10],
                    ["jungleground", 7, 350, -60, 10, 10],
                    ["jungleground", 5, 360, -60, 10, 10],
                        
                    ["jungleground", 4, 370, -24, 10, 10],
                    ["jungleground", 7, 380, -24, 10, 10],
                    ["jungleground", 5, 390, -24, 10, 10],

                    ["jungleground", 4, 210, -35, 10, 10],
                    ["jungleground", 5, 220, -35, 10, 10],
                    ["junglevegetation", 0, 203, 9, 36, 48],

                    ["jungleground", 11, 560, 4, 15, 20],
                    ["jungleground", 11, 550, 0, 12, 17],

                    ["jungleground", 12, 580, 0, 12, 17],
                    ["jungleground", 11, 590, -1, 12, 18],

                    ["jungleground", 13, 560, 50, 17, 23],
                    ["jungleground", 13, 550, 50, 15, 20],

                    ["jungleground", 15, 577, -5, 16, 24],
                    ["jungleground", 15, 557, 63, 16, 24],
                    ["jungleground", 14, 567, 59, 16, 14],

// pillar 1
                    ["jungleground", 16, 627, 19, 16, 14],
                    ["jungleground", 16, 627, 5, 16, 14],
                    ["jungleground", 16, 627, -9, 16, 14],                   
                    ["jungleground", 16, 627, -27, 16, 14],                    
                    ["jungleground", 15, 637, -13, 16, 24],
                    ["jungleground", 15, 627, -12, 16, 24],
                    ["jungleground", 16, 627, -36, 16, 14],
                    ["jungleground", 16, 627, -50, 16, 14],
                    ["jungleground", 14, 627, -58, 16, 14],
                    ["jungleground", 11, 628, 32, 16, 14],

// pillar 2

                    ["jungleground", 16, 697, 29, 16, 14],
                    ["jungleground", 16, 697, 15, 16, 14],
                    ["jungleground", 16, 697, 1, 16, 14],                    
                    ["jungleground", 16, 697, -17, 16, 14],                   
                    ["jungleground", 15, 697, -2, 16, 24],
                    ["jungleground", 16, 697, -26, 16, 14],
                    ["jungleground", 14, 697, -34, 16, 14],
                    ["jungleground", 11, 698, 42, 16, 14],

                    ["jungleground", 16, 697, -85, 16, 14],
                    ["jungleground", 16, 697, -99, 16, 14],                    
                    ["jungleground", 16, 697, -117, 16, 14],                   
                    ["jungleground", 15, 697, -102, 16, 24],
                    ["jungleground", 14, 697, -134, 16, 14],
                    ["jungleground", 11, 698, -72, 16, 14],

                    ["jungleground", 16, 697, -85, 16, 14],
                    ["jungleground", 16, 697, -99, 16, 14],                    
                    ["jungleground", 16, 697, -117, 16, 14],                   
                    ["jungleground", 15, 697, -102, 16, 24],
                    ["jungleground", 14, 697, -134, 16, 14],
                    ["jungleground", 11, 698, -72, 16, 14],


                    ["jungleground", 16, 700, 140, 16, 14],
                    ["jungleground", 16, 700, 154, 16, 14],
                    ["jungleground", 16, 700, 168, 16, 14],
                    ["jungleground", 15, 700, 176, 16, 18],
                    ["jungleground", 16, 700, 123, 16, 18],
                    ["jungleground", 15, 700, 135, 16, 20],
                    ["jungleground", 14, 700, 115, 20, 14],
     
                    ["jungleground", 16, 784, -10, 14, 12],
                    ["jungleground", 14, 784, 0, 14, 12],
                    ["jungleground", 16, 784, -20, 14, 12],
                    ["jungleground", 16, 784, -30, 14, 12],
                    ["jungleground", 14, 784, -33, 14, 12],
                    ["jungleground", 16, 784, -43, 14, 12],
                    ["jungleground", 16, 784, -52, 14, 12],
                    ["jungleground", 16, 784, -62, 14, 12],
                    ["jungleground", 16, 784, -72, 14, 12],
                    ["jungleground", 16, 784, -84, 14, 12],
                    ["jungleground", 13, 784, -78, 14, 12],
                    ["jungleground", 16, 784, -94, 14, 12],
                    ["jungleground", 16, 784, -104, 14, 12],
                    ["jungleground", 14, 784, -106, 14, 12],

                    ["jungleground", 16, 710, 90, 16, 14],
                    ["jungleground", 15, 710, 99, 16, 19],
                    ["jungleground", 16, 710, 77, 16, 14],
                    ["jungleground", 14, 706, 70, 20, 14],

                    ["jungleground", 8, 790, -3, 10, 10],
                    ["jungleground", 5, 800, -3, 10, 10],
                    ["jungleground", 0, 790, 5, 10, 10],
                    ["jungleground", 0, 800, 5, 10, 10],
                    ["jungleground", 0, 810, 5, 10, 10],
                    ["jungleground", 0, 780, 5, 10, 10],
                    ["jungleground", 13, 776, -1, 14, 12],
                    ["jungleground", 14, 772, 8, 14, 12],
                    ["jungleground", 13, 792, -6, 14, 12],

                    ["jungleground", 10, 792, 6, 20, 5],
                    ["jungleground", 5, 820, 5, 10, 10],
                    ["jungleground", 5, 810, 0, 10, 10],
                    ["jungleground", 0, 800, 0, 10, 10],
                    ["jungleground", 4, 790, 0, 10, 10],
                   
                    ["jungleground", 4, 840, 2, 10, 10],
                    ["jungleground", 5, 850, 2, 10, 10],
                    ["junglevegetation", 0, 830, 46, 36, 48],

                    ["jungleground", 13, 842, -40, 14, 20],
                
                    ["jungleground", 8, 840, -38, 10, 10],
                 
                    ["jungleground", 1, 910, -36, 10, 10],
                    ["jungleground", 9, 910, -46, 10, 10],

                    ["jungleground", 4, 820, -30, 10, 10],
                    ["jungleground", 0, 830, -30, 10, 10],
                    ["jungleground", 7, 840, -30, 10, 10],
                    ["jungleground", 2, 850, -30, 10, 20],
                    ["jungleground", 3, 889, -30, 10, 20],
                    ["jungleground", 13, 895, -39, 19, 23],
                    
                    
                    ["jungleground", 11, 925, -4, 24,28],
           
                    ["jungleground", 0, 899, -30, 10, 10],
                    ["jungleground", 14, 897, -30, 18, 14],
                    ["jungleground", 15, 902, -23, 19, 23],
                    ["jungleground", 14, 912, -25, 18, 14],
                    ["jungleground", 13, 926, -35, 23, 26],
                    ["jungleground", 15, 926, -21, 23, 25],

                    ["jungleground", 17, 927, -12, 10, 20],

                    ["bridge", 1, 850, -24, 49, 15],
// chrash island orange 49 100 -20
                    ["jungleground", 4, 515, -87, 10, 10],
                    ["jungleground", 0, 525, -87, 10, 10],
                    ["jungleground", 0, 535, -87, 10, 10],
                    ["jungleground", 7, 545, -87, 10, 10],
                    ["jungleground", 0, 555, -87, 10, 10],
                    ["jungleground", 0, 565, -87, 10, 10],
                    ["jungleground", 7, 575, -87, 10, 10],
                    ["jungleground", 5, 585, -87, 10, 10],
                  
                    ["jungleground", 18, 824, -13, 5, 20],
                    ["jungleground", 18, 824, 6, 5, 20],

                    ["junglevegetation", 0, 555, -43, 36, 48],

                ],
            
            foreground_map:[
                    ["bridge", 0, 28.5, 6, 49, 15],
                    ["bridge", 0, 850, -24, 49, 15],

            ],


            ground_borders: // ground lines [y, xstart, xend] 
                [
                    [-2, 0, 35],
                    [-2.5, 35, 71.5],
                    [-2, 71.5, 95],                 
                    [-7, 95, 110],
                    [-5.3, 122, 168],
                    [-37.2, 212.4, 227.4],
                    
                    [-3.3, 138.5, 157.5],


// big island with waterfall
                    [2.7, 322, 340],
                    [1.7, 340, 391],
                    [-21.3, 342, 387],
                    [-1.6, 391, 396],
                    [-14.6, 387, 396],
                    [-9.6, 330, 342],
                    [-4, 322, 330],

// small islands below
                    [-62.5, 342, 368],                   
                    [-69.5, 342, 368],     
                       
                    [-82.5, 322, 348],                   
                    [-89.5, 322, 348],  

                    [-26.3, 372, 398],                   
                    [-33.3, 372, 398],  


// rocks
                    [54, 552.3, 582.3],
                    [33, 552.3, 575],
                    [47, 575, 582.3],

                    [-20, 551, 600],
                    [-1.7, 551, 600],
// pillars
                    [30, 629, 641],
                    [-69, 629, 641],
                    [-21, 641, 652],
                    [-28, 641, 652],
                    [-45, 699, 711],
                    [40, 699, 711],

                    [65.2, 706, 712],
                    [58, 706, 724],
                    [92, 712, 724],

                    [103, 701, 719],

                    [-74, 699, 710],
//last island
                    [-10, 778, 786],
                    [-2.5, 773, 778],
                    [3.8, 773, 784],
                    [2.3, 784, 828],
                    [3.6, 792.5, 811.5],
                    [-3.3, 818, 828],
                    [-10, 795, 818],
                    [-32.5, 822, 856],
                    [-33, 856, 893],
                    [-32.5, 893, 910.4],
                    [-30.5, 910.4, 945],

// crash island         
                    [-90, 517, 590],
                ],



            side_borders: // side lines [x, ystart, yend] 
                [
                    
                    [792.5, 2.3, 3.6],
                    [811.5, 2.3, 3.6],
                    [818, -10, -3.3],
                    [795, -100, -10],
                    [822, -38, -32.5],
                    [856, -33, -32.5],
                    [893, -33, -32.5],
                    [910.4, -32.5, -30.5],
                    [936, -30.5, -12],

                    [95, -7, -2],
                    [110, -14, -7],
                    [35, -2.5, -2],
                    [71.5, -2.5, -2],
                    [122, -12, -5.3],
                    [168, -12, -5.3],

                    [ 138.5, -5.3, -3.3],
                    [ 157.5, -5.3, -3.3],

// big island with waterfall                   
                    [340, 1.7, 2.7],
                    [391, -1.6, 1.7],
                    [396, -14.6, -1.6],
                    [387, -21.3, -14.6],
                    [342, -21.3, -9.6],
                    [330, -9.6, -4],
                    [322, -4, 2.7],
// small islands below
                    [342, -69.5, -62.5],    
                    [368, -69.5, -62.5],    
                    
                    [322, -89.5, -82.5],
                    [348, -89.5, -82.5],

                    [372, -33.3, -26.3],
                    [398, -33.3, -26.3],
// rocks
                    [552.3, 33, 54],
                    [575, 33, 47],
                    [582.3, 47, 54],

                    [551, -20, -1.7],
                    [600, -20, -1.7],

// pillars
                    [629, -69, 30],
                    [641, -69, 30],

                    [652, -28, -21],
                    [699, -45, 40],
                    [711, -45, 40],
                    [712, 65.2, 92],
                    [724, 58, 92],
                    [706, 58, 65.2],

                    [701, 103, 135],
                    [719, 103, 135],
                    [699, -110, -74],
                    [710, -110, -74],
//last island
                    [786, -110, -10],
                    [778, -10, -2.5],
                    [773, -2.5, 3.8],
                    [784, 2.3, 3.8],
                    [828, -3.3, 2.3],

// crash island

                ],

            platforms: 
                [
                    [-0.3, 842, 857],
                ],
            
            ladders : 
                [
                     [826, -28, 1, 32]
                ]
        },

        
// LEVEL 5
        {
            coins: [
              [38.4, 2.6],
              [42.4, 2.6],
              [46.4, 2.6],
              [50.4, 2.6],
              [84, 26],
 
              [88, 26],
              [92, 26],
              [213, -36],
              [219, -32],
              [225, -30],
           
              [220, -3],
              [215, -3],
              [210, -3],
              [225, -3],
              [86.6, -44],

              [90, -42],
              [94, -41],
              [98, -41],
              [164, -16],
              [168, -16],

            ],

            sprites: [  // sprite groups
              "hubatz",
              "laboratory",
              "personblue",
              "ladders",
              "box",
              "lego",
              "personblue",
              "giftcard",
              "benni",
              "oma",
              "opa",
              "beate",
              "soni",
              "elvi",
              "hannah",
              "janina",
              "personbrown",
              "persongrey",
              "personwhite",
              "personorange",
              "junglevegetation",
              "rocket",
            ],
            

            gameover_icon : 5,

            outline_sprites: [ // sprites with coloured outline
              ["box", 1, 5],
              ["laboratory", 13, 4], 
              ["lego", 9, 3],
              ["rocket", 0, 3],
            ],
            patterns : [
            ],

            sounds : ["end", "rocket", "level5"],

            sky_color: {0:"#000000", 0.4:"#131942", 0.67:"#e69598"},
            border_color: "#bf203a",
            secondary_color: "#e5607f",

            map:
                [
                   ["laboratory", 0, 0, 0, 10, 10],
                   ["laboratory", 0, 10, 0, 10, 10],
                   ["laboratory", 0, 20, 0, 10, 10],
                   ["laboratory", 0, 30, 0, 10, 10],
                   ["laboratory", 0, 40, 0, 10, 10],
                   ["laboratory", 0, 50, 0, 10, 10],
                   ["laboratory", 0, 70, 0, 10, 10],
                   ["laboratory", 0, 80, 0, 10, 10],
                   ["laboratory", 0, 90, 0, 10, 10],
                   ["laboratory", 0, 100, 0, 10, 10],

                   ["laboratory", 1, 20, 40, 10, 10],
                   ["laboratory", 1, 30, 40, 10, 10],
                   ["laboratory", 1, 40, 40, 10, 10],
                   ["laboratory", 1, 50, 40, 10, 10],
                   ["laboratory", 1, 60, 40, 10, 10],
                   ["laboratory", 1, 70, 40, 10, 10],
                   ["laboratory", 1, 80, 40, 10, 10],
                   ["laboratory", 1, 90, 40, 10, 10],
                   ["laboratory", 1, 100, 40, 10, 10],


                   ["laboratory", 4, 0, 20, 10, 10],
                   ["laboratory", 5, 10, 20, 10, 10],
                   ["laboratory", 6, 0, 10, 10, 10],
                   ["laboratory", 7, 10, 10, 10, 10],  

                   ["laboratory", 4, 0, 40, 10, 10],
                   ["laboratory", 5, 10, 40, 10, 10],
                   ["laboratory", 6, 0, 30, 10, 10],
                   ["laboratory", 7, 10, 30, 10, 10],  
                   ["laboratory", 6, 0, 50, 10, 10],
                   ["laboratory", 7, 10, 50, 10, 10],  
                   ["laboratory", 4, 0, 60, 10, 10],
                   ["laboratory", 4, 10, 60, 10, 10],  
                   ["laboratory", 6, 0, 70, 10, 10],
                   ["laboratory", 7, 10, 70, 10, 10],  
                                      
                   ["laboratory", 6, 20, 30, 10, 10],
                   ["laboratory", 7, 30, 30, 10, 10],  
                   ["laboratory", 4, 40, 30, 10, 10],
                   ["laboratory", 5, 50, 30, 10, 10],  
                   ["laboratory", 5, 60, 30, 10, 10],  
                   ["laboratory", 4, 70, 30, 10, 10],  
                   ["laboratory", 6, 80, 30, 10, 10],  
                   ["laboratory", 7, 90, 30, 10, 10],  
                   ["laboratory", 4, 100, 30, 10, 10],  
                   ["laboratory", 6, 100, 20, 10, 10],  
                   ["laboratory", 5, 100, 10, 10, 10],  

                   ["laboratory", 4, 0, 80, 10, 10],
                   ["laboratory", 5, 10, 80, 10, 10],  
                   ["laboratory", 6, 0, 90, 10, 10],
                   ["laboratory", 7, 10, 90, 10, 10], 
                   ["laboratory", 6, 0, 100, 10, 10],
                   ["laboratory", 7, 10, 100, 10, 10], 
                   ["laboratory", 4, 0, 110, 10, 10],
                   ["laboratory", 4, 10, 110, 10, 10], 
                   ["laboratory", 4, 0, 120, 10, 10],
                   ["laboratory", 5, 10, 120, 10, 10],   
                                                                                                                                                                  
                   ["laboratory", 4, 20, 20, 10, 10],
                   ["laboratory", 6, 20, 10, 10, 10],                   
                   ["laboratory", 5, 60, 20, 10, 10],
                   ["laboratory", 7, 60, 10, 10, 10],  
                   ["laboratory", 7, 60, 0, 10, 10],  

                      
                   ["laboratory", 2, 30, 10, 10, 10],
                   ["laboratory", 2, 40, 10, 10, 10],
                   ["laboratory", 2, 50, 10, 10, 10],
                   ["laboratory", 2, 30, 20, 10, 10],
                   ["laboratory", 2, 40, 20, 10, 10],
                   ["laboratory", 2, 50, 20, 10, 10],   

                      
                   ["laboratory", 2, 70, 10, 10, 10],
                   ["laboratory", 2, 80, 10, 10, 10],
                   ["laboratory", 2, 90, 10, 10, 10],
                   ["laboratory", 2, 70, 20, 10, 10],
                   ["laboratory", 2, 80, 20, 10, 10],
                   ["laboratory", 2, 90, 20, 10, 10],       

                   ["laboratory", 9, 2, 120, 2, 120],       
                   ["laboratory", 9, 17, 120, 2, 120],       
           

                   ["laboratory", 7, 60, -10, 10, 10],   
                   ["laboratory", 6, 60, -20, 10, 10],   
                   ["laboratory", 5, 60, -30, 10, 10],   
                   ["laboratory", 7, 60, -40, 10, 10],   


                   ["ladders", 0, 60, 0, 10, 10],
                   ["ladders", 0, 60, -10, 10, 10],
                   ["ladders", 0, 60, -20, 10, 10],
                   ["ladders", 0, 60, -30, 10, 10],
                   ["ladders", 0, 60, -40, 10, 10],

                   ["laboratory", 10, -5, 120, 5, 130],  
                   ["laboratory", 10, 20, 120, 5, 86.5],  


                   ["laboratory", 10, 29, 30, 3, 30],  
                   ["laboratory", 10, 58, 30, 3, 83],  
                   ["laboratory", 10, 69, 30, 3, 80],  
                   ["laboratory", 10, 98, 30, 3, 30],  

                   ["laboratory", 10, 108, 34, 5, 38],  

                   ["lego", 4, 70, -20, 10, 10],  
                   ["lego", 4, 80, -20, 10, 10],  
                   ["lego", 4, 90, -20, 10, 10],  
                   ["lego", 4, 100, -20, 10, 10],  
                   ["lego", 4, 70, -30, 10, 10],  
                   ["lego", 4, 80, -30, 10, 10],  
                   ["lego", 4, 90, -30, 10, 10],  
                   ["lego", 4, 100, -30, 10, 10],                    
                   ["lego", 4, 70, -40, 10, 10],  
                   ["lego", 4, 80, -40, 10, 10],  
                   ["lego", 4, 90, -40, 10, 10],  
                   ["lego", 4, 100, -40, 10, 10],  

                   ["lego", 4, 110, -20, 10, 10],  
                   ["lego", 4, 120, -20, 10, 10],  
                   ["lego", 4, 110, -30, 10, 10],  
                   ["lego", 4, 120, -30, 10, 10],  
                   ["lego", 4, 110, -40, 10, 10],  
                   ["lego", 4, 120, -40, 10, 10],  

                   ["lego", 4, 130, -40, 10, 10],  
                   ["lego", 4, 130, -30, 10, 10],  
                   ["lego", 4, 130, -20, 10, 10],  
                   ["lego", 4, 130, -10, 10, 10],  
                   ["lego", 4, 130, 0, 10, 10],  
                   ["lego", 4, 130, 10, 10, 10],  
                  
                   ["lego", 4, 140, 10, 10, 10],  
                   ["lego", 4, 140, 0, 10, 10],  
                   ["lego", 4, 140, -10, 10, 10],  
                   ["lego", 4, 140, -20, 10, 10],  
                   ["lego", 4, 140, -30, 10, 10],  
                   ["lego", 4, 140, -40, 10, 10],  
                   
                   ["lego", 4, 150, 10, 10, 10],  
                   ["lego", 4, 150, 0, 10, 10],  
                   ["lego", 4, 150, -10, 10, 10],  
                   ["lego", 4, 150, -20, 10, 10],  
                   ["lego", 4, 150, -30, 10, 10],  
                   ["lego", 4, 150, -40, 10, 10], 

                   ["lego", 4, 160, 10, 10, 10],  
                   ["lego", 4, 160, 0, 10, 10],  
                   ["lego", 4, 160, -10, 10, 10],  
                   ["lego", 4, 160, -20, 10, 10],  
                   ["lego", 4, 160, -30, 10, 10],  
                   ["lego", 4, 160, -40, 10, 10], 

                   ["lego", 4, 170, -40, 10, 10], 
                   ["lego", 4, 170, -30, 10, 10], 
                   ["lego", 4, 170, -20, 10, 10], 
                   ["lego", 4, 170, -10, 10, 10], 
                   ["lego", 4, 170, 0, 10, 10], 
                   ["lego", 4, 170, 10, 10, 10], 

                   ["lego", 4, 180, -40, 10, 10], 
                   ["lego", 4, 180, -30, 10, 10], 
                   ["lego", 4, 180, -20, 10, 10], 
                   ["lego", 4, 180, -10, 10, 10], 
                   ["lego", 4, 180, 0, 10, 10], 
                   ["lego", 4, 180, 10, 10, 10],
                   
                   ["lego", 4, 190, -40, 10, 10], 
                   ["lego", 4, 190, -30, 10, 10], 
                   ["lego", 4, 190, -20, 10, 10], 
                   ["lego", 4, 190, -10, 10, 10], 
                   ["lego", 4, 190, 0, 10, 10], 
                   ["lego", 4, 190, 10, 10, 10],

                   ["lego", 4, 200, -40, 10, 10], 
                   ["lego", 4, 200, -30, 10, 10], 
                   ["lego", 4, 200, -20, 10, 10], 
                   ["lego", 4, 200, -10, 10, 10], 
                   ["lego", 4, 200, 0, 10, 10], 
                   ["lego", 4, 200, 10, 10, 10],

                   ["lego", 4, 210, -40, 10, 10], 
                   ["lego", 4, 210, -30, 10, 10], 
                   ["lego", 4, 210, -20, 10, 10], 
                   ["lego", 4, 210, -10, 10, 10], 
                   ["lego", 4, 210, 0, 10, 10], 
                   ["lego", 4, 210, 10, 10, 10],

                   ["lego", 4, 220, 10, 10, 10],
                   ["lego", 4, 220, 0, 10, 10],
                   ["lego", 4, 220, -10, 10, 10],
                   ["lego", 4, 220, -20, 10, 10],
                   ["lego", 4, 220, -30, 10, 10],
                   ["lego", 4, 220, -40, 10, 10],

                   ["lego", 4, 230, 10, 10, 10],
                   ["lego", 4, 230, 0, 10, 10],
                   ["lego", 4, 230, -10, 10, 10],
                   ["lego", 4, 230, -20, 10, 10],
                   ["lego", 4, 230, -30, 10, 10],
                   ["lego", 4, 230, -40, 10, 10],

                   ["lego", 7, 125, -40, 10, 10],  
                   ["lego", 7, 125, -31, 10, 10],  
                   ["lego", 7, 125, -22, 10, 10],  
                   ["lego", 7, 125, 5, 10, 10],  
                   ["lego", 7, 125, 14, 10, 10],  

                   ["lego", 2, 120, -13, 10, 10],  


                   ["lego", 2, 70, -10, 10, 10],  
                   ["lego", 2, 80, -10, 10, 10],  
                   ["lego", 2, 90, -10, 10, 10],  
                   ["lego", 2, 100, -10, 10, 10],  
                   ["lego", 2, 110, -10, 10, 10],  
                   ["lego", 2, 120, -10, 10, 10],  

                   ["lego", 2, 120, -7, 10, 10],  
                   ["lego", 7, 125, -4, 10, 10],  



                   ["lego", 2, 130, -43, 10, 10],  
                   ["lego", 2, 140, -43, 10, 10],  
                   ["lego", 2, 150, -43, 10, 10],  
                   ["lego", 2, 160, -43, 10, 10],  
                   ["lego", 2, 170, -43, 10, 10],  
                   ["lego", 2, 180, -43, 10, 10],  
                   ["lego", 2, 190, -43, 10, 10],  
                   ["lego", 2, 200, -43, 10, 10],  
                   ["lego", 2, 210, -43, 10, 10],  
                   ["lego", 2, 220, -43, 10, 10],  
                   ["lego", 2, 230, -43, 10, 10],  

                   ["lego", 3, 60, -43, 10, 10],  
                   ["lego", 3, 70, -43, 10, 10],  
                   ["lego", 3, 80, -43, 10, 10],  
                   ["lego", 3, 90, -43, 10, 10],  
                   ["lego", 3, 100, -43, 10, 10],  
                   ["lego", 3, 110, -43, 10, 10],  
                   ["lego", 3, 125, -46, 10, 10],  
                   ["lego", 3, 135, -46, 10, 10],  
                   ["lego", 0, 145, -46, 10, 10],  
                   ["lego", 3, 155, -46, 10, 10],  
                   ["lego", 3, 165, -46, 10, 10],  
                   ["lego", 0, 175, -46, 10, 10],  
                   ["lego", 3, 185, -46, 10, 10],  
                   ["lego", 3, 195, -46, 10, 10],  
                   ["lego", 0, 205, -46, 10, 10],  


                   ["lego", 3, 225, -46, 10, 10],                     
                   ["lego", 3, 215, -46, 10, 10],  

                   ["lego", 0, 235, -46, 10, 10],  

                   ["lego", 3, 120, -43, 10, 10],  

                   ["lego", 0, 90, -40, 10, 10],  
                   ["lego", 1, 100, -40, 10, 10],  
                   ["lego", 0, 97.5, -37, 10, 10],  

                   ["lego", 5, 140, -5, 75, 30],  

                   ["lego", 6, 85, -27.5, 10, 10],  

                   ["lego", 7, 240, -43, 10, 10],  
                   ["lego", 7, 240, -34, 10, 10],  
                  

                   ["lego", 1, 235, -30, 10, 10],  

                   ["lego", 7, 240, -27, 10, 10],  
                   ["lego", 7, 240, -18, 10, 10],  
                   ["lego", 7, 240, -9, 10, 10],  
                   ["lego", 7, 240, 0, 10, 10],  
                   ["lego", 7, 240, 7, 10, 10],  
                   ["lego", 2, 235, 16, 10, 10],  
                   //["lego", 2, 235, 16, 10, 10],  


                   ["lego", 1, 208, -35, 10, 10],  
                   ["lego", 1, 230, -27, 10, 10],  

                   ["lego", 2, 180, -14, 10, 10],  
                   ["lego", 2, 163, -12, 10, 10],  
                   ["lego", 2, 161, 1, 10, 10],  

                   ["laboratory", 4, 155, 40, 10, 10],       
                   ["laboratory", 5, 165, 40, 10, 10],       
                   ["laboratory", 6, 155, 30, 10, 10],       
                   ["laboratory", 7, 165, 30, 10, 10],       
                   ["laboratory", 4, 155, 20, 10, 10],       
                   ["laboratory", 5, 165, 20, 10, 10], 
                   
                   ["laboratory", 4, 175, 40, 10, 10],       
                   ["laboratory", 5, 185, 40, 10, 10],       
                   ["laboratory", 6, 175, 30, 10, 10],       
                   ["laboratory", 7, 185, 30, 10, 10],       
                   ["laboratory", 4, 175, 20, 10, 10],       
                   ["laboratory", 5, 185, 20, 10, 10], 

                   ["laboratory", 2, 200, 30, 10, 10],       
                   ["laboratory", 2, 210, 30, 10, 10],       
                   ["laboratory", 2, 220, 30, 10, 10],       
                   ["laboratory", 2, 200, 20, 10, 7],       
                   ["laboratory", 2, 210, 20, 10, 7],       
                   ["laboratory", 2, 220, 20, 10, 7],  
                        
                   ["laboratory", 2, 200, 40, 10, 10],       
                   ["laboratory", 2, 210, 40, 10, 10],       
                   ["laboratory", 2, 220, 40, 10, 10],       
                   ["laboratory", 10, 192.5, 47, 10, 37],       

                   ["laboratory", 4, 230, 50, 10, 10],
                   ["laboratory", 5, 240, 50, 10, 10],
                   ["laboratory", 5, 250, 50, 10, 10],
                   ["laboratory", 5, 220, 50, 10, 10],
                   ["laboratory", 7, 210, 50, 10, 10],
                   ["laboratory", 6, 200, 50, 10, 10],
                   
            

                   ["laboratory", 10, 152, 73, 8, 62],       


                   ["laboratory", 1, 125, 19.5, 10, 10],       
                   ["laboratory", 1, 135, 19.5, 10, 10],       
                   ["laboratory", 1, 145, 19.5, 10, 10],       
         

                   ["laboratory", 4, 230, 40, 10, 10],       
                   ["laboratory", 5, 240, 40, 10, 10],       
                   ["laboratory", 6, 230, 30, 10, 10],       
                   ["laboratory", 7, 240, 30, 10, 10],       
                   ["laboratory", 7, 230, 20, 10, 10],       
                   ["laboratory", 6, 240, 20, 10, 10],       
                   ["laboratory", 4, 250, 20, 10, 10],       
                   ["laboratory", 4, 250, 30, 10, 10],       
                   ["laboratory", 5, 250, 40, 10, 10],    

                   ["laboratory", 4, 260, 30, 10, 10],       
                   ["laboratory", 5, 260, 40, 10, 10],    
                   ["laboratory", 5, 260, 22.5, 10, 10],    
                   ["laboratory", 5, 260, 50, 10, 10],    
                   ["laboratory", 5, 270, 50, 10, 10],    
                   ["laboratory", 5, 270, 40, 10, 10],    
                   ["laboratory", 5, 270, 26, 10, 10],    
                   ["laboratory", 5, 270, 30, 10, 10],    
                   ["laboratory", 5, 280, 32, 10, 10],    
                   ["laboratory", 5, 280, 40, 10, 10],    
                   ["laboratory", 5, 280, 50, 10, 10],    
                   ["laboratory", 4, 280, 60, 10, 10],    
                   ["laboratory", 4, 290, 60, 10, 10],    
                   ["laboratory", 5, 290, 50, 10, 10],    
                   ["laboratory", 5, 290, 40, 10, 10],    
                   ["laboratory", 4, 300, 70, 10, 10],    
                   ["laboratory", 5, 300, 60, 10, 10],    
                   ["laboratory", 5, 300, 50, 10, 10],    
                   ["laboratory", 5, 300, 47, 10, 10],    
                   ["laboratory", 5, 310, 70, 10, 10],    
                   ["laboratory", 5, 310, 60, 10, 10],    
                   ["laboratory", 5, 310, 50, 10, 10],    
                   ["laboratory", 5, 320, 70, 10, 10],    
                   ["laboratory", 7, 320, 57, 10, 10],    
                   ["laboratory", 5, 320, 60, 10, 10],    
                   ["laboratory", 5, 330, 70, 10, 10],    
                   ["laboratory", 5, 330, 62, 10, 10],    
                   ["laboratory", 5, 340, 70, 10, 10],    
                   ["laboratory", 5, 350, 70, 10, 5],    

                   ["laboratory", 1, 155, 46.7, 10, 10],       
                   ["laboratory", 1, 165, 46.7, 10, 10],       
                   ["laboratory", 1, 175, 46.7, 10, 10],       
                   ["laboratory", 1, 185, 46.7, 10, 10],       
                   ["laboratory", 1, 195, 56.7, 10, 10],       
                   ["laboratory", 1, 205, 56.7, 10, 10],       
                   ["laboratory", 1, 215, 56.7, 10, 10],       
                   ["laboratory", 1, 225, 56.7, 10, 10],                 
                   ["laboratory", 1, 235, 56.7, 10, 10],                 
                   ["laboratory", 1, 245, 56.7, 10, 10],                 
                   ["laboratory", 1, 255, 56.7, 10, 10],                 
                   ["laboratory", 1, 265, 56.7, 10, 10],     
 
                   ["laboratory", 1, 280, 82.7, 5, 35],     
                   ["laboratory", 1, 275, 56.7, 10, 10],     
                   ["laboratory", 1, 285, 66.7, 10, 10],     
                   ["laboratory", 1, 295, 66.7, 10, 10],     
                   ["laboratory", 1, 300, 90.7, 5, 35],     
                   ["laboratory", 1, 300, 76.7, 10, 10],     
                   ["laboratory", 1, 310, 76.7, 10, 10],     

                   ["laboratory", 10, 227, 47, 10, 37],       


                   ["laboratory", 0, 155, 13, 10, 10],       
                   ["laboratory", 0, 185, 13, 10, 10],       
                   ["laboratory", 0, 195, 13, 10, 10],       
                   ["laboratory", 0, 205, 13, 10, 10],       
                   ["laboratory", 0, 215, 13, 10, 10],       
                   ["laboratory", 0, 225, 13, 10, 10],       
                   ["laboratory", 0, 235, 13, 10, 10],       
                   ["laboratory", 0, 245, 13, 10, 10],       
                   ["laboratory", 0, 255, 13, 10, 10],       
                       

                   ["ladders", 0, 228, 22, 10, 10],
                   ["ladders", 0, 228, 32, 10, 10],
                   ["ladders", 0, 228, 42, 10, 10],

                   ["laboratory", 0, 265, 16, 10, 10],       
                   ["laboratory", 0, 270, 19, 10, 10],       
                   ["laboratory", 0, 275, 22, 10, 10],       
                   ["laboratory", 0, 280, 25, 10, 10],       
                   ["laboratory", 0, 285, 28, 10, 10],       
                   ["laboratory", 0, 290, 31, 10, 10],       
                   ["laboratory", 0, 295, 34, 10, 10],       
                   ["laboratory", 0, 300, 37, 10, 10],       
                   ["laboratory", 0, 305, 40, 10, 10],       
                   ["laboratory", 0, 310, 43, 10, 10],       
                   ["laboratory", 0, 315, 46, 10, 10],       
                   ["laboratory", 0, 320, 49, 10, 10],       
                   ["laboratory", 0, 325, 52, 10, 10],       
                   ["laboratory", 0, 330, 55, 10, 10],       
                   ["laboratory", 0, 335, 58, 10, 10],       
                   ["laboratory", 0, 340, 61, 10, 10],       
                   ["laboratory", 0, 345, 64, 10, 10],       
                   ["laboratory", 0, 350, 67, 10, 10],       
                   ["laboratory", 0, 355, 70, 10, 10],       

                   ["laboratory", 12, 230, 40, 30, 30],   

                   ["junglevegetation", 0, 280, 109, 30, 40],
                   ["junglevegetation", 0, 365, 109, 30, 40],
                   ["junglevegetation", 0, 360, 109, 30, 40],
                   ["junglevegetation", 0, 600, 109, 30, 40],
                   ["junglevegetation", 0, 610, 104, 28, 35],

                   ["laboratory", 5, 440, 71, 10, 3],       
                   ["laboratory", 5, 450, 71, 10, 3],       
                   ["laboratory", 5, 460, 71, 10, 3],       
                   ["laboratory", 5, 470, 71, 10, 3],       
                   ["laboratory", 5, 480, 71, 10, 3],       
                   ["laboratory", 5, 490, 71, 10, 3],       
                   ["laboratory", 0, 500, 71.5, 10, 10],       
                   ["laboratory", 0, 510, 71.5, 10, 10],       
                   ["laboratory", 0, 520, 71.5, 10, 10],       
                   ["laboratory", 0, 530, 71.5, 10, 10],       
                   ["laboratory", 0, 540, 71.5, 10, 10],  
                   ["laboratory", 5, 550, 71, 10, 3],       
                   ["laboratory", 5, 560, 71, 10, 3],       
                   ["laboratory", 5, 570, 71, 10, 3],       
                   ["laboratory", 5, 580, 71, 10, 3],       
                   ["laboratory", 5, 590, 71, 10, 3],     
                   ["laboratory", 5, 600, 71, 10, 3],       
  
                ],

            foreground_map:[
            
            ],

            ground_borders: // ground lines [y, xstart, xend] 
                [
                   [45, 2.6, 18.5],
                   [68, 2.6, 18.5],
                   [0, 0, 60],
                   [0, 70, 110],
                   [30, 18.5, 110],
                   [-20, 70, 130],
                   [-49, 60, 130],
                   [-52, 130, 240],

                   [-46, 90, 110],
                   [-43, 97.5, 107.5],

                   [10, 130, 165],
                   [12.5, 156.5, 165],

                   [37, 156.5, 195],
                   [49, 195, 285],
                   [47, 230, 233],

                   [57, 285, 305],
                   [67, 305, 320],
                   [70, 290, 320],

                   [12.5, 185, 280],
                   [10, 185, 250],


                   [16, 265, 270],
                   [19, 270, 275],
                   [22, 275, 280],
                   [25, 280, 285],
                   [28, 285, 290],
                   [31, 290, 295],
                   [34, 295, 300],
                   [37, 300, 305],
                   [40, 305, 310],
                   [43, 310, 315],
                   [46, 315, 320],
                   [49, 320, 325],
                   [52, 325, 330],
                   [55, 330, 335],
                   [58, 335, 340],
                   [61, 340, 345],
                   [64, 345, 350],
                   [67, 350, 355],

                   [70, 355, 440],

                   [71, 440, 700],
                      

                ],

            side_borders: // side lines [x, ystart, yend] 
                [
                   [440, 70, 71],

                   [298, 70, 90],

                   [265, 13, 16],
                   [270, 16, 19],
                   [275, 19, 22],
                   [280, 22, 25],
                   [285, 25, 28],
                   [290, 28, 31],
                   [295, 31, 34],
                   [300, 34, 37],
                   [305, 37, 40],
                   [310, 40, 43],
                   [315, 43, 46],
                   [320, 46, 49],
                   [325, 49, 52],
                   [330, 52, 55],
                   [335, 55, 58],
                   [340, 58, 61],
                   [345, 61, 64],
                   [350, 64, 67],
                   [355, 67, 70],

                   [285, 49, 57],
                   [305, 57, 67],
                   [320, 67, 70],



                  [16, 265, 270],
                  [165, 10, 12.5],
                        
                  [2.5, 0, 120],
                  [18.5, 20, 120],
                  [60, -50, 0],
                  [70, -20, 0],
                  [110, 0, 30],

                  [90, -49, -46],
                  [110, -49, -46],

                  [97.5, -46, -43],
                  [107.5, -46, -43],
                  [130, -52, -49],

                  [240, -52, 0],
                  [130, -20, 10],
                  [156.5, 10, 37],

                  [185, 10, 12.5],

                  [195, 37, 47]

                ],

            platforms: 
                [
                  [-20, 230, 240],
                  [-5, 171, 181],

                  [-41, 208, 218],
                  [-33, 230, 240],
                  
                  [-20, 180, 190],
                  [-18, 163, 173],
                  [-5, 161, 171],

                  [34, 231, 240],


                ],
            
            ladders : 
                [
                   [64, -45, 2, 45],
                   [232, 15, .3, 19.5],
                ]
        },






    ]
