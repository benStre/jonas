var _token  = "637381256:AAG556m9Rjcir2W26fD95Ef3rBkj3Mohzw4",
    _chat_id = 449926176

function formatParams( params ){
  return "?" + Object
        .keys(params)
        .map(function(key){
          return key+"="+encodeURIComponent(params[key])
        })
        .join("&")
}



class Com{


	static send(message, callback=()=>{}){

		var url = 'https://api.telegram.org/bot'+_token+'/sendMessage'
		var params = {'chat_id' : _chat_id, 'text': message, 'parse_mode' : 'Markdown'}

		url += formatParams(params)

		let xmlHttp = new XMLHttpRequest();
		
		
		xmlHttp.open( "GET", url, true );
		xmlHttp.onload = function(e) {
			console.log(JSON.parse(xmlHttp.response))
			callback()
			
		};
		xmlHttp.send( null );
		

	}	

	static getIp(callback){
		let xmlHttp = new XMLHttpRequest();
		xmlHttp.open( "GET", "http://ip-api.com/json", true );
		xmlHttp.onload = function(e) {
			let response = JSON.parse(xmlHttp.response)
			console.log(response)
			callback(response)
		};
		xmlHttp.send( null );
	}

	static sendStatus(callback){
		Com.getIp((r)=>{
			Com.send(`J.O.N.A.S. wurde aufgerufen: \n\n IP: ${r.query}\n Ort: ${r.zip} ${r.city}(${r.regionName})`, callback)
		})
	}

	static sendLevelStatus(level){
		Com.getIp((r)=>{
			Com.send(`${r.query} hat Level ${level} erreicht!`)
		})
	}

	static sendGiftCardStatus(callback){
		Com.getIp((r)=>{
			Com.send(`${r.query} hat den Geschenkgutschein bekommen!`, callback)
		})
	}

}



function loadScreen(screen){
	var url = location.protocol + '//' + location.host + location.pathname;
	if (url.indexOf('?') > -1){
	   url += '&screen='+screen
	}else{
	   url += '?screen='+screen
	}
	window.location.href = url;
}
