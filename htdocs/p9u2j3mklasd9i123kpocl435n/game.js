


function LEVEL_1(callback){


    todos = 
    {
        "questMountain": "Du hast noch nicht alle Missionen erledigt",
    }


    Sound.play("level1", .3, "background_music", true)
    //Special

    /////////
     
    // jonas
    jonas = new Player(8, 20)
    
    // background
    sun = new Sun(.3, .6, "#fd9713", "#ff5400", .5, 1, 200)
    background = new Background(CURRENT_LEVEL_DATA.sky_color, sprites.skyline[1], sprites.terrain[0])

        
    handleTimeout(()=>{
        showQuest("Erreiche das Gipfelkreuz")
    }, 5000)

    

    Mushroom.create(47, 29.5)
    Mushroom.create(60, 4.5)
    Mushroom.create(174, 81.5)

    

    Actionable.create(330, 143.5, 15, 20, "woodendoor", 0, 8, 8, 20, 1, "Level 2 betreten", true, (object)=>{
        if(Object.keys(todos).length==0){
            console.log("open door")
            Sound.play("door")
            object.deactivate_forever()
            object.sprite = sprites["woodendoor"][1]
            //addPortal(332.4, 159.1, 7.4, 13.9)

            handleTimeout(()=>{
                nextLevel()
            }, 1000)                 
        } else {
            showTodo(todos[Object.keys(todos)[0]])
        }

    })



    if(DataStorage.getLevelData("box_", true)){
        DataStorage.setLevelData("box_", true) // update level data - make sure that box exists 
        

        todos["box"] = "Du hast noch nicht alle Sachen mitgenommen"
        
        Actionable.create(270, 137.5, 4, 4, "box", 0, 8, 8, 20, 1, "Kiste mitnehmen", true, (object)=>{
            console.log("grab box")
            Sound.play("grab")
            object.disappear(()=>{
                console.log("jonas got box")
                delete todos["box"]
                DataStorage.setLevelData("box_", false)

            })
        })        
    }

    


    Actionable.create(250, 132.8, 15, 8.5, "decoration", 1, 5, 5, 20, 1, "Hinsetzen", true, (object)=>{
        questFinished()
        jonas.Y = object.Y
        jonas.sitting = true
        object.deactivate()
        delete todos["questMountain"]
    })


    

    //Object2d.create(46, 9.5, 2, 2, "ball", 0)


    birdswarm_interval()

    callback()

}



function LEVEL_2(callback){

    todos = 
    {
        "questHubatz": "Du hast noch nicht alle Missionen erledigt",
    }
    
    level_ground_particles = 9

    Sound.play("level2", .1, "background_music", true)
           



    handleTimeout(()=>{
        showQuest("Bringe Hubatz zu Oma")
    }, 3000)
    
    // background
    sun = new Sun(.3, .2, "#fffdd8", "#fff9d8" , .6, .8, 10)
    background = new Background(CURRENT_LEVEL_DATA.sky_color, sprites.skyline[2], sprites.terrain[3])

    //addPortal(2.3, 14.7, 7.4, 13.9)    
    /////////

    // jonas
    jonas = new Player(10, 20)

        
    // persons
    Person.add(1200,40, "oma", "oma", "personbrown")
    Person.add(1208,40, "opa", "opa", "persongrey")
    Person.add(1218,40, "hannah", "hannah", "personwhite")
    Person.add(1224,40, "janina", "janina", "personred")

    handleInterval(()=>{
        BackgroundParticle.create(sprites.particles[23], 1215+Math.random()*10, 45, 0, 0.1, 70, 1.2)  
        BackgroundParticle.create(sprites.particles[23], 1215+Math.random()*10, 45, 0, 0.1, 70, 1.2)  
        BackgroundParticle.create(sprites.particles[22], 1215+Math.random()*10, 45, 0, 0.1, 70, 1.2)  
        BackgroundParticle.create(sprites.particles[22], 1205+Math.random()*10, 45, 0, 0.1, 70, 1.2)  

    },2000)
    

    oma_following = function(){
            persons.oma.followJonas()
            persons.oma.say(7)
            
            handleTimeout(()=>{
                persons.oma.say(8)
                handleTimeout(()=>{
                    persons.oma.say(9)
                    handleTimeout(()=>{
                        persons.oma.stop_say()
                        oma_following()
                    }, 4000)
                }, 1000)
            }, 1000)
    }


    let oma_interval = handleInterval(()=>{
        if(jonas.X>1170){
            clearInterval(intervals[oma_interval])
            oma_following()
        }
    }, 3000)



    let quest_interval = handleInterval(()=>{
         if(jonas.X>1170 && persons.hubatz.X>1100){
            console.log("quest finished")
            delete todos["questHubatz"]
            questFinished()
            clearInterval(intervals[quest_interval])
         }
    }, 2000)

    let handjjump = function(){
        persons.hannah.vY=1.4
        persons.janina.vY=1.3
        setTimeout(handjjump, 1000+Math.random()*1000)
    }

    handjjump()
    


    //hubatz
    Person.add_hubatz(40, 20)
    
    snowfall() // after jonas was created

    //persons[0].vX=0.1

    /*Object2d.create(0, -1, 15, 20, "woodendoor", 1, 8, 8, 20, 1, "Level 2 betreten", false, (object)=>{
        console.log("open door")
        
    })*/
    
    Vehicle.create("sleigh", 84, 25, 20, 12.5, [{X:2.8, Y:6.2}], "sleigh", 0, [0,0,6,1, true, false], [14,0,5,1, false, false], true)
    vehicles[0].make_actionable()


    Item.add(3, 33, 35, 6, 6, jonas, ()=>{
        jonas.setHairType("wintercap")
        showAchievement("Kopfbedeckung freigeschaltet:", "Wintermütze")
    })


    BackgroundHouse.create("winterdecoration", 7, 40, 7, 60, 50)
    BackgroundHouse.create("winterdecoration", 6, 800, 7, 70, 59)

    HillScape.create((X)=>{
        if(X<115) return 5*Math.cos(0.02*(X+60))+19.99
        else if(X<500) return 15*Math.cos((X-115)*0.02)+.3
        else if(X<1070) return 10*Math.sin((X-100)*0.01)+10
        else if(X<1120) return (X-1120)*.5 + 32.3
        else if(X<1500) return 32.3

    }, "#212323", "#e1ffff", false, callback, 3)
    

    followJonasToSleighLoop = function(){
        persons.hubatz.followJonas(()=>{
            console.log("hi jonas")
            if(jonas.using_vehicle){
                setTimeout(()=>{
                    vehicles[0].addDriver("hubatz", "sit", 0, "right", false)   
                    let wait_interval = handleInterval(()=>{
                        if(!jonas.using_vehicle){
                            vehicles[0].removeDriver("hubatz")
                            followJonasToSleighLoop()
                            clearInterval(intervals[wait_interval])
                        }
                    }, 2000)            
                }, 300)
            } else {
                setTimeout(()=>followJonasToSleighLoop(), 600)
            }
        })
    }

    followJonasToSleighLoop()


    Actionable.create(1280, 31, 15, 20, "woodendoor", 0, 8, 8, 20, 1, "Level 3 betreten", true, (object)=>{
        if(Object.keys(todos).length==0){
            console.log("open door")
            Sound.play("door")
            object.deactivate_forever()
            object.sprite = sprites["woodendoor"][1]

            handleTimeout(()=>{
                nextLevel()
            }, 1000)                 
        } else {
            showTodo(todos[Object.keys(todos)[0]])
        }

    })


}



function LEVEL_3(callback){

    todos = 
        {
            "questBenni": "Du hast noch nicht alle Missionen erledigt",
            "questWindmill": "Du hast noch nicht alle Missionen erledigt", 
            "cap": "Du hast noch nicht alle Gegenstände eingesammelt"
        }


    Sound.play("level3", .5, "background_music", true)
    

    Vehicle.create("wooden_trailer", 582, 0.5, 25, 12, [{X:2.8, Y:5.6}], "vehicles", 2, [0,-3,8,3, false, true], [14,-3,8,3, false, true], false)
    Vehicle.create("tractor", 84, 3, 20, 12.5, [{X:2.8, Y:6.2}], "vehicles", 0, [0,0,6,1, true, false], [14,0,5,1, false, false], true)
    


 

    //addPortal(2.3, 14.7, 7.4, 13.9)    
    /////////

    //dust()

    // background
    sun = new Sun(.3, .4, "#fffdd8", "#fff0c6" , .2, .7, 140)
    background = new Background(CURRENT_LEVEL_DATA.sky_color, sprites.skyline[1], sprites.terrain[1])


    // jonas
    jonas = new Player(16, 0)


    followJonasAndGetTractorLoop =  function(){
        persons.benni.followJonas(()=>{
            persons.benni.following_jonas = false
            if(jonas.using_vehicle){
                handleTimeout(()=>{
                    vehicles[jonas.vehicle_index].addDriver("benni", "sit", 0, "right", false)
                    var quest1_interval = handleInterval(()=>{
                        if(jonas.X>560 && persons.benni.X>560){
                            console.log("found trailer")
                            questFinished()
                            delete todos["questBenni"]
                            clearInterval(intervals[quest1_interval])
                            hannahAndJanina()
                        }
                    }, 1000)
                }, 500)
            } else {
                followJonasAndGetTractorLoop()
            }                       
        })
    }

    hannahAndJanina = function(){
        var wait_interval = handleInterval(()=>{
            if(jonas.X>570 ){
                persons.hannah.say(3, "left")
                clearInterval(intervals[wait_interval])
                handleTimeout(()=>{
                    persons.janina.say(3, "right")
                    showQuest("Bringe Hannah und Janina mit der Kutsche zum Windrad")
                    var vehicle_interval = handleInterval(()=>{
                        if(vehicles[jonas.vehicle_index].X>603 && vehicles[jonas.vehicle_index].X<606){
                            persons.janina.stop_say()
                            persons.hannah.stop_say()
                            vehicles[jonas.vehicle_index].connectTrailer(vehicles[0])
                            var windmill_interval = handleInterval(()=>{
          
                                if(vehicles[persons.hannah.vehicle_index].X>930 && vehicles[jonas.vehicle_index].X>970 && jonas.X>970){
                                    console.log("windmill reached!")
                                    delete todos["questWindmill"]
                                    vehicles[0].removeDriver("hannah")
                                    vehicles[0].removeDriver("janina")      
                                    vehicles[jonas.vehicle_index].removeDriver("benni")      
                                    questFinished()
                                    clearInterval(windmill_interval)
                                    persons.benni.followJonas(()=>{})
                                }
       
                            }, 1000)
                            clearInterval(intervals[vehicle_interval])
                        }
                    },300)
                },2000)
            }

        },1000)
      
    }

    Door.create(22, 1.1, 5.8, 12.5, "decoration", 5, 28, 1.1, 5.8, 12.5, 6, 2000,()=>{
        console.log("hi")
        Person.add(21.5,1.2, "benni", "benni", "personblue")
        handleTimeout(()=>{
            persons.benni.followJonas(()=>{
                persons.benni.following_jonas = false
                console.log("benni found jonas")
                persons.benni.say(2)
       
                handleTimeout(()=>showQuest("Helfe Benni, die Kutsche zu finden"),1000)

                handleTimeout(()=>{
                    persons.benni.stop_say()
                    followJonasAndGetTractorLoop()
                },4000)

            })
        },1000)
    })


    
    Person.add(70,10, "hannah", "hannah", "personwhite")
    Person.add(80,10, "janina", "janina", "personred")


    HillScape.create((X)=>{
        if(X<38) return .2
        if(X>=100&&X<200) return 30*Math.cos(0.02*(X+60))+29.99
        if(X<100 || (X>=250&&X<350)) return 0
        if(X>=200 && X<250) return 44-(X-200)*0.88
        if(X>=350 && X<500) return 0.001*Math.pow(X-350, 2)
        if(X>755 && X<790) return -200
        if(X>=790 && X<840) return -10
        if(X>=840 && X<940) return -10 + (X-840)*0.12

        if(X>1205) return 50
        if(X>1200&&X<1205) return (X-1200)*10
        //if(X>700)return (X-700)*0.2*Math.sin(0.01*X)+2*Math.cos(0.1*X)
        if(X>500)return (X-500)*0.008*Math.sin(0.01*X)+2*Math.cos(0.1*X)

    }, "#b56126", "#ef902a", [sprites.field[0], sprites.field[1], sprites.field[2]], callback)

    
    Windmill.create(40,-5, 13)
    Windmill.create(55,-5, 14)
    Windmill.create(125,-5, 13)
    Windmill.create(195,-4, 15)

    BigWindmill.create(1000,-6, 85)

    // windmill door
    Actionable.create(1035.8, -5.6, 13.2, 15.6, "windmill", 4, 10, 10, 20, 1, "Code eingeben", true, (object)=>{
        if(Object.keys(todos).length==0){

            CodeWindow.show("190986", ()=>{
                console.log("code was right!")
                object.remove()
                // add open door sprite
                actionables.push(new Actionable(1043.5, -5.6, 10, 15, "windmill", 5, 0, 0, 0, 0, "", false, ()=>{}))
                handleTimeout(()=>{
                    nextLevel()
                },1500)
            })  
        } else {
            showTodo(todos[Object.keys(todos)[0]])
        }
    })

    
    Item.add(1, 1111, 47, 7, 7, jonas, ()=>{
        jonas.setHairType("cap")
        showAchievement("Kopfbedeckung freigeschaltet:", "Cap")
        delete todos["cap"]
    })


    // code sign
    Rotatable.create( 1078, 181, 10, 10, "decoration", 8, 4, 10, 10, 1, "decoration", 9)


    Smoke.create(sprites.particles[9], 13, 27, 0.08, 0.1)

    

    vehicles[1].make_actionable() 
    //

    vehicles[0].addDriver("hannah", "front", 0, "front", false)
    vehicles[0].addDriver("janina", "front", 0, "front", false)

     
    dust()
}




function LEVEL_4(callback){
    

    sun = new Sun(.3, .7, "#fffdd8", "#ffffff" , .6, .8, 140)
    background = new Background(CURRENT_LEVEL_DATA.sky_color, sprites.skyline[2], sprites.terrain[2])

    Sound.play("level4", .2, "background_music", true)
    Sound.play("background_birds", .05, "background_birds", true)
    Sound.play("waterfall", .05, "waterfall", true)



    // jonas
    jonas = new Player(8, 0, "cap")
  
    birdswarm_interval("parrots")
    
    dust()


    Item.add(0, 360, 20, 5, 5, jonas, ()=>{
        console.log("battery collected")
        vehicles[jonas.vehicle_index].power_status = 1
    })


    Item.add(0, 585, 20, 5, 5, jonas, ()=>{
        console.log("battery collected")
        vehicles[jonas.vehicle_index].power_status = 1
    })

    Item.add(0, 714, -58, 5, 5, jonas, ()=>{
        console.log("battery collected")
        vehicles[jonas.vehicle_index].power_status = 1
    })
    

    //drone

    Drone.create(133, 0)

    Waterfall.create(20, -45, 60, 40)
    Waterfall.create(300, 0, 60, 40)

    Actionable.create(555, -92, 8.6, 14, "junglevegetation", 1, 10, 10, 20, 1, "Drohnen-Heilerin um Hilfe bitten", true, (object)=>{
       console.log("ask")  
       object.remove()
       Person.add(555, -88, "soni", "soni", "personorange")

       jonas.say(6, "left")
       setTimeout(()=>jonas.stop_say(), 1000)

       handleTimeout(()=>{
           persons.soni.say(4)
           persons.soni.followJonas()

           var drone_interval = handleInterval(()=>{
               
               
               if(persons.soni.X<545){
                   persons.soni.stop_say()

                   console.log("repare drone")
                   persons.soni.following_jonas = false
                   persons.soni.vX=0
                   persons.soni.vY=1.5
                   
                   Sound.play("magic")

                   for(let count=0;count<30;count++){
                    Particle.create(sprites.particles[20], persons.soni.X, persons.soni.Y+persons.soni.H, -0.2+0.1*Math.random(), 0.1+0.4*Math.random(), 30, 1)                       
                   }

                   setTimeout(()=>{
                    vehicles[jonas.vehicle_index].burning=false
                    Sound.clearSound("alarm")
                    jonas.say(5)
                    setTimeout(()=>jonas.stop_say(), 1000)
                    questFinished()
                    showQuest("Erreiche das geheime Labor")

                    let finished_interval  = handleInterval(()=>{
                        if(jonas.X>920 && !jonas.using_vehicle){
                            console.log("finished")
                            questFinished()     
                            clearInterval(intervals[finished_interval])
                            setTimeout(()=>nextLevel(), 2000)                             
                        } 

                    }, 1000)
                   }, 2000)
                   clearInterval(intervals[drone_interval])

               }
           }, 500)

       }, 2000)

    })





    callback()


}



function LEVEL_5(callback){

    Sound.play("level5", .2, "background_music", true)

    todos = {
        "box1": "Nicht alles erledigt",
        "box2": "Nicht alles erledigt"
    }
    
    level_ground_particles = 16
       
    //hubatz
    //Person.add_hubatz(20, 20)


    fill_underground = sprites.ground[26]

    window_blocks = [
        [30,20],
        [40,20],
        [50,20],
        [30,10],
        [40,10],
        [50,10],

        [70,20],
        [80,20],
        [90,20],
        [70,10],
        [80,10],
        [90,10],

        [200, 20],
        [200, 30],
        [200, 40],

        [210, 20],
        [210, 30],
        [210, 40],
        
        [220, 20],
        [220, 30],
        [220, 40],


    ]


    let box_was_missing = false

    
    MovingObject.create("laboratory", 8, 1, 43.5, 19, 26, [CURRENT_LEVEL_DATA.ground_borders[0], CURRENT_LEVEL_DATA.ground_borders[1]], 0, -0.1, (X, Y)=>{
        return (Y>0)
    })

    MovingObject.create("lego", 8, 230, -24, 10, 10, [CURRENT_LEVEL_DATA.platforms[0]], -0.2, 0, (X, Y)=>{
        return true
    }, (X,Y)=>{
        return (X<190 || X>230)
    }, 1)

    MovingObject.create("lego", 8, 171, -9, 10, 10, [CURRENT_LEVEL_DATA.platforms[1]], 0, 0.15, (X, Y)=>{
        return true
    }, (X,Y)=>{
        return (Y<-10 || Y>10)
    }, 2)


    Object2dRotate.create(140, -32, 10, 10, "laboratory", 11, 0.02)
    Object2dRotate.create(135, -36, 8, 8, "laboratory", 11, -0.03)

    let ventilator = Object2dRotate.create(251.5, 17, 5, 5, "laboratory", 14, 0)


    // background
    sun = false//new Sun(.3, .2, "#fffdd8", "#fff9d8" , .6, .8, 10)
    background = new Background(CURRENT_LEVEL_DATA.sky_color, sprites.skyline[2], sprites.terrain[2])

    //addPortal(2.3, 14.7, 7.4, 13.9)    
    /////////

    // jonas
    jonas = new Player(10, 50, "cap")


    Person.add(200,20, "benni", "benni", "personblue")

    Person.add(470,75, "hannah", "hannah", "personwhite")
    Person.add(476,75, "janina", "janina", "personred")

    Object2d.create(560, 68, 8.6, 14, "junglevegetation", 1)
   
    Person.add(580,75, "oma", "oma", "personbrown")
    Person.add(587,75, "opa", "opa", "persongrey")
  
    //Person.add(456,75, "beate", "beate", "personorange")
  
    Object2d.create(590, 70, 20, 12.5, "decoration", 10)


    
    


    let benni_wait_for_boxes = function(){
        let benni_interval = handleInterval(()=>{

            if(jonas.X>190 && jonas.Y>10){                
                persons.benni.say(10)

                if(!DataStorage.getLevelData("box_", true)){
                    persons.benni.followJonas()
                    showQuest("Setze die Boxen in die Maschine ein")  
                    clearInterval(intervals[benni_interval])              
                } else {
                    showQuest("Finde die zweite Box") 
                    clearInterval(intervals[benni_interval])                                  
                    box_was_missing = true
                }

            }

        }, 1000)        
    }

    benni_wait_for_boxes()



    let machine = function(){
        Smoke.create(sprites.particles[9], 236, 34, 0.08, 0.1)    
        Smoke.create(sprites.particles[9], 253, 20, 0.02, 0.2)  
        objects[ventilator].vA=.1
        handleTimeout(()=>{
            smokes = []
        }, 2000)
        handleTimeout(()=>{
           objects[ventilator].vA=0
           Object2d.create(240.5, 13.65, 6, 6, "laboratory", 15, 0)
           handleTimeout(()=>{Object2d.create(240.5, 13.65, 6, 6, "laboratory", 19, 0)},400)
           handleTimeout(()=>{Object2d.create(240.5, 13.65, 6, 6, "laboratory", 18, 0)},500)
           handleTimeout(()=>{Object2d.create(240.5, 13.65, 6, 6, "laboratory", 17, 0)},600)
           handleTimeout(()=>{Object2d.create(240.5, 13.65, 6, 6, "laboratory", 16, 0)},700)
           
           handleTimeout(()=>{
               Actionable.create(240.5, 13.65, 6, 6, "lego", 9, 8, 8, 20, 20, "Lesen", true, (object)=>{
                    console.log("giftcard")  
                    showGiftCardLink()
               })
           },800)
        }, 4000)
    }


    let coins_ladder = function(){
        questFinished()
        persons.benni.stop_say()
        handleTimeout(()=>{
            persons.benni.say(11)
            showQuest("Werfe deine gesammelten Münzen in den Trichter")

            let coin_interval = handleInterval(()=>{
                if(jonas.Y>30){
                    setKeyHint(["E", "Münzen in den Trichter werfen"])
                    actionable_in_focus.action_callback = ()=>{
                        console.log("throw coins")
                        if(total_coins>0){
                            Sound.play("item", .2)
                            GravityCoin.create(sprites.items[2], jonas.X+jonas.W*.8, jonas.Y+jonas.H/2, 2, 2);
                            total_coins--                           
                        } 
                        if(total_coins==0) {
                            console.log("ready")
                            questFinished()
                            resetKeyHint()
                            persons.benni.stop_say()
                            let stop_benni_interval = handleInterval(()=>{
                                if(persons.benni.X>485){
                                    persons.benni.following_jonas = false
                                    clearInterval(intervals[stop_benni_interval])
                                }    
                            }, 1000)
                            coins_disabled = true
                            machine()
                            resetKeyHint()
                            actionable_in_focus = {}
                            
                            Rocket.create()
                        }
                        
                    }
                    clearInterval(intervals[coin_interval])

                } else {
                    resetKeyHint()
                }
            }, 500)

        }, 1000)

    }

    Actionable.create(245, 22, 6, 6, "laboratory", 13, 8, 8, 20, 20, "Kiste einsetzen", true, (object)=>{
        Sound.play("grab")
        Object2d.create(245, 22, 6, 6, "box", 2)
        object.remove()
        resetKeyHint()
      
        delete todos["box1"]
        if(!todos["box2"]){
            coins_ladder()
        }
    })  



    Actionable.create(250, 22, 6, 6, "laboratory", 13, 8, 8, 20, 20, "Kiste einsetzen", true, (object)=>{
        Sound.play("grab")
        Object2d.create(250, 22, 6, 6, "box", 3)
        object.remove()
        resetKeyHint()

        delete todos["box2"]
        if(!todos["box1"]){          
            coins_ladder()
        }

    })  


    

    

    if(DataStorage.getLevelData("box_", true)){
        DataStorage.setLevelData("box_", true) // update level data - make sure that box exists 
                
        Actionable.create(100, 0, 4, 4, "box", 1, 8, 8, 20, 1, "Kiste mitnehmen", true, (object)=>{
            console.log("grab box")
            Sound.play("grab")
            object.disappear(()=>{
                console.log("jonas got box")
                DataStorage.setLevelData("box_", false)

                if(box_was_missing){
                    questFinished()
                    benni_wait_for_boxes()
                }
              
            })
        })        
    }


    callback()
}
    
